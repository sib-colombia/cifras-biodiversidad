# Lista taxonómicas y temáticas de referencia

Estas listas provienen de diferentes fuentes externas o recursos publicados por especialistas en diferentes grupos a traves del SiB Colombia. Anualmente, se consulta cada uno de los recursos, identificando actualizaciones. Además, se realizan validaciones taxonomicas con el fin de detectar actualizaciones en la información taxonomica o posibles inconsistencias en la información.

Para estas listas se mantendrá dos archivos (a excepción de lor archivos _Lista_InvasorasMADS, Listas-CEIBA-2023 y Listas-GRIIS-2023_), uno con los campos de validación y el total de campos originales de los archivos iniciales, este lleva la palabra completa en el nombre (Ej: _Listas_taxonomicasCol_2022_completa_). El otro archivo, solo cuenta con la información de taxonomía y de categorías de interés necesarias para hacer el cruce con los registros biológicos del corte (Ej: _Listas_taxonomicasCol_2022_).
