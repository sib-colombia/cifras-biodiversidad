# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 11:34:59 2022

@author: nerieth.leuro

Este script fue creado por el SiB Colombia para realizar la valicación y completar la 
información de habitat (marino, salobre y terrestre) para registros marinos tanto 
a nivel municipal como departamental.

Este script cuanta con cuatro secciones:
    1- Validación zonas marinas: Esta sección permite validar que los registros que se
    identifiquen como marinos corresponden a departamentos y municipios con zonas 
    maritimas y que se encuentran dentro de los limites de cada región. 
    2- Obtener nombres cientificos unicos: Permite obtener los nombres cientificos
    unicos encontrados del conjunto de datos.
    3- Llamado API WoRMS: Realiza el llamado de la API de WorMS para obtener los
    habitats correspondientes a cada taxón. 
    4- Procesar registros marinos: Permite unir la información obtenida de lifewatch con
    el conjunto de datos.

Las secciones 1 y 2 se ejecutan para categorizar los datos con el fin de determinar
cuales se encuentran en zonas marinas y con estos obtener nombres unicos de especies
para validar con WoRMS.

La validación con WoRMS se puede realizar con el uso de Taxon match World 
Register of Marine Species (WoRMS) en https://www.lifewatch.be/data-services/?cache=1635170836 
o utilizando la sección 3 de este script. En el caso de utilizar la sección 3
no es necesario utilizar la sección 4, de lo contrario ejecute esta última 
para unir la información obtenida de Taxon match World Register of Marine 
Species (WoRMS) y los registros.
    
Tenga en cuenta que dentro del script podra encontrar algunas secciones identificadas 
con ##, estas corresponden a líneas explicativas. Y partes del código que se encuentran 
comentariadas con # que corresponden a secciones de código que se pueden habilitar o 
deshabilitar según los datos que se desee obtener.

"""

##-----------------------------------------------------Preparar el entorno---------------------------------------------##
##Importar las librerias necesarias para el uso del script
import pandas as pd 
import os 
import time
import numpy as np
import pyworms

##Iniciar el conteo de tiempo de ejecución del script
inicio=time.time()

##Mostrar la carpeta principal donde se encuentra alojado el script
print(os.getcwd())

##Establecer la carpeta de trabajo
os.chdir("D:\\cifras_Nariño\\2022T2")


##----------------------------------------------------Cargar conjunto de datos--------------------------------------------------##
'''
Para conjuntos de datos para obtener cifras municipales y equipos con poca memoria RAM (8 gb o menos)
Se agrega dtype (asignar el tipo de dato a cada columna)
Con el fin de evitar el error: pandas.errors.ParserError: Error tokenizing data. C error: out of memory
En el caso de conjuntos de datos para cifras departamentales realice la ejecución en un equipo con memoria RAM de 16 gb

dtype: se especifica cada columna y el tipo de dato que corresponde, el orden de los campos no afecta la ejecución, pero debe actualizarse
cada que se agreguen y quiten campos para hacer más eficiente el proceso.
    str: Se utiliza para campos que contengan texto y símbolos
    float: Aplica en campos que contengan números decimales

'''

##Crear un objeto cargando la tabla de datos
##Se pueden cargar archivos con formato csv, txt o tsv. Tenga en cuenta los separadores 
##y la codificación con la que se guardaron los archivos. Para especificar estos valores
##utilice sep y encoding

registros = pd.read_table('Narino_data_marinos2022T2_n.tsv', sep='\t', encoding = "utf8",dtype=str)    
print(registros.shape)
##Eliminar las columnas 'isMarine','isBrackish','isTerrestrial' cuando sea necesario
#registros=registros.drop(['isMarine','isBrackish','isTerrestrial'], axis='columns')


##Cargar archivos de referencia geográfica 
##Con la información de departamentos
staProv_divipola = pd.read_table('divipola\\departamento.tsv', encoding = "utf8")

##Con la información de municipios
staProv_divipola_m = pd.read_table('divipola\\region.tsv', encoding = "utf8",usecols=['parent','slug','label'])

##----------------------------------------------------Tipo de ejecución--------------------------------------------------##
'''
La variable 'tipo' permite condicionar los procesos teniendo en cuenta si se van a sacar cifras departamentales o municipales y si 
el conjunto de datos contiene información de registros marítimos
Departamental con datos marinos='DCDM'
Departamental sin datos marinos='DSDM'
Municipal con datos marinos='MCDM'
Municipal sin datos marinos='MSDM'
Otros='OT'
Al seleccionar alguna de las opciones sin datos marinos, las cifras se calculan en forma general, sin discriminar por los hábitat marino, 
terrestre y salobre

Para las opciones con datos marinos, se realiza el cálculo de cifras general y adicionalmente el cálculo para los hábitat marino, terrestre 
y salobre. Es importante aclarar que debido a los hábitos y distribución de las especies se pueden encontrar especies presentes en más de 
un hábitat por lo tanto, el valor general no corresponde a la suma de los valores para cada hábitat.

'''
#tipo='DCDM'
#tipo='DSDM'
tipo='MCDM'
#tipo='MSDM'
#tipo='CCDM'
#tipo='CSDM'

departamento='Nariño'

##--------------------------------------------Ajustes preliminares de los datos ---------------------------------------------##
'''
Cuando se realiza la ejecución de este script sin realizar un proceso de limpieza en el campo de stateProvince se debe realizar el cruce con el
archivo de referencia geográfica DIVIPOLA. Lo cual permitirá asegurar que en este campo solo se encuentren los departamentos válidos para el
país.
'''
if tipo=='DCDM' or tipo=='DSDM' or tipo=='CCDM' or tipo=='CSDM':
    
     ##Reajustar campo stateProvince (asignar al campo verbatimStateProvince el valor de stateProvince)
    registros['verbatimStateProvince']=registros['stateProvince']
    
    ##Quitar comillas
    registros['stateProvince']=registros.stateProvince.str.replace('"','')
    
    ## Unir las tablas por medio del campo stateProvince
    registros=pd.merge(registros,staProv_divipola, left_on='stateProvince', right_on='label',how='left') 
    
    #Reemplazar los valores null de stateProvinceDivipola por los valores de Departamento-ubicacionCoordenada
    registros['stateProvince']=registros['label'].mask(pd.isnull, registros['Departamento-ubicacionCoordenada'])
    
if tipo=='DCDM' or tipo=='DSDM':    
    region='slug'   
if tipo=='CCDM' or tipo=='CSDM':
    registros['slug_col']='colombia'
    region='slug_col'  
    
    
if tipo=='MCDM' or tipo=='MSDM':
    ##Reajustar campo stateProvince (asignar al campo verbatimStateProvince el valor de stateProvince)
    registros['verbatimStateProvince']=registros['stateProvince']
    
    ##Quitar comillas
    registros['stateProvince']=registros.stateProvince.str.replace('"','')
    
    ## Unir las tablas por medio del campo stateProvince
    registros=pd.merge(registros,staProv_divipola, left_on='stateProvince', right_on='label',how='left') 
    
    #Reemplazar los valores null de stateProvinceDivipola por los valores de Departamento-ubicacionCoordenada
    registros['stateProvince']=registros['label'].mask(pd.isnull, registros['Departamento-ubicacionCoordenada'])    
    registros=pd.merge(registros,staProv_divipola_m, left_on=['slug','county'], right_on=['parent','label'],how='left') 
    registros['verbatimcounty']=registros['county']
    registros['county']=registros['label_y'].mask(pd.isnull, registros['Municipio-ubicacionCoordenada'])
        
    region='slug_y'
    
##---------------------------------------------------- 1- Validación zonas marinas--------------------------------------------------##
'''
En esta sección se asigna el tipo de registro. Se marcan como 'Marinos' teniendo
en cuenta el campo de ZonaMaritima, para Colombia se tienen en cuenta todas las
zonas maritimas. En el caso de departamento se tiene en cuenta solo la zona maritima
que aplique.En el caso de datos de Nariño adicionalmente se tienen en cuenta 
el campo Nombre, correspondiente a la zona geografica marina para el departamento de Nariño.

'''

'''
if tipo=='DCDM':
    registros.loc[(registros['ZonaMaritima'] == 'Cayo Bajo Nuevo') | (registros['ZonaMaritima'] == 'Cayo Serranilla') 
    | (registros['ZonaMaritima'] == 'Mar Caribe') | (registros['ZonaMaritima'] == 'Oceano Pacífico') 
    | (registros['ZonaMaritima'] == 'Área de régimen común'), 'tipo_registro'] = 'Marino'

if tipo=='MCDM':
    ##Para Nariño
    registros.loc[(registros['Nombre'] == 'UAC Llanura Aluvial del Sur') | (registros['ZonaMaritima'] == 'Oceano Pacífico'), 'tipo_registro'] = 'Marino'

'''
#registros.loc[(registros['Nombre'] == 'UAC Llanura Aluvial del Sur') | (registros['ZonaMaritima'] == 'Oceano Pacífico'), 'tipo_registro'] = 'Marino'
##----------------------------------------------------  2- Obtener nombres cientificos unicos--------------------------------------------------##
##Sacar los nombres cientificos unicos
'''
nombres=pd.DataFrame()
registros =registros.loc[registros.loc[:, 'tipo_registro'] == 'Marino']
nombres['scientificName']=registros['scientificName'].unique()
##Exportar archivo con nombres unicos
nombres.to_csv('nombresCientificosUnicos.txt',sep='\t', encoding = "utf8")
'''
##---------------------------------------------------3- Llamado API WoRMS---------------------------------------------------------------------##
'''
Con el siguiente código se puede hacer el llamado de la API de WoRMS. Sin embargo esta puede ser inestable en algunos momentos
y tardar mucho tiempo en su ejecución, por lo cual, se puede utilizar el Taxon match World Register of Marine Species (WoRMS) en 
https://www.lifewatch.be/data-services/?cache=1635170836 el cual permite obtener la misma información. Tenga en cuenta que puede
cargar un maximo de 600000 filas. 


##Funcion llamado de la API
def testAphiaRecordsByMatchNames(especie):
    res = pyworms.aphiaRecordsByMatchNames(especie)
    #print(len(res[0]))
    if len(res[0])!=0:
        isMarine=res[0][0]['isMarine']
        isTerrestrial=res[0][0]['isTerrestrial']
        isBrackish=res[0][0]['isBrackish']
    else:
        isMarine=0
        isTerrestrial=0
        isBrackish=0
    return isMarine, isTerrestrial,isBrackish
    
##Ciclo para asignar las categorias a cada especie
for i in range(len(nombres)):
    llamadoCategorias=testAphiaRecordsByMatchNames([nombres['scientificName'][i]])
    nombres['isMarine']=llamadoCategorias[0]
    nombres['isTerrestrial']=llamadoCategorias[1]
    nombres['isBrackish']=llamadoCategorias[2]

registrosMarinosFinales=pd.merge(registros,nombres, on=['scientificName'],how='left')

registrosMarinosFinales.loc[registrosMarinosFinales.isMarine==1,'isMarine']='Marine'
registrosMarinosFinales.loc[registrosMarinosFinales.isTerrestrial==1,'isTerrestrial']='Terrestrial'
registrosMarinosFinales.loc[registrosMarinosFinales.isBrackish==1,'isBrackish']='Brackish'

registrosMarinosFinales.loc[(registrosMarinosFinales['ZonaMaritima'].isnull()),  'isMarine'] = np.nan
registrosMarinosFinales.loc[(registrosMarinosFinales['ZonaMaritima'].isnull()),  'isTerrestrial'] = 'Terrestrial'
#registros.loc[(registros['Nombre'] == 'UAC Llanura Aluvial del Sur') | (registros['ZonaMaritima'] == 'Oceano Pacífico'), 'isMarine'] = np.nan

registrosMarinosFinales.loc[registrosMarinosFinales.marino=='FALSE','isMarine']=np.nan

registrosMarinosFinales.to_csv('registrosMarinosFinales.txt',sep='\t')
'''
##---------------------------------------------------4 -Procesar registros marinos --------------------------------------------------##
'''

Con el archivo resultante de la ejecución del servicio de WoRMS se realiza la limpieza de la información y se crean las columnas con las 
categorias de habitat marino, terrestre y salobre

'''
##Cargar el archivo obtenido de LifeWatch
#registrosCategoria = pd.read_table('nombrescientificosunicos_matched.txt', sep='\t', encoding = "utf8").dropna().drop(['column 0','required_fields_check'], axis=1)
registrosCategoria = pd.read_table('nombrescientificosunicos_matched.txt', sep='\t', encoding = "utf8",dtype=str)

##Seleccionar solo las columnas scientificName, environment_aphia_WORMS y aphiaid_WORMS
#registrosCategoria =registrosCategoria.loc[:,['scientificname','environment_aphia_WORMS','aphiaid_WORMS'] ]
registrosCategoria =registrosCategoria.loc[:,['ScientificName','isMarine','isTerrestrial','isBrackish','AphiaID'] ]

##Ajustar el nombre de la columna scientificname
registrosCategoria = registrosCategoria.rename(columns={'ScientificName':'scientificName'})

##Crear una nueva columna 'URL WoRMS' en la cual se concatena la url de marinespecies y el 'aphiaid_WORMS'
registrosCategoria['URL WoRMS']='https://www.marinespecies.org/aphia.php?p=taxdetails&id='+ registrosCategoria['AphiaID'].map(str)
registrosCategoria['URL WoRMS']=registrosCategoria['URL WoRMS'].replace(to_replace='\.0+$',value="",regex=True)

##Buscar en la columna environment_aphia_WORMS las categorías de Marine, Brackish y Terrestrial
##En el caso donde contenga la palabra Marine, en la columna isMarine se asigna el valor Marine
registrosCategoria.loc[registrosCategoria['isMarine']=='1','isMarine']='Marine'
registrosCategoria.loc[registrosCategoria['isMarine']=='0','isMarine']= np.nan

##En el caso donde contenga la palabra Brackish, en la columna isBrackish se asigna el valor Brackish
registrosCategoria.loc[registrosCategoria['isBrackish']=='1','isBrackish']='Brackish'
registrosCategoria.loc[registrosCategoria['isBrackish']=='0','isBrackish']= np.nan

##En el caso donde contenga la palabra Terrestrial, en la columna isTerrestrial se asigna el valor Terrestrial
registrosCategoria.loc[registrosCategoria['isTerrestrial']=='1','isTerrestrial']='Terrestrial'
registrosCategoria.loc[registrosCategoria['isTerrestrial']=='0','isTerrestrial']= np.nan
#registros.loc[registros.tipoRegistro.str.contains('Continental'),'isTerrestrial']='Terrestrial'

##Unir los registros con las categorias y exportar el archivo final para cifras
registrosFinales=pd.merge(registros,registrosCategoria, on=['scientificName'],how='left')


registrosFinales.loc[(registrosFinales['ZonaMaritima'].isnull()) | (registrosFinales['ZonaMaritima'].isnull()),  'isMarine'] = np.nan
registrosFinales.loc[(registrosFinales['ZonaMaritima'].isnull()),  'isTerrestrial'] = 'Terrestrial'


#registrosFinales.loc[registrosFinales.marino=='FALSE','isMarine']=np.nan

print(registrosFinales.groupby('isMarine')['isMarine'].unique())
#print(registrosFinales.groupby('isMarine')['ZonaMaritima'].unique())
#print(list(registrosFinales.groupby('isMarine')['stateProvince'].unique()))

##Exportar el conjunto de datos final con la información de los habitats
registrosFinales=registrosFinales[['Column','gbifID','organization','eventDate','stateProvince','county','municipality','locality',
                                  'decimalLatitude','decimalLongitude','Departamento-ubicacionCoordenada','Municipio-ubicacionCoordenada','ZonaMaritima','stateProvinceValidation','countyValidation',
                                  'scientificName','kingdom','phylum','class','order','family','genus','species','taxonRank','appendixCITES',
                                  'threatStatus_UICN','threatStatus_MADS','especies_invasoras','especies_exoticas','especies_exotica_riesgo_invasion',
                                  'endemic','migratory','publishingOrgKey','logoUrl','publishingCountry','Nombre','isMarine','isBrackish','isTerrestrial','verbatimStateProvince','verbatimcounty']]

registrosFinales.to_csv('Narino_data2022T2_finales.tsv',sep='\t', encoding = "utf8", index=False)
print(registrosFinales.shape)


