# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 17:05:58 2022

@author: nerieth.leuro

Este script permite calcular el número de especies estimadas para diferentes temáticas.
Para su ejecución debe contar con la lista de especies con su taxonomica superior y la categoría
temática que corresponde a cada una. 
Estas listas pueden variar año a año por lo cúal asegurese de usar la más actualizada. Y son las
misma utilizadas para completar los registros biologicos obtenidos de GBIF para la obtención de 
cifras.
Las listas disponibles son:
Lista_InvasorasMADS   
Lista_GRIIS
Lista_UICNredList
ListaCITES
Lista_amenazadas_resolucion1912_2017 
"""

##-----------------------------------------------------Preparar el entorno---------------------------------------------##
##Importar las librerias necesarias para el uso del script
import pandas as pd 
import os 


##Establecer la carpeta de trabajo
os.chdir("D:\\Versión 2022-2\cifras_Colombia")

##----------------------------------------------------Cargar lista de referencia--------------------------------------------------##

##Cargar el archivo que contine la información del taxón relacionado a la categoría tematica
cites = pd.read_table('CITES\\ListaCITES_2022.tsv', sep='\t', encoding = "utf8",dtype=str)    

mads = pd.read_table('MADS\\Lista_amenazadas_resolucion1912_2017_2022.tsv', sep='\t', encoding = "utf8",dtype=str)    

iucn = pd.read_table('IUCN\\Lista_UICNredList_2022.tsv', sep='\t', encoding = "utf8",dtype=str)    
iucn=iucn.loc[iucn.loc[:, 'threatStatus_UICN'].isin(['VU','EN','CR'])]

exoticas=pd.read_table('exoticas\\lista_invasoras_exoticas_2022.tsv', sep='\t', encoding = "utf8",dtype=str)    

taxonomicas=pd.read_table('taxonomicas\\Listas_taxonomicasCol_2022.tsv', sep='\t', encoding = "utf8",dtype=str)    


## Cargar el archivo de grupos biologicos que relacionan la taxonomía con los grupos biológicos
grupos_biologicos = pd.read_table('grupos_biologicos\\gruposBiologicosCifrasSiB-v20220705.tsv', sep='\t', encoding = "utf8")
grupos_biologicos=grupos_biologicos.loc[grupos_biologicos.loc[:, 'tipo_grupo'] != '-']
##Defina la columna que contiene la categoría según la tematica que desea tratar
##CITES
#categoria='appendixCITES'

##Amenazadas MADS
#categoria='threatStatus'

##Crear una variable string con el nombre del conjunto de datos. Este nombre debe incluir: la geografía, el año y el corte trimestral utilizado 
#nombre_mads,nombre_cites,nombre_iucn='estimada_mads','estimada_cites','estimadas_iucn'
#nombre_exoticas,nombre_invasoras,nombre_potencial='estimada_exoticas','estimada_invasoras','estimadas_potencial'

##-------------------------------------------------Loop calculo cifras ---------------------------------------------------##

## Define una lista con todos las categorias taxonomicas
## Asegurese que las columnas en el archivo tengan los mismos nombres encontrados en la lista
taxon = ['kingdom','phylum','class','order','family','genus','species'] 

## Dataframe para especies
sp_estimadas_mads=pd.DataFrame()
sp_estimadas_cites=pd.DataFrame()
sp_estimadas_iucn=pd.DataFrame()
sp_estimadas_exoticas=pd.DataFrame()
sp_estimadas_invasoras=pd.DataFrame()
sp_estimadas_potencial=pd.DataFrame()
sp_estimadas_endemicas=pd.DataFrame()
sp_estimadas_migratorias=pd.DataFrame()

for j in taxon:    
    
    ## Número de taxones por grupo taxonómico en la categoría general
    sp_mads = mads.groupby([j,'threatStatus'])['threatStatus'].count().to_frame(name = 'taxones' ).reset_index()
    sp_cites = cites.groupby([j,'appendixCITES'])['appendixCITES'].count().to_frame(name = 'taxones' ).reset_index()
    sp_iucn = iucn.groupby([j,'threatStatus_UICN'])['threatStatus_UICN'].count().to_frame(name = 'taxones' ).reset_index()
    sp_exoticas = exoticas.groupby([j,'especies_exoticas'])['especies_exoticas'].count().to_frame(name = 'taxones' ).reset_index()
    sp_invasoras = exoticas.groupby([j,'especies_invasoras'])['especies_invasoras'].count().to_frame(name = 'taxones' ).reset_index()
    sp_potencial = exoticas.groupby([j,'especies_exotica_riesgo_invasion'])['especies_exotica_riesgo_invasion'].count().to_frame(name = 'taxones' ).reset_index()
    sp_endemicas = taxonomicas.groupby([j,'endemic'])['endemic'].count().to_frame(name = 'taxones' ).reset_index()
    sp_migratorias = taxonomicas.groupby([j,'migratory'])['migratory'].count().to_frame(name = 'taxones' ).reset_index()


    
    #rb_numero['thematic']=cites[categoria]
    sp_mads=sp_mads.rename(columns={j:'grupoTax','threatStatus':'thematic'})
    sp_mads['taxonRank']=j
    sp_cites=sp_cites.rename(columns={j:'grupoTax','appendixCITES':'thematic'})
    sp_cites['taxonRank']=j
    sp_iucn=sp_iucn.rename(columns={j:'grupoTax','threatStatus_UICN':'thematic'})
    sp_iucn['taxonRank']=j
    sp_exoticas=sp_exoticas.rename(columns={j:'grupoTax','especies_exoticas':'thematic'})
    sp_exoticas['taxonRank']=j
    sp_invasoras=sp_invasoras.rename(columns={j:'grupoTax','especies_invasoras':'thematic'})
    sp_invasoras['taxonRank']=j
    sp_potencial=sp_potencial.rename(columns={j:'grupoTax','especies_exotica_riesgo_invasion':'thematic'})
    sp_potencial['taxonRank']=j
    sp_endemicas=sp_endemicas.rename(columns={j:'grupoTax','endemic':'thematic'})
    sp_endemicas['taxonRank']=j    
    sp_migratorias=sp_migratorias.rename(columns={j:'grupoTax','migratory':'thematic'})
    sp_migratorias['taxonRank']=j      
    
    
    
    sp_estimadas_mads=pd.concat([sp_estimadas_mads,sp_mads])
    sp_estimadas_cites=pd.concat([sp_estimadas_cites,sp_cites])
    sp_estimadas_iucn=pd.concat([sp_estimadas_iucn,sp_iucn])
    sp_estimadas_exoticas=pd.concat([sp_estimadas_exoticas,sp_exoticas])
    sp_estimadas_invasoras=pd.concat([sp_estimadas_invasoras,sp_invasoras])
    sp_estimadas_potencial=pd.concat([sp_estimadas_potencial,sp_potencial])
    sp_estimadas_endemicas=pd.concat([sp_estimadas_endemicas,sp_endemicas])
    sp_estimadas_migratorias=pd.concat([sp_estimadas_migratorias,sp_migratorias])
    
##---------------------Transformación número de especies por categoría taxonómica a número de especies por grupos biológicos----------------------------##
##Seleccionar las columnas del archivo grupos biologicos a utilizar
grupos_biologicost=grupos_biologicos[['grupo_id','grupoTax','taxonRank']]

## Resumen cifras taxones por grupo biológico general
mads_grupos_biologicos=pd.merge(sp_estimadas_mads,grupos_biologicost, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 
mads_grupos_biologicos=mads_grupos_biologicos[['taxones','grupoTax','grupo_id','thematic']]

cites_grupos_biologicos=pd.merge(sp_estimadas_cites,grupos_biologicost, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 
cites_grupos_biologicos=cites_grupos_biologicos[['taxones','grupoTax','grupo_id','thematic']]


iucn_grupos_biologicos=pd.merge(sp_estimadas_iucn,grupos_biologicost, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 
iucn_grupos_biologicos=iucn_grupos_biologicos[['taxones','grupoTax','grupo_id','thematic']]

exoticas_grupos_biologicos=pd.merge(sp_estimadas_exoticas,grupos_biologicost, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 
exoticas_grupos_biologicos=exoticas_grupos_biologicos[['taxones','grupoTax','grupo_id','thematic']]

invasoras_grupos_biologicos=pd.merge(sp_estimadas_invasoras,grupos_biologicost, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 
invasoras_grupos_biologicos=invasoras_grupos_biologicos[['taxones','grupoTax','grupo_id','thematic']]

potencial_grupos_biologicos=pd.merge(sp_estimadas_potencial,grupos_biologicost, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 
potencial_grupos_biologicos=potencial_grupos_biologicos[['taxones','grupoTax','grupo_id','thematic']]

endemicas_grupos_biologicos=pd.merge(sp_estimadas_endemicas,grupos_biologicost, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 
endemicas_grupos_biologicos=endemicas_grupos_biologicos[['taxones','grupoTax','grupo_id','thematic']]

migratorias_grupos_biologicos=pd.merge(sp_estimadas_migratorias,grupos_biologicost, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 
migratorias_grupos_biologicos=migratorias_grupos_biologicos[['taxones','grupoTax','grupo_id','thematic']]


#Especies total para la tematica por grupo biologico
mads_tem_gp=mads_grupos_biologicos.groupby('grupo_id').sum().reset_index()
cites_tem_gp=cites_grupos_biologicos.groupby('grupo_id').sum().reset_index()
iucn_tem_gp=iucn_grupos_biologicos.groupby('grupo_id').sum().reset_index()
exoticas_tem_gp=exoticas_grupos_biologicos.groupby('grupo_id').sum().reset_index().fillna('-') 
invasoras_tem_gp=invasoras_grupos_biologicos.groupby('grupo_id').sum().reset_index().fillna('-') 
potencial_tem_gp=potencial_grupos_biologicos.groupby('grupo_id').sum().reset_index().fillna('-') 
endemicas_tem_gp=endemicas_grupos_biologicos.groupby('grupo_id').sum().reset_index().fillna('-') 
migratorias_tem_gp=migratorias_grupos_biologicos.groupby('grupo_id').sum().reset_index().fillna('-') 



## Agrupar las cifras por grupo biológico y temática, y reordenar el conjunto de datos
taxon_tematica_cifras_mads=mads_grupos_biologicos.groupby(['grupo_id','thematic']).sum().reset_index()
taxon_tematica_cifras_mads=taxon_tematica_cifras_mads.pivot('grupo_id','thematic').fillna('-') 
taxon_tematica_cifras_mads=pd.merge(taxon_tematica_cifras_mads,mads_tem_gp, on=['grupo_id'],how='left')

taxon_tematica_cifras_cites=cites_grupos_biologicos.groupby(['grupo_id','thematic']).sum().reset_index()
taxon_tematica_cifras_cites=taxon_tematica_cifras_cites.pivot('grupo_id','thematic').fillna('-') 
taxon_tematica_cifras_cites=pd.merge(taxon_tematica_cifras_cites,cites_tem_gp, on=['grupo_id'],how='left')

taxon_tematica_cifras_iucn=iucn_grupos_biologicos.groupby(['grupo_id','thematic']).sum().reset_index()
taxon_tematica_cifras_iucn=taxon_tematica_cifras_iucn.pivot('grupo_id','thematic').fillna('-') 
taxon_tematica_cifras_iucn=pd.merge(taxon_tematica_cifras_iucn,iucn_tem_gp, on=['grupo_id'],how='left')



taxon_tematica_cifras_cites.columns=['slug_grupo','especies_cites_i_estimadas','especies_cites_ii_estimadas','especies_cites_i_ii_estimadas','especies_cites_iii_estimadas','especies_cites_total_estimadas']

taxon_tematica_cifras_mads.columns=['slug_grupo','especies_amenazadas_nacional_cr_estimadas','especies_amenazadas_nacional_en_estimadas','especies_amenazadas_nacional_vu_estimadas','especies_amenazadas_nacional_total_estimadas']

taxon_tematica_cifras_iucn.columns=['slug_grupo','especies_amenazadas_global_cr_estimadas','especies_amenazadas_global_en_estimadas','especies_amenazadas_global_vu_estimadas','especies_amenazadas_global_total_estimadas']

exoticas_tem_gp.columns=['slug_grupo','especies_exoticas_estimadas']
invasoras_tem_gp.columns=['slug_grupo','especies_invasoras_estimadas']
potencial_tem_gp.columns=['slug_grupo','especies_exoticas_riesgo_invasion_estimadas']
endemicas_tem_gp.columns=['slug_grupo','especies_endemicas_estimadas']
migratorias_tem_gp.columns=['slug_grupo','especies_migratorias_estimadas']

estimadas_total=taxon_tematica_cifras_cites.merge(taxon_tematica_cifras_mads, on='slug_grupo', how='outer').merge(taxon_tematica_cifras_iucn, on='slug_grupo', how='outer').merge(exoticas_tem_gp, on='slug_grupo', how='outer').merge(invasoras_tem_gp, on='slug_grupo', how='outer').merge(potencial_tem_gp, on='slug_grupo', how='outer').merge(endemicas_tem_gp, on='slug_grupo', how='outer').merge(migratorias_tem_gp, on='slug_grupo', how='outer').fillna('-') 

##Quitar los .0

estimadas_total=estimadas_total.astype(str)
estimadas_total=estimadas_total.replace(to_replace='\.0+$',value="",regex=True)
estimadas_total=estimadas_total.astype(str)
estimadas_total=estimadas_total.replace(to_replace='\.0+$',value="",regex=True)

##Exportar los archivo resultantes

#taxon_tematica_cifras_mads.to_excel(nombre_mads+'.xlsx', sheet_name='Estimadas amenazadas MADS', index=False)
#taxon_tematica_cifras_cites.to_excel(nombre_cites+'.xlsx', sheet_name='Estimadas CITES', index=False)
#taxon_tematica_cifras_iucn.to_excel(nombre_iucn+'.xlsx', sheet_name='Estimadas IUCN', index=False)
#exoticas_tem_gp.to_excel(nombre_exoticas+'.xlsx', sheet_name='Estimadas Exóticas', index=False)
#invasoras_tem_gp.to_excel(nombre_invasoras+'.xlsx', sheet_name='Estimadas invasoras', index=False)
#potencial_tem_gp.to_excel(nombre_potencial+'.xlsx', sheet_name='Estimadas Exóticas potencial', index=False)

estimadas_total.to_excel('estimadas.xlsx', sheet_name='Estimadas tematicas total', index=False)



