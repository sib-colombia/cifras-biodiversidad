# Biodiversidad en cifras nacional y ventanas regionales 
_Last update: 2021-10-14_
## Tabla de contenido
1. [Información general ](#descripcion-general)
2. [Requisitos ](#requisitos)
3. [Recursos adicionales ](#recursos)
4. [Ejecución ](#ejecucion)
5. [Posibles errores ](#errores)
6. [Derechos de autor y licencia ](#derechos)

## Información general
Estos scripts fueron creados por el SiB Colombia para la obtención de cifras y listas de especies a nivel municipal o departamental. Teniendo en cuenta cuatro  ejes principales grupos biológicos, áreas geográficas, temáticas de conservación, uso y manejo y organizaciones publicadoras.

El archivo que se utilice para la ejecución de los scripts debe haber pasado por un proceso de validación, limpieza 
y una revisión del equipo de trabajo para asegurar la presencia de los campos necesarios para la obtención de cada una 
de las cifras. Adicionalmente, se debe contar con los archivos complementarios, correspondientes a DIVIPOLA y listas de referencia: CITES, IUCN, especies amenazadas MADS (resolución 1912 de 2017), GRIIS y listas de referencia grupos biologicos (Aves, peces, mamíferos y plantas).

Para la obtención de las cifras y listas de especies nacionales y regionales se deben ejecutar los script generadorCifras_2021.py y generadorListaEspecies_2021.py

## Requisitos

* Se sugiere realizar la ejecución en un equipo con una memoria RAM de 8 gb o más, esto con el fin de minimizar la posibilidad de errores por capacidad del equipo.
* Tener instalado Python preferiblemente en la última versión disponible y un editor de código
* Tener todos lor archivos necesarios en una sola carpeta de trabajo
* Es necesario que el conjunto de datos utilizado para la ejecución de estos scripts allá pasado por un proceso de limpieza y depuración previo que asegure la presencia de las columnas relacionadas a las diferentes tematicas de interes y la completitud de la información taxonomica. 

## Recursos adicionales
Es necesario contar con los siguientes archivos:
* Archivo con información DIVIPOLA. Disponible en: [Biodiversidad En Cifras: Scripts-cifras](https://gitlab.com/sib-colombia/administracion-contenidos/-/tree/master/Scripts-cifras) 
* Archivo con grupos biologicos: Disponible en: [Biodiversidad En Cifras: Grupos biologicos](https://gitlab.com/sib-colombia/administracion-contenidos/-/tree/master/Scripts-cifras/gruposBiologicos) 

En el caso de ventanas regionales utilice el archivo: **gruposBiologicosCifrasSiB_VRC.txt** y para cifras nacionales: **gruposBiologicosCifrasSiB_G.txt**

***
**Para mayor detalle consulte los siguientes documentos:**
* [Biodiversidad En Cifras: Ficha metodológica](https://example.com): V2.1 (2021)
* [Biodiversidad en Cifras: Criterios limpieza](https://example.com): V1 (2021)

## Ejecución

### Generador cifras
El script **generadorCifras_2021.py** permite obtener las cifras temáticas, geograficas, por entidades publicadoras y grupos biologicos.
Adicionalmente, en el caso de conjuntos de datos que contengan información de registros marinos se encontrara información sobre los diferentes habitats (terrestre, salobre y marino). Por lo cual, el script cruzara las tematicas, geografia, entidades publicadoras y grupos biologicos con la información de habitats y generara cifras para cada uno de estos. 

### Generador lista de especies
El script **generadorListaEspecies_2021.py** producira una lista de especies con la información de municipio, departamento, número de registros y categorias tematicas. 
En el caso de conjuntos de datos que contengan información de registros marinos se encontrara información sobre los diferentes habitats (terrestre, salobre y marino) y el número de registros de las especie en cada uno de los habitats. 


## Posibles errores
A continuación se listan algunos de los errores que podrian presentarse al ejecutar los scripts y las posibles soluciones:

### Falta de memoria 
Debido a la gran cantidad de registros que contienen estos conjuntos de datos es posible que se presente el siguiente error:
**"pandas.errors.ParserError: Error tokenizing data. C error: out of memory"**

Una forma de evitarlo es asignar el tipo de dato que tiene cada una de las columnas del conjunto de datos. Por lo tanto, es necesario revisar que las columnas utilizadas se encuentren incluidas al momento de cargar el archivo. Adicionalmente, debe tener en cuenta el tipo de dato:     
* str: Se utiliza para campos que contengan texto y simbolos
* float: Aplica en campos que contengan números decimales

### Tipo de dato erroneo

Este error puede presentarse por dos razones:
* El tipo de dato asignado a la columna no es el adecuado. Por lo cual, es importante asegurarse de asignar el tipo acorde a la información contenida.
* Dentro de la información existe algún error de tipeo o información que no corresponde a la columna. Ej. year: "Sin coordenadas". Por lo tanto, se debe hacer una limpieza previa al conjunto de datos.


## Autores
**Equipo coordinador SiB-Colombia**

Camila Andrea Plata Corredor

Nerieth Goretti Leuro Robles

## Derechos de autor y licencia
Estos documentos se publica bajo una licencia Creative Commons Attribution 4.0
