# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 18:54:00 2019

@author: ricardo.ortiz
"""
#%% 
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
import json
import time
import requests
from pandas.io.json import json_normalize
import numpy as np

# Load csv-SIMPLE (interpreted) 
dwi = pd.read_table("C:\\Cifras_Colombia\\2021\\Datos_Trimetrales\\TrimestreIV\\0104744-210914110416597.csv", encoding = "utf8", sep= "\t", quoting=3, 
       usecols = ['occurrenceID','basisOfRecord','institutionCode','collectionCode','catalogNumber','recordedBy','individualCount','eventDate','countryCode','stateProvince','locality','elevation','depth','decimalLatitude','decimalLongitude','coordinateUncertaintyInMeters','identifiedBy','dateIdentified','scientificName','kingdom','phylum','class','order','family','genus','species','infraspecificEpithet','taxonRank','day','month','year','verbatimScientificName','gbifID','datasetKey','publishingOrgKey','taxonKey', 'issue' ])                                

# Load occurrece-DwCA 
dwo = pd.read_table("C:\\Cifras_Colombia\\2021\\Datos_Trimetrales\\TrimestreIV\\occurrence.txt", encoding = "utf8", sep= "\t", quoting=3,
       usecols = ['type','datasetID','datasetName','organismQuantity','organismQuantityType','sex','lifeStage','preparations','disposition','eventID','samplingProtocol','samplingEffort','habitat','waterBody','county','municipality','identificationQualifier','vernacularName','gbifID','repatriated','publishingCountry'])

# Join the both datasets through GBIFID
dwm=pd.merge(dwi,dwo,on='gbifID')

#STOP... Remove the dwi and dwo variables from the explorer
del dwi
del dwo

#Number of rows and columns documented for Integrated datasets
#eBird
dwm[(dwm.datasetKey == '4fa7b334-ce0d-4e88-aaae-2e0c138d049e')].count()
#iNat - count by GBIFID
dwm[(dwm.datasetKey == 'b1047888-ae52-4179-9dd5-5448ea342a24')].count()
#Xeno-Canto - count by GBIFID
dwm[(dwm.datasetKey == '50c9509d-22c7-4a22-a47d-8c48425ef4a7')].count()
#Repatriated - GBIF - count by GBIFID
dwm[(dwm.publishingCountry != 'CO') & (dwm.datasetKey != 'b1047888-ae52-4179-9dd5-5448ea342a24') & (dwm.datasetKey != '50c9509d-22c7-4a22-a47d-8c48425ef4a7') ].count()

#%% 


# Split the columns to keep only the necesary for furhter process.
dwm_hold=dwm[[ 'gbifID', 'occurrenceID', 'basisOfRecord', 'type', 'institutionCode', 'collectionCode', 'catalogNumber',  'datasetID', 'recordedBy', 'individualCount', 'organismQuantity', 'organismQuantityType', 'sex', 'lifeStage', 'preparations', 'disposition', 'eventID', 'samplingProtocol','samplingEffort', 'eventDate', 'year', 'month', 'day','habitat','waterBody', 'municipality', 'locality',  'elevation',  'depth',  'coordinateUncertaintyInMeters', 'identifiedBy','dateIdentified','identificationQualifier',  'infraspecificEpithet', 'verbatimScientificName', 'vernacularName', 'publishingCountry', 'repatriated', 'issue']]

#Save de dwme_hold file, it reduce weigh to the memory but it will be used later to consolidate the whole dataset. Once you finish all the proces you can delete it.
#REMEMBER to use other file directory to avoid overwrite the file
dwm_hold.to_csv('C:\\Cifras_Colombia\\2021\\Datos_Trimetrales\\TrimestreIV\\dwme_hold_20211231.txt', sep="\t", encoding = "utf8")

# Neccesary columns to continue with the script
dwm=dwm[['gbifID', 'countryCode', 'stateProvince', 'county', 'decimalLatitude', 'decimalLongitude', 'scientificName', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species', 'taxonRank', 'datasetKey', 'publishingOrgKey']]

del dwm_hold


#%% 

#Translate the taxonRank element
dwm['taxonRank']=dwm['taxonRank'].replace('SPECIES', 'Especie').replace('SUBSPECIES', 'Subespecie').replace('GENUS', 'Género').replace('FAMILY', 'Familia').replace('ORDER', 'Orden').replace('CLASS', 'Clase').replace('PHYLUM', 'Filo').replace('KINGDOM', 'Reino').replace('FORM', 'Forma').replace('VARIETY', 'Variedad').replace('UNRANKED', '')
#Translate verification
dwc_co.groupby(['taxonRank']).gbifID.nunique().reset_index()

# Prueba para validar que no haya "species" vacios si el taxonRank es especie
dwm['pruebaSpecies'] = np.where((dwm['taxonRank'] == 'Especie') & (dwm['species'].isnull()), 'true', 'false')
dwm.groupby(['pruebaSpecies']).gbifID.nunique().reset_index()
taxoIssue = dwm[(dwm['pruebaSpecies'] =='true')]


#Corregir error de GBIF especie sin species 
dwm.loc[dwm.scientificName == 'Phlegopsis barringeri Meyer de Schauensee, 1951', 'species'] = 'Phlegopsis barringeri'
dwm.loc[dwm.scientificName == 'Hansenium stebbingi (Richardson, 1902)', 'species'] = 'Hansenium stebbingi'
dwm.loc[dwm.scientificName == 'Scytalopus sanctamartae Chapman, 1915', 'species'] = 'Scytalopus sanctamartae'
dwm.loc[dwm.scientificName == 'Thaumasia niceforoi Mello-Leitão, 1941', 'species'] = 'Thaumasia niceforoi'
dwm.loc[dwm.scientificName == 'Stenetrium stebbingi Richardson, 1902', 'species'] = 'Stenetrium stebbingi'

#Vuelva a correr los pasos de la línea 80 y verifique que taxoIssue se genere sin registros, sino identifique las especies y haga la validación.
dwm = dwm.drop(['pruebaSpecies'], axis=1)
del taxoIssue


#%% 


#Load thematic list
#CITES
cites = pd.read_table('C:\\Cifras_Colombia\\2021\\ListasTaxo\\Tematicas\\ListaCITES_2021_ajustadoRO.txt', usecols =['species','appendixCITES','taxonRankSuggested'])
cites = cites[(cites['taxonRankSuggested'] == 'SPECIES')].drop(['taxonRankSuggested'],axis = 1)
#UICN
uicn = pd.read_table('C:\\Cifras_Colombia\\2021\\ListasTaxo\\Tematicas\\Lista_UICNredList_2021_ajustadoRO.txt', usecols = ['threatStatus_UICN', 'species'])
#Amenazadas MADS
MADS_am = pd.read_table('C:\\Cifras_Colombia\\2021\\ListasTaxo\\Tematicas\\Lista_amenazadas_resolucion1912_2017_2021.txt', usecols = ['threatStatus', 'species','taxonRankSuggested'])
MADS_am = MADS_am[(MADS_am['taxonRankSuggested'] == 'SPECIES')].drop(['taxonRankSuggested'],axis = 1)
MADS_am.rename(columns={'threatStatus': 'threatStatus_MADS'}, inplace=True)
#Invasoras MADS
MADS_inv = pd.read_table('C:\\Cifras_Colombia\\2021\\ListasTaxo\\Tematicas\\Lista_InvasorasMADS_2010_2021.txt', usecols = ['isInvasive_mads', 'species','taxonRankSuggested'])
MADS_inv = MADS_inv[(MADS_inv['taxonRankSuggested'] == 'SPECIES')].drop(['taxonRankSuggested'],axis = 1)
#GRIIS
griis = pd.read_table('C:\\Cifras_Colombia\\2021\\ListasTaxo\\Tematicas\\Lista_GRIIS_2018_2021_ajustadoRO.txt', usecols = ['isInvasive_griis', 'establishmentMeans', 'species','taxonRankSuggested'])
griis = griis[(griis['taxonRankSuggested'] == 'SPECIES')].drop(['taxonRankSuggested'],axis = 1)
griis.rename(columns={'establishmentMeans': 'exotic'}, inplace=True)
#Listas de referencia taxonómica
referencia = pd.read_table('C:\\Cifras_Colombia\\2021\\ListasTaxo\\Referencia\\Listas_taxonomicasCol_2020.txt', usecols =['species','endemic','migratory','taxonRank','File', 'isTerrestrial', 'isFreshwater',	'isMarine',	'isBrackish','isHybrid'])
referencia = referencia[(referencia['taxonRank'] == 'Especie')].drop(['taxonRank'],axis = 1)
referencia = referencia[referencia['species'].notnull()]
referencia = referencia.replace({'File':{'mamiferos_col_2020.csv' : 'doi.org/10.15472/kl1whs', 'taxon_catalogo_plantas_liquenes.csv' : 'doi.org/10.15472/7avdhn', 'taxon_listaAves_2020.csv':'doi.org/10.15472/qhsz0p', 'taxon_listaPecesDulce_2020.csv': 'doi.org/10.15472/numrso' }})
referencia.rename(columns={'File': 'listaReferenciaCO'}, inplace=True)

#Cruces con archivo de datos.
dwm=pd.merge(left=dwm,right=cites, how= 'left', left_on='species', right_on='species')
dwm=pd.merge(left=dwm,right=uicn, how= 'left', left_on='species', right_on='species')
dwm=pd.merge(left=dwm,right=MADS_am, how= 'left', left_on='species', right_on='species')
dwm=pd.merge(left=dwm,right=MADS_inv, how= 'left', left_on='species', right_on='species')
dwm=pd.merge(left=dwm,right=griis, how= 'left', left_on='species', right_on='species')
#Eliminar el valor criptogénica en la lista temática de griis
dwm['exotic'] = np.where((dwm['exotic'] =='Criptogénica') ,'', dwm['exotic'])
dwm=pd.merge(left=dwm,right=referencia, how= 'left', left_on='species', right_on='species')


#Flag Taxonómico (Solo para aves, plantas y mamíferos, para peces se descartó debido a la complejidad de diferenciar peces dulceacuícolas y marinos)
dwm['flagTAXO'] = np.where((dwm['listaReferenciaCO'].isnull()) & (dwm['species'].notnull()) & (dwm['kingdom'] =='Plantae') , 'Especie no coincide con listas taxonómicas de referencia', '')
dwm['flagTAXO'] = np.where((dwm['listaReferenciaCO'].isnull()) & (dwm['species'].notnull()) & (dwm['class'] =='Aves') , 'Especie no coincide con listas taxonómicas de referencia', dwm['flagTAXO'])
dwm['flagTAXO'] = np.where((dwm['listaReferenciaCO'].isnull()) & (dwm['species'].notnull()) & (dwm['class'] =='Mammalia') , 'Especie no coincide con listas taxonómicas de referencia', dwm['flagTAXO'])

#Verificación Flag
dwm.groupby(['flagTAXO']).gbifID.nunique().reset_index()
dwm.groupby(['flagTAXO']).species.nunique().reset_index()


#Mammalia especies no válidadas lista taxo = 134
#Aves especies no válidadas lista taxo = 267
#plantae especies no válidadas lista taxo= 6683


del MADS_am
del MADS_inv
del cites
del griis
del uicn
del referencia


#%% 

#dwc_co = pd.read_csv('C:\\Cifras_Colombia\\2017\\0031527-180131172636756.csv', encoding = "utf8", sep="\t", quoting=3,)

#Geographic spatial join. Get stateProvince and county according to DANE MGN
#llenar coordenadas vacías con 0's
dwm[['decimalLatitude', 'decimalLongitude']] = dwm[['decimalLatitude','decimalLongitude']].fillna(value=0)
dwm['Coordinates'] = list(zip(dwm.decimalLongitude, dwm.decimalLatitude))
dwm['Coordinates'] = dwm['Coordinates'].apply(Point)
dwm = gpd.GeoDataFrame(dwm, geometry='Coordinates')
dwm.crs = {'init' :'epsg:4326'}
#Remove dwme

#Split dataset for faster procesing

dwm_g1 = dwm[(dwm['class'] =='Aves')]
dwm_g2 = dwm[(dwm['class'] !='Aves')]
del dwm

#%% 

#Importar archivo shapefile (revisar la codificación de la capa y el sistema de referencia) preferiblemente que esté ya en WGS84, sino hay que realizar una conversión para que funcione el spatialJoin
colombiaMGN =gpd.read_file('C:\\ValidacionGeografica_SiB-QGIS\\MGN_MPIO_POLITICO_2020.shp', encoding = "utf8")
dwm_g1 = gpd.sjoin(dwm_g1, colombiaMGN, how="left")
dwm_g2 = gpd.sjoin(dwm_g2, colombiaMGN, how="left")
dwme_g=pd.concat([dwm_g1,dwm_g2], sort=False)

del dwm_g1
del dwm_g2

dwme_g = dwme_g.drop(['DPTO_CCDGO', 'MPIO_CCDGO', 'MPIO_CNMBR', 'MPIO_CRSLC', 'MPIO_NAREA', 'MPIO_CCNCT', 'MPIO_NANO','SHAPE_AREA', 'SHAPE_LEN', 'DPTO_CNMBR','Coordinates','index_right'], axis=1)
dwme_g.rename(columns={'suggestedS': 'Departamento-ubicacionCoordenada'}, inplace=True)
dwme_g.rename(columns={'suggestedC': 'Municipio-ubicacionCoordenada'}, inplace=True)

#%% 

#dwc_co = pd.read_csv('C:\\Cifras_Colombia\\2021\\Datos_Trimetrales\\TrimestreI\\dwc_co_20210331_provisionalGEO.txt', encoding = "utf8", sep="\t")


#Cruce con capa de áras marítimas para validar correctamente registros que coinciden con estas áreas
colombiaMar =gpd.read_file('C:\\ValidacionGeografica_SiB-QGIS\\RegionesMaritimas.shp', encoding = "utf8")
dwme_gNO = dwme_g[(dwme_g['Municipio-ubicacionCoordenada'].isnull())]
dwme_gNO[['decimalLatitude', 'decimalLongitude']] = dwme_gNO[['decimalLatitude','decimalLongitude']].fillna(value=0)
dwme_gNO['Coordinates'] = list(zip(dwme_gNO.decimalLongitude, dwme_gNO.decimalLatitude))
dwme_gNO['Coordinates'] = dwme_gNO['Coordinates'].apply(Point)
dwme_gNO = gpd.GeoDataFrame(dwme_gNO, geometry='Coordinates')
dwme_gNO.crs = {'init' :'epsg:4326'}
dwme_gNO = gpd.sjoin(dwme_gNO, colombiaMar, how="left")
#Validación de resultados del cruce
dwme_gNO.groupby(['DESCRIP']).gbifID.nunique().reset_index()
dwme_gNO.rename(columns={'DESCRIP': 'ZonaMaritima'}, inplace=True)
dwme_gNO=dwme_gNO.drop([ 'countryCode', 'stateProvince', 'county', 'decimalLatitude', 'decimalLongitude', 'scientificName', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species', 'taxonRank', 'datasetKey', 'publishingOrgKey', 'appendixCITES', 'threatStatus_UICN', 'threatStatus_MADS', 'isInvasive_mads', 'isInvasive_griis', 'exotic', 'listaReferenciaCO', 'endemic', 'migratory', 'flagTAXO', 'Departamento-ubicacionCoordenada', 'Municipio-ubicacionCoordenada', 'Coordinates', 'index_right', 'OBJECTID'], axis=1)
dwme_g=pd.merge(dwme_g, dwme_gNO, on='gbifID',how='left')
#10735128 / 78

del colombiaMar
del dwme_gNO


#%% 

# Create a stateProvinceValidation element based on the coordinate location and original statePronvince text documentation.
dwme_g['stateProvinceValidation'] = np.where((dwme_g['stateProvince'] == dwme_g['Departamento-ubicacionCoordenada']) , 'true', 'false')
#Deja vacío el campo de validación si el stateProvince original no está documentado
dwme_g['stateProvinceValidation'] = np.where((dwme_g['stateProvinceValidation'] == 'false') & (dwme_g['stateProvince'].isnull()), '', dwme_g['stateProvinceValidation'])
#Deja vacío el campo de validación si no hay coordenadas documentadas
dwme_g['stateProvinceValidation'] = np.where((dwme_g['stateProvinceValidation'] == 'false') & (dwme_g['decimalLatitude'] == 0) & (dwme_g['decimalLongitude'] == 0), '', dwme_g['stateProvinceValidation'])
#Deja vacío el campo de validación si la coordenada está en área marítima
dwme_g['stateProvinceValidation'] = np.where((dwme_g['stateProvinceValidation'] == 'false') & (dwme_g['ZonaMaritima'].notnull())  , '', dwme_g['stateProvinceValidation'])
#Verificación de resultados
dwme_g.groupby(['stateProvinceValidation']).gbifID.nunique().reset_index()


# Create a countyValidation element based on the coordinate location and original statePronvince text documentation.
dwme_g['countyValidation'] = np.where((dwme_g['county'] == dwme_g['Municipio-ubicacionCoordenada']) , 'true', 'false')
#Deja vacío el campo de validación si el stateProvince original no está documentado
dwme_g['countyValidation'] = np.where((dwme_g['countyValidation'] == 'false') & (dwme_g['county'].isnull()), '', dwme_g['countyValidation'])
#Deja vacío el campo de validación si no hay coordenadas documentadas
dwme_g['countyValidation'] = np.where((dwme_g['countyValidation'] == 'false') & (dwme_g['decimalLatitude'] == 0) & (dwme_g['decimalLongitude'] == 0), '', dwme_g['countyValidation'])
#Deja vacío el campo de validación si la coordenada está en área marítima
dwme_g['countyValidation'] = np.where((dwme_g['countyValidation'] == 'false') & (dwme_g['ZonaMaritima'].notnull())  , '', dwme_g['countyValidation'])
#Verificación de resultados
dwme_g.groupby(['countyValidation']).gbifID.nunique().reset_index()


#Create flag column with geographic issues based on stateProvince, county and maritime validations
dwme_g['flagGEO'] = np.where((dwme_g['stateProvinceValidation'] == 'false') & (dwme_g['countyValidation']=='false'), 'Departamento y municipio no coinciden con ubicación de la coordenada', '')
dwme_g['flagGEO'] = np.where((dwme_g['flagGEO']== '') & (dwme_g['countyValidation']=='false') & (dwme_g['stateProvinceValidation'] == 'true'), 'Municipio no coincide con ubicación de la coordenada', dwme_g['flagGEO'])
dwme_g['flagGEO'] = np.where((dwme_g['flagGEO']== '') & (dwme_g['countyValidation']=='true') & (dwme_g['stateProvinceValidation'] == 'false'), 'Departamento no coincide con ubicación de la coordenada', dwme_g['flagGEO'])
dwme_g['flagGEO'] = np.where((dwme_g['flagGEO']== '') & (dwme_g['countyValidation']=='true') & (dwme_g['stateProvinceValidation'] == 'false'), 'Departamento no coincide con ubicación de la coordenada', dwme_g['flagGEO'])
dwme_g['flagGEO'] = np.where((dwme_g['flagGEO']== '') & (dwme_g['countyValidation']== '') & (dwme_g['stateProvinceValidation']== '') & (dwme_g['ZonaMaritima'].notnull()), 'Coordenada ubicada en área marítima', dwme_g['flagGEO'])
dwme_g['flagGEO'] = np.where((dwme_g['flagGEO']== '') & (dwme_g['countyValidation']== '') & (dwme_g['stateProvinceValidation']== '') & (dwme_g['decimalLatitude'] == 0) & (dwme_g['decimalLongitude'] == 0), 'Sin coordenadas', dwme_g['flagGEO'])

#Verificación de resultados
dwme_g.groupby(['flagGEO']).gbifID.nunique().reset_index()
 #dwme_gVerificacion = dwme_g[['gbifID','stateProvince','stateProvinceValidation','Departamento-ubicacionCoordenada','county','countyValidation','Municipio-ubicacionCoordenada','decimalLatitude','decimalLongitude','ZonaMaritima','flagGEO']]

#%% 

#Call the GBIF API to get organization and dataset information
dwme_dk=dwme_g[["gbifID","datasetKey"]]# this is the dataset
df=dwme_dk.drop_duplicates('datasetKey',inplace=False) # work with unique values
#Define the gbif API call for datasetKey
def call_gbif(row):#configure the API call
    try:
        url = "http://api.gbif.org/v1/dataset/"+ str(row['datasetKey'])#define the URL with the column for the call
        
        response = (requests.get(url).text)
        response_json = json.loads(response)
        time.sleep(0.005)
        return response_json
    
    except Exception as e:
        raise e

df['API_response'] = df.apply(call_gbif,axis=1)#aply the call
norm_dk = json_normalize(df['API_response']) #convert the respone in a table
dk = norm_dk[['key','license','doi','title','created','logoUrl','type']]#define the elements you need
dk.rename(columns={'title': 'datasetTitle'}, inplace=True)#rename columns
dk.rename(columns={'type': 'dataType'}, inplace=True)#rename columns
dk.rename(columns={'key': 'datasetKey'}, inplace=True)#rename columns

#%% 

#Call the GBIF API to get organization and dataset information
dwme_ok=dwme_g[["gbifID","publishingOrgKey"]]
dfo=dwme_ok.drop_duplicates('publishingOrgKey',inplace=False)
#Define the gbif API call for publishingOrgKey
def call_gbif(row):
    try:
        url = "http://api.gbif.org/v1/organization/"+ str(row['publishingOrgKey'])
        
        response = (requests.get(url).text)
        response_json = json.loads(response)
        time.sleep(0.005)
        return response_json
    
    except Exception as e:
        raise e



dfo['API_response'] = dfo.apply(call_gbif,axis=1)
norm_ok = json_normalize(dfo['API_response'])
ok = norm_ok[['key','title']]
ok.rename(columns={'title': 'organization'}, inplace=True)
ok.rename(columns={'key': 'publishingOrgKey'}, inplace=True)

#merge API call's to dataset
dwc_co=pd.merge(dwme_g,dk,on='datasetKey')
dwc_co=pd.merge(dwc_co,ok,on='publishingOrgKey')

# Remove unnecesary files
del df
del dfo
del dk
del dwme_dk
del dwme_g
del dwme_ok
del norm_dk
del norm_ok
del ok
#%% 
#Remove data type Checklist (occurrence extention) and Metadata (No idea what is doing here)
dwc_co=dwc_co[dwc_co.dataType != 'CHECKLIST']
dwc_co=dwc_co[dwc_co.dataType != 'METADATA']

#Revisar registros de metadatos y listas
#dwc_co_clist=dwc_co[dwc_co.dataType == 'CHECKLIST']
#dwc_co_meta=dwc_co[dwc_co.dataType == 'METADATA']

#%%
dwc_co = pd.read_csv('C:\\Cifras_Colombia\\2021\\Datos_Trimetrales\\TrimestreIV\\dwc_co_2021T4_preview.txt', encoding = "utf8", sep="\t")

#Merge the whole dataset again
#Load rest of the data
dwme_hold = pd.read_csv('C:\\Cifras_Colombia\\2021\\Datos_Trimetrales\\TrimestreIV\\dwme_hold_20211231.txt', encoding = "utf8", sep="\t")
dwc_co=pd.merge(left=dwc_co,right=dwme_hold, how= 'left', left_on='gbifID', right_on='gbifID')
#dwme_gNO=dwme_gNO.drop([ 'Unnamed: 0'], axis=1)

del dwme_hold


#%% 
# Add and modify necesary elements to the dwm dataset
dwc_co['institutionID'] = 'https://www.gbif.org/publisher/' + dwc_co['publishingOrgKey']
#dwc_co['occurrenceCitation'] = 'https://www.gbif.org/occurrence/' + dwc_co['gbifID'].astype(str)
#dwc_co['geodeticDatum']='WGS84'
#dwc_co['taxonID'] = 'https://www.gbif.org/species/' + dwc_co['taxonKey'].astype(str)
dwc_co['country'] = np.where((dwc_co['countryCode'] == 'CO'), 'Colombia', '')
#dwc_co['verbatimLocality']=dwc_co['countryCode'].astype(str)+ ' | '+dwc_co['stateProvince'].astype(str)+ ' | '+dwc_co['county'].astype(str)+ ' | '+dwc_co['municipality'].astype(str)+ ' | '+dwc_co['locality'].astype(str)
#dwc_co['minimumElevationInMeters']=dwc_co['elevation']
#dwm['maximumElevationInMeters']=dwm['elevation']
#dwc_co['minimumDepthInMeters']=dwc_co['depth']
#dwm['maximumDepthInMeters']=dwm['depth']
#Split the species element to various elements. The second split (species 2) is taken as specificEpithet (Verify!!!!)
#dwc_co['species1'],dwc_co['species2'],dwc_co['species3'],dwc_co['species4'] = dwc_co['species'].str.split(' ', 4).str
#dwc_co = dwc_co.drop(['species1','species3','species4'], axis=1)
#dwc_co.rename(columns={'species2': 'specificEpithet'}, inplace=True)
#Rename scientificName (gbif interpreted with authorship) as acceptedNameUsage
#dwc_co.rename(columns={'scientificName': 'acceptedNameUsage'}, inplace=True)
#Change values according to controled vocabulary
dwc_co['basisOfRecord']=dwc_co['basisOfRecord'].replace('HUMAN_OBSERVATION', 'HumanObservation').replace('PRESERVED_SPECIMEN', 'PreservedSpecimen').replace('MACHINE_OBSERVATION', 'MachineObservation').replace('FOSSIL_SPECIMEN', 'FossilSpecimen').replace('MATERIAL_SAMPLE', 'MaterialSample').replace('LIVING_SPECIMEN', 'LivingSpecimen').replace('OBSERVATION', 'HumanObservation').replace('LITERATURE', 'Occurrence').replace('UNKNOWN', 'Occurrence')
#Standarize and translate the Type element. Verify the unique values in Type in case you need to add more lines to the scipt ;)
dwc_co['type'].unique() # See the results in console
#Make voabulary changes in type element
dwc_co['type']=dwc_co['type'].replace('PhysicalObject', 'Objeto físico').replace('Image', 'Imagen estática').replace('Event', 'Evento').replace('Physical Object', 'Objeto físico').replace('collection', 'Objeto físico').replace('Collection', 'Objeto físico').replace('Sound', 'Sonido').replace('text', 'Texto').replace('Text', 'Texto').replace('StillImage', 'Imagen estática').replace('http://purl.org/dc/dcmitype/PhysicalObject', 'Objeto físico').replace('http://purl.org/dc/dcmitype/StillImage', 'Imagen estática').replace('Dataset', '').replace('event', 'Evento').replace('Colletion', 'Objeto físico').replace('Physical object', 'Objeto físico').replace('physicalObject', 'Objeto físico').replace('Preserved Specimen [en]', 'Objeto físico').replace('physical object', 'Objeto físico').replace('"Evento; Sonido"', 'Sonido').replace('"Evento;Sonido"', 'Sonido').replace('"Sonido;Evento"', 'Sonido').replace('nan', '').replace('Objeto Fisico', 'Objeto físico').replace('Imagen Estática', 'Imagen estática').replace('evento', 'Evento').replace('Imagén estática', 'Imagen estática').replace('Evento; Sonido', 'Sonido').replace('Imagen movimiento', 'Imagen en movimiento').replace('MovingImage', 'Imagen en movimiento').replace('Imagen estatica', 'Imagen estática').replace('Imajen fija', 'Imagen estática').replace('Objeto f’sico', 'Objeto físico').replace('Imágen', 'Imagen').replace('Grabación (track 40)', 'Sonido').replace('Grabación (track 52)', 'Sonido').replace('Grabación (track 46)', 'Sonido').replace('Grabación (track 54)', 'Sonido').replace('Grabación (track 45, 48,49,51)', 'Sonido').replace('Grabación (track 43)', 'Sonido').replace('es', '').replace('Vocalización', 'Evento').replace('Avistamiento/Vocalización', 'Evento').replace('Avistamiento', 'Evento').replace('Especimen preservado', 'Objeto físico').replace('Objeto Físico', 'Objeto físico').replace('Objeto fisico', 'Objeto físico').replace('SonidoObjeto físico', 'Objeto físico').replace('Imagen', 'Imagen estática').replace('Imagen en Movimiento', 'Imagen en movimiento').replace('Objeto f�sico', 'Objeto físico').replace('Video', 'Imagen en movimiento').replace('Imagen fija', 'Imagen estática').replace('Basidiomycete | PhysicalObject', 'Objeto físico').replace('Fern ally | PhysicalObject', 'Objeto físico').replace('Fern | PhysicalObject', 'Objeto físico').replace('Musci | PhysicalObject', 'Objeto físico').replace('PreservedSpecimen', 'Objeto físico').replace('Specimen', 'Objeto físico').replace('Static image', 'Imagen estática')
dwc_co.groupby(['type']).gbifID.nunique().reset_index()
#Make voabulary changes in sex element
dwc_co['sex'].unique() # See the results in console
dwc_co['sex']=dwc_co['sex'].replace('MALE', 'Macho').replace('FEMALE', 'Hembra').replace('HERMAPHRODITE', 'Hermafrodita')
#Make voabulary changes in lifeStage element
#dwc_co['lifeStage'].unique() # See the results in console
#dwc_co['lifeStage']=dwc_co['lifeStage'].replace('ADULT', 'Adulto').replace('LARVA', 'Larva').replace('JUVENILE', 'Juvenil').replace('PUPA', 'Pupa')

#Organize date
dwc_co['eventDate1'],dwc_co['eventDate2']= dwc_co['eventDate'].str.split('T', 2).str
dwc_co = dwc_co.drop(['eventDate2', 'eventDate'], axis=1)
dwc_co.rename(columns={'eventDate1': 'eventDate'}, inplace=True)
#dwc_co['dateIdentified1'],dwc_co['dateIdentified2']= dwc_co['dateIdentified'].str.split('T', 2).str
#dwc_co = dwc_co.drop(['dateIdentified2', 'dateIdentified'], axis=1)
#dwc_co.rename(columns={'dateIdentified1': 'dateIdentified'}, inplace=True)

#Eliminar .0 al final de valores numéricos y borrar coordenadas en 0,0 de la validación geográfica.
#dwc_co['individualCount']=dwc_co['individualCount'].astype(str).replace(to_replace='\.0+$',value='',regex=True).replace('nan','')
#dwc_co['organismQuantity']=dwc_co['organismQuantity'].astype(str).replace(to_replace='\.0+$',value='',regex=True).replace('nan','')
#dwc_co['day']=dwc_co['day'].astype(str).replace(to_replace='\.0+$',value='',regex=True).replace('nan','')
#dwc_co['month']=dwc_co['month'].astype(str).replace(to_replace='\.0+$',value='',regex=True).replace('nan','')
#dwc_co['year']=dwc_co['year'].astype(str).replace(to_replace='\.0+$',value='',regex=True).replace('nan','')
#dwc_co['minimumElevationInMeters']=dwc_co['minimumElevationInMeters'].astype(str).replace(to_replace='\.0+$',value='',regex=True).replace('nan','')
#dwc_co['elevation']=dwc_co['elevation'].astype(str).replace(to_replace='\.0+$',value='',regex=True).replace('nan','')
#dwc_co['minimumDepthInMeters']=dwc_co['minimumDepthInMeters'].astype(str).replace(to_replace='\.0+$',value='',regex=True).replace('nan','')
#dwc_co['depth']=dwc_co['depth'].astype(str).replace(to_replace='\.0+$',value='',regex=True).replace('nan','')

#Eliminar coordenadas 0.0 en decimalLatitude y decimalLongitude creadas para los cruces geográficos.
dwc_co['decimalLatitude'] = np.where((dwc_co['flagGEO']== 'Sin coordenadas') & (dwc_co['decimalLatitude'] == 0) & (dwc_co['decimalLatitude'] == 0), '', dwc_co['decimalLatitude'])
dwc_co['decimalLongitude'] = np.where((dwc_co['flagGEO']== 'Sin coordenadas') & (dwc_co['decimalLongitude'] == 0) & (dwc_co['decimalLongitude'] == 0), '', dwc_co['decimalLongitude'])

#dwc_prueba=dwc_co[['gbifID','individualCount','organismQuantity','day', 'month', 'year','minimumElevationInMeters','elevation','minimumDepthInMeters','depth','decimalLatitude','decimalLongitude', 'flagGEO']]


#Create a scientificName element without Authorship based in the taxonRank value
#dwm.loc[dwm.taxonRank == 'Reino', 'scientificName'] = dwm['kingdom']
#dwm.loc[dwm.taxonRank == 'Filo', 'scientificName'] = dwm['phylum']
#dwm.loc[dwm.taxonRank == 'Clase', 'scientificName'] = dwm['class']
#dwm.loc[dwm.taxonRank == 'Orden', 'scientificName'] = dwm['order']
#dwm.loc[dwm.taxonRank == 'Familia', 'scientificName'] = dwm['family']
#dwm.loc[dwm.taxonRank == 'Género', 'scientificName'] = dwm['genus']
#dwm.loc[dwm.taxonRank == 'Especie', 'scientificName'] = dwm['species'] # Carefull, sometimes the species could be empty, its a GBIF issue. Needs to be checked out of the script 
#dwm.loc[dwm.taxonRank == 'Subespecie', 'scientificName'] = dwm['species']+' '+dwm['infraspecificEpithet']
#dwm.loc[dwm.taxonRank == 'Variedad', 'scientificName'] = dwm['species']+' var. '+dwm['infraspecificEpithet']
#dwm.loc[dwm.taxonRank == 'Forma', 'scientificName'] = dwm['species']+' f. '+dwm['infraspecificEpithet']
# Verify there is no nan or null in scienficName.
#dwm.groupby(['scientificName']).gbifID.nunique().reset_index()


#%%
#Reorganize de columns of the final dataset
dwc_co=dwc_co[[
 'gbifID',
 'doi',
 'organization',
 'datasetTitle',
 'license',
 'occurrenceID',
 'basisOfRecord',
 'type',
 #'institutionID',
 #'institutionCode',
 'collectionCode',
 'catalogNumber',
 'datasetID',
 'recordedBy',
 'individualCount',
 'organismQuantity',
 'organismQuantityType',
 #'sex',
 #'lifeStage',
 #'preparations',
 #'disposition',
 'eventID',
 'samplingProtocol',
 #'samplingEffort',
 'eventDate',
 'year',
 'month',
 'day',
 'countryCode',
 'country',
 'stateProvince',
 'county',
 'habitat',
#'waterBody',
 'municipality',
 'locality',
 'elevation',
 'depth',
 'coordinateUncertaintyInMeters',
 'decimalLatitude',
 'decimalLongitude',
 'Departamento-ubicacionCoordenada',
 'Municipio-ubicacionCoordenada',
 'ZonaMaritima',
 'stateProvinceValidation',
 'countyValidation',
 'flagGEO',
 'scientificName',
 'kingdom',
 'phylum',
 'class',
 'order',
 'family',
 'genus',
 'species',
 'taxonRank',
 'identifiedBy',
 'dateIdentified',
 'identificationQualifier',
 'vernacularName',
 'appendixCITES',
 'threatStatus_UICN',
 'threatStatus_MADS',
 'isInvasive_mads',
 'isInvasive_griis',
 'exotic',
 'listaReferenciaCO',
 'endemic',
 'migratory',
 'isTerrestrial',
 'isFreshwater',
 'isMarine',
 'isBrackish',
 'isHybrid',
 'flagTAXO',
 'datasetKey',
 'publishingOrgKey',
 'created',
 'logoUrl',
 'dataType',
 'publishingCountry',
 'repatriated',
 'issue'
]]



#%%

#Export the final dataset
dwc_co.to_csv('C:\\Cifras_Colombia\\2021\\Datos_Trimetrales\\TrimestreIV\\dwc_co_2021T4.txt', sep="\t", encoding = "utf8")

#%%

#IT'S DONE!!!!


#%%
