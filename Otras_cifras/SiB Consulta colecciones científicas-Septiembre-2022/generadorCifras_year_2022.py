'''
Created: 2022-04-20
Last update: 2022-04-20

@author: Nerieth Leuro

Este script fue creado por el SiB Colombia para la obtención de cifras de especies a nivel municipal o departamental.
El archivo que se utilice para la ejecución de este script debe haber pasado por un proceso de validación, limpieza 
y una revisión del equipo de trabajo para asegurar la presencia de los campos necesarios para la obtención de cada una 
de las cifras. Adicionalmente, se debe contar con los archivos complementarios, correspondientes a DIVIPOLA y listas de referencia:
CITES, IUCN, especies amenazadas(resolución 1912 de 2017), GRIIS, MADS y listas de referencia grupos biologicos(Aves,
peces, mamíferos y plantas).

Al terminar la ejecución del script se obtendran diferentes archivos que contendran la información agrupada por tematicas, grupos
biológicos, Geografía (Municipios o Departamentos) y entidades.

Tenga en cuenta que dentro del script podra encontrar algunas secciones identificadas con ##, estas corresponden a líneas explicativas. 
Y partes del código que se encuentran comentariadas con # que corresponden a secciones de código que se pueden habilitar o deshabilitar
según los datos que se deseen obtener.

'''

##-----------------------------------------------------Preparar el entorno---------------------------------------------##
##Importar las librerias necesarias para el uso del script
import pandas as pd 
import os 
import time
import numpy as np


##Iniciar el conteo de tiempo de ejecución del script
inicio=time.time()

##Mostrar la carpeta principal donde se encuentra alojado el script
print(os.getcwd())

##Establecer la carpeta de trabajo
##Tenga en cuenta que en el momento de diligenciar la ubicación de la carpeta de trabajo se deben documentar doble barra invertida "\\" para evitar errores de sintaxis
os.chdir("D:\\Consulta colecciones")

##Crear una variable string con el nombre del conjunto de datos. Este nombre debe incluir: la geografía, el año y el corte trimestral utilizado 
nombre='Colecciones2022T2_temporalidad_'

##----------------------------------------------------Cargar archivos--------------------------------------------------##
'''
Para conjuntos de datos para obtener cifras municipales y equipos con poca memoria RAM (8 gb o menos)
Se agrega dtype (asignar el tipo de dato a cada columna)
Con el fin de evitar el error: pandas.errors.ParserError: Error tokenizing data. C error: out of memory
En el caso de conjuntos de datos para cifras departamentales realice la ejecución en un equipo con memoria RAM de 16 gb

dtype: se especifica cada columna y el tipo de dato que corresponde, el orden de los campos no afecta la ejecución, pero debe actualizarse
cada que se agreguen y quiten campos para hacer más eficiente el proceso.
    str: Se utiliza para campos que contengan texto y símbolos
    float: Aplica en campos que contengan números decimales
Adicionalmente, se especifican las columnas a cargar con el fin de dejar solo las columnas necesarias
para la obtención de cifras y no sobrecargar la memoria RAM y hacer el proceso más eficiente.
'''

##Crear un objeto cargando la tabla de datos(formato tsv o txt)
registros = pd.read_table('registros_Colecciones2022T2.tsv', sep='\t', encoding = "utf8",dtype=str)    

##Reemplaza los campos vacios por NaN, esto para evitar errores de conversión de datos al momento de
## hacer los calculos
registros = registros.replace(r'^\s*$', np.nan, regex=True)

## Cargar el archivo de grupos biologicos que relacionan la taxonomía con los grupos biológicos
grupos_biologicos = pd.read_table('grupos_biologicos\\gruposBiologicosCifrasSiB-v20220705.tsv', sep='\t', encoding = "utf8")
grupos_biologicos=grupos_biologicos.loc[grupos_biologicos.loc[:, 'tipo_grupo'] != '-']
##Cargar archivo de referencia geográfica 
staProv_divipola = pd.read_table('DIVIPOLA_20200311_unique.txt', encoding = "utf8")

##Archivo del último reporte mensual
entidades_reporte= pd.read_table('reporteMensual\\datasetCO-2022-09-01.tsv', sep='\t', encoding = "utf8",usecols=['organization','Logo','typeOrg','URLSocio','NombreCorto_Org'])

##----------------------------------------------------Tipo de ejecución--------------------------------------------------##
'''
La variable 'tipo' permite condicionar los procesos teniendo en cuenta si se van a sacar cifras departamentales o municipales y si 
el conjunto de datos contiene información de registros marítimos
Departamental con datos marinos='DCDM'
Departamental sin datos marinos='DSDM'
Municipal con datos marinos='MCDM'
Municipal sin datos marinos='MSDM'
Otros='OT'
Al seleccionar alguna de las opciones sin datos marinos, las cifras se calculan en forma general, sin discriminar por los hábitat marino, 
terrestre y salobre

Para las opciones con datos marinos, se realiza el cálculo de cifras general y adicionalmente el cálculo para los hábitat marino, terrestre 
y salobre. Es importante aclarar que debido a los hábitos y distribución de las especies se pueden encontrar especies presentes en más de 
un hábitat por lo tanto, el valor general no corresponde a la suma de los valores para cada hábitat.

'''
#tipo='DCDM'
tipo='DSDM'
#tipo='MCDM'
#tipo='MSDM'
#tipo='OT'

'''
En el caso de ejecutar Otros, debe asignar el nombre de la variable que contiene la unidad geografica a trabajar. Ej: 'cmpljnom' para complejos de
páramos
'''
#geografia='cmpljnom'


##--------------------------------------------Ajustes preliminares de los datos ---------------------------------------------##
'''
Cuando se realiza la ejecución de este script sin realizar un proceso de limpieza en el campo de stateProvince se debe realizar el cruce con el
archivo de referencia geográfica DIVIPOLA. Lo cual permitirá asegurar que en este campo solo se encuentren los departamentos válidos para el
país.
'''
registros['verbatimStateProvince']=registros['stateProvince']


##Quitar comillas
registros['stateProvince']=registros.stateProvince.str.replace('"','')

## Unir las tablas por medio del campo stateProvince
registros=pd.merge(registros,staProv_divipola, on='stateProvince',how='left') 

#Reemplazar los valores null de stateProvince_divipola por los valores de Departamento-ubicacionCoordenada
registros['stateProvince']=registros['stateProvince_divipola'].mask(pd.isnull, registros['Departamento-ubicacionCoordenada'])

#print(list(registros['stateProvince'].unique()))

##Ajuste de las categorías threathStatus para asegurar integridad de las cifras MADS e IUCN en los conteos
registros.threatStatus_UICN=registros.threatStatus_UICN + '_IUCN'
registros.threatStatus_MADS=registros.threatStatus_MADS + '_MADS'
##SubsetTaxonómico si se necesitan cifras totales por reinos (obtener solo los datos que pertenezcan a ese reino)
#registros= registros[(registros['kingdom'] =='Animalia')]

##Obtener el número de registros para cada reino
#numSpEndemicas=registros.groupby(["kingdom"])["kingdom"].count()


##--------------------------------------------RRBB y SPP X Año evento ---------------------------------------------##
##Crear un dataframe vacío
total_cifras=pd.DataFrame(index=[0])

##Crear variable para los conteos por especie general
variable_conteos = registros[registros['species'].notna()].drop_duplicates('species').sort_values(by=['species']) 
#registros.groupby(i)['gbifID'].count()

##Crear la columna registros y especies general contando el campo gbifID
total_registros=registros.groupby('year')['gbifID'].count().to_frame(name = 'registros').reset_index()
total_especies=variable_conteos.groupby('year')['gbifID'].count().to_frame(name = 'especies').reset_index()
total_cifras=pd.merge(total_registros,total_especies, on=['year'],how='left')


##Condicional para registros marinos, continentales y salobres
if tipo =='MCDM' or tipo =='DCDM':
    ##Crear un subconjunto de datos para los registros marinos, continentales y salobres
    registros_marinos=registros[registros['isMarine']=='Marine']
    registros_continentales=registros[registros['isTerrestrial']=='Terrestrial']
    registros_salobres=registros[registros['isBrackish']=='Brackish']

    ##Crear variables para los conteos por especie de las categorias marinos, continentales y salobres
    variable_conteos_marinos = registros_marinos[registros_marinos['species'].notna()].drop_duplicates('species').sort_values(by=['species']) #se puede dejar en una sola variable
    variable_conteos_continentales = registros_continentales[registros_continentales['species'].notna()].drop_duplicates('species').sort_values(by=['species']) #se puede dejar en una sola variable
    variable_conteos_salobres = registros_salobres[registros_salobres['species'].notna()].drop_duplicates('species').sort_values(by=['species']) #se puede dejar en una sola variable

    ##Crear las columnas registros marinos y especies marinas
    registrosMarinos=registros.groupby('year')['isMarine'].count().to_frame(name = 'registrosMarinos').reset_index()
    especiesMarinas=variable_conteos.groupby('year')['isMarine'].count().to_frame(name = 'especiesMarinas').reset_index()

    ##Crear la columna registros salobres y especies salobres
    registrosSalobres=registros.groupby('year')['isBrackish'].count().to_frame(name = 'registrosSalobres').reset_index()
    especiesSalobres=variable_conteos.groupby('year')['isBrackish'].count().to_frame(name = 'especiesSalobres').reset_index()

    ##Crear las columnas registros continentales y especies continentales
    registrosContinentales=registros.groupby('year')['isTerrestrial'].count().to_frame(name = 'registrosContinentales').reset_index()
    especiesContinentales=variable_conteos.groupby('year')['isTerrestrial'].count().to_frame(name = 'especiesContinentales').reset_index()

    total_cifras=pd.merge(total_cifras,registrosMarinos, on=['year'],how='left').merge(especiesMarinas, on=['year'],how='left')
    total_cifras=pd.merge(total_cifras,registrosSalobres, on=['year'],how='left').merge(especiesSalobres, on=['year'],how='left')
    total_cifras=pd.merge(total_cifras,registrosContinentales, on=['year'],how='left').merge(especiesContinentales, on=['year'],how='left')
    
    total_cifras.columns=['anio','registros','especies','registros_marinos','especies_marinas','registros_salobres', 'especies_salobres', 'registros_continentales', 'especies_continentales']
    total_cifras=total_cifras[['anio','registros','registros_continentales','registros_marinos','registros_salobres','especies','especies_continentales', 'especies_marinas', 'especies_salobres']]

##Quitar los .0
total_cifras=total_cifras.replace(np.nan,'',regex=True)
total_cifras=total_cifras.astype(str)
total_cifras=total_cifras.replace(to_replace='\.0+$',value="",regex=True)
total_cifras=total_cifras.astype(str)
total_cifras=total_cifras.replace(to_replace='\.0+$',value="",regex=True)

##crear archivo tsv 
total_cifras.to_csv(nombre+'cifrasTotales.tsv',sep='\t', index=False ) 

##crear archivo excel y la hoja Cifras totales
total_cifras.to_excel(nombre +'cifrasTotales.xlsx', sheet_name='Cifras totales', index=False)


##---------------------------------------------Cifras X año geográficas --------------------------------------------------##
'''
Dentro de esta sección se define el alcance geográfico del conjunto de datos. Se selecciona si se va a trabajar a nivel nacional o
departamental. En caso de que se trabaje a nivel nacional, las cifras geográficas harán referencia a los departamentos. Para el nivel 
departamental las cifras se obtendrán para cada uno de los municipios encontrados dentro del departamento de interés
Dentro de los nombres se encuentran las siguientes convenciones:
rb= Registros biologicos
sp= especies
geo= geografía
'''

## Definir el alcance de las cifras geográficas
## Seleccionar 'stateProvince' para cifras Nacionales, 'county' para cifras departamentales
if tipo =='MCDM' or tipo =='MSDM':
    geografia='county' 
if tipo =='DCDM' or tipo =='DSDM':
    geografia='stateProvince' 

##Crear dataframes vacíos para almacenar las cifras geográficas de especies y registros
geo_rb_total=pd.DataFrame()
geo_sp_total=pd.DataFrame()
geo_anio_total=pd.DataFrame()

##Cifras registros por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geográficas y contar los registros generales
##Crear columna registros
geo_rb_total= registros.groupby([geografia, 'year'])['gbifID'].count().to_frame(name = 'registros').reset_index()

##Cifras especies por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geografica y contar los registros por species
##Crear un dataframe con los datos. Agrupar por entidad geografica y contar los registros por species
geo_sp_total= registros.groupby([geografia,'year','species'])['species'].count().to_frame(name = 'registros').reset_index()
geo_sp_total= geo_sp_total.groupby([geografia,'year'])['species'].count().to_frame(name = 'especies').reset_index().fillna('').sort_values(geografia)


if tipo =='MCDM' or tipo =='DCDM':
    
    ##Cifras registros por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geográfica y contar los registros para cada una de las categorías (marinos, salobres y continentales)
    ##Crear columna registros para datos marinos
    geo_rb_marinos= registros.groupby([geografia,'year'])['isMarine'].count().to_frame(name = 'registrosMarinos').reset_index()
    ##Crear columna registros para datos salobres
    geo_rb_salobres= registros.groupby([geografia,'year'])['isBrackish'].count().to_frame(name = 'registrosSalobres').reset_index()
    ##Crear columna registros para datos continentales
    geo_rb_continentales= registros.groupby([geografia,'year'])['isTerrestrial'].count().to_frame(name = 'registrosContinentales').reset_index()
    
    geo_rb_total=pd.merge(geo_rb_total,geo_rb_marinos, on=['year',geografia],how='left').merge(geo_rb_salobres, on=['year',geografia],how='left').merge(geo_rb_continentales, on=['year',geografia],how='left')
    
    ##Cifras especies por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geográfica y contar los registros por species para cada una de las categorías (marinos, salobres y continentales)
    ##Crear conjunto de datos con especies y geografía para datos marinos y Contar número de especies marinos para entidad geográfica 
    geo_sp_marino=registros.groupby([geografia,'year','species','isMarine'])['isMarine'].count().to_frame(name = 'registros').reset_index()
    #print(geo_sp_marino.groupby(geografia).count())
    geo_sp_marino=geo_sp_marino.groupby([geografia,'year'])['registros'].count().to_frame(name = 'especiesMarinas').reset_index()
    
    ##Crear conjunto de datos con especies y geografía para datos salobres y Contar número de especies salobres para entidad geográfica 
    geo_sp_salobre=registros.groupby([geografia,'year','species','isBrackish'])['isBrackish'].count().to_frame(name = 'registros').reset_index()
    geo_sp_salobre=geo_sp_salobre.groupby([geografia,'year'])['registros'].count().to_frame(name = 'especiesSalobres').reset_index()

    ##Crear conjunto de datos con especies y geografía para datos continentales y Contar número de especies continentales para entidad geográfica 
    ##Contar número de especies salobres para entidad geográfica 
    geo_sp_continental=registros.groupby([geografia,'year','species','isTerrestrial'])['isTerrestrial'].count().to_frame(name = 'registros').reset_index()
    geo_sp_continental=geo_sp_continental.groupby([geografia,'year'])['registros'].count().to_frame(name = 'especiesContinentales').reset_index()

    ##Unir todos los conjuntos de datos, Reemplazar los valores NaN y ordenar las entidades geográficas
    geo_sp_total=pd.merge(geo_sp_total,geo_sp_salobre, on=[geografia,'year'],how='left').merge(geo_sp_marino, on=[geografia,'year'],how='left').merge(geo_sp_continental, on=[geografia,'year'],how='left').fillna('').sort_values(geografia)

geo_anio_total=pd.merge(geo_sp_total,geo_rb_total, on=[geografia,'year'],how='left')
##Quitar los .0
geo_anio_total=geo_anio_total.astype(str)
geo_anio_total=geo_anio_total.replace(to_replace='\.0+$',value="",regex=True)
geo_anio_total=geo_anio_total.astype(str)
geo_anio_total=geo_anio_total.replace(to_replace='\.0+$',value="",regex=True)

##crear archivo tsv 
geo_anio_total.to_csv(nombre+'geografia.tsv',sep='\t', index=False ) 

##crear archivo excel y la hoja Cifras totales
geo_anio_total.to_excel(nombre +'geografia.xlsx', sheet_name='temporalidad_geografia', index=False)

##---------------------------------------------Cifras X organizaciones publicadoras y país publicación---------------------------------##
'''
Se obtienen las cifras de especies y registros para todas las organizaciones publicadoras que se encuentran representadas en 
el conjunto de datos
'''

##Quitar el texto: "País publicador:" del campo 'publishingCountry'
registros=registros.replace('País publicador: ','',regex=True)

##Reemplazar los nombres y la url del logo para las entidades que lo requieran
registros.loc[registros.organization=='iNaturalist.org','organization']='Naturalista Colombia'
registros.loc[registros.organization=='Cornell Lab of Ornithology','organization']='eBird Colombia'
registros.loc[registros.organization=='Naturalista Colombia','publishingCountry']='CO'

##Crear dataframes vacíos para almacenar las cifras de entidades publicadoras
entidades_registros=pd.DataFrame()
entidades_total=pd.DataFrame()
entidades_total_pc=pd.DataFrame()

##Conteo de registros y especies por país publicación
entidades_rb_pc= registros.groupby(['publishingCountry','year'])['gbifID'].count().to_frame(name = 'registros').reset_index()

entidades_sp_pc= registros.groupby(['publishingCountry','year','species'])['species'].count().to_frame(name = 'especies').reset_index()
entidades_sp_pc= entidades_sp_pc.groupby(['publishingCountry','year'])['especies'].count().to_frame(name = 'especies').reset_index() 

entidades_total_pc=pd.merge(entidades_rb_pc,entidades_sp_pc, on=['publishingCountry','year'],how='left').sort_values('publishingCountry').fillna('')
    
##Condicional para registros marinos, continentales y salobres
if tipo =='MCDM' or tipo =='DCDM':
    ## Cifras registros por organización publicadora: Crear un dataframe con los datos, agrupar por organización y contar los registros para cada una de las categorías (marinos, salobres y continentales)
    ##Crear conjunto de datos con registros y organización para datos marinos, salobres y continentales
    entidades_rb= registros.groupby(['organization','year'])['gbifID'].count().to_frame(name = 'registros').reset_index()
    entidades_rb_marino= registros.groupby(['organization','year'])['isMarine'].count().to_frame(name = 'registrosMarinos').reset_index()
    entidades_rb_salobre= registros.groupby(['organization','year'])['isBrackish'].count().to_frame(name = 'registrosSalobres').reset_index()
    entidades_rb_continental= registros.groupby(['organization','year'])['isTerrestrial'].count().to_frame(name = 'registrosContinentales').reset_index()

    #Conteo de registros por país publicador    
    ## Cifras especies por organización publicadora: Crear un dataframe con los datos, agrupar por organización y especie, contar los registros por species para cada una de las categorías (marinos, salobres y continentales)
    entidades_sp= registros.groupby(['organization','year','species'])['species'].count().to_frame(name = 'especies').reset_index()
    entidades_sp= entidades_sp.groupby(['organization','year'])['especies'].count().to_frame(name = 'especies').reset_index() 
    
    ##Crear conjunto de datos con especies y entidad publicadora para datos marinos
    ##Contar número de especies marinos para entidad publicadora 
    ent_sp_marino= registros.groupby(['organization','year','species','isMarine'])['species'].count().to_frame(name = 'especies').reset_index()
    ent_sp_marino= ent_sp_marino.groupby(['organization','year'])['especies'].count().to_frame(name = 'especiesMarinas').reset_index() 

    ##Crear conjunto de datos con especies y entidad publicadora para datos salobres
    ##Contar número de especies salobres para entidad publicadora 
    ent_sp_salobre= registros.groupby(['organization','year','species','isBrackish'])['species'].count().to_frame(name = 'especies').reset_index()
    ent_sp_salobre= ent_sp_salobre.groupby(['organization','year'])['especies'].count().to_frame(name = 'especiesSalobres').reset_index() 

    ##Crear conjunto de datos con especies y entidad publicadora para datos continentales
    ##Contar número de especies continentales para entidad publicadora
    ent_sp_continental= registros.groupby(['organization','year','species','isTerrestrial'])['species'].count().to_frame(name = 'especies').reset_index() 
    ent_sp_continental= ent_sp_continental.groupby(['organization','year'])['especies'].count().to_frame(name = 'especiesContinentales').reset_index() 

    ##Unir todos los conjuntos de datos, Reemplazar los valores NaN y ordenar las entidades geográficas
    entidades_total=registros.groupby(['organization','year'])['organization'].count().to_frame(name = 'especies').reset_index().drop('especies', axis=1)  
    entidades_total=pd.merge(entidades_total,entidades_sp, on=['organization','year'],how='left').merge(ent_sp_salobre, on=['organization','year'],how='left').merge(ent_sp_marino, on=['organization','year'],how='left').merge(ent_sp_continental, on=['organization','year'],how='left')
    entidades_total=pd.merge(entidades_total,entidades_rb, on=['organization','year'],how='left').merge(entidades_rb_marino, on=['organization','year'],how='left').merge(entidades_rb_salobre, on=['organization','year'],how='left').merge(entidades_rb_continental, on=['organization','year'],how='left').sort_values('organization').fillna('')
    
else:
    ## Cifras registros por organización publicadora: Crear un dataframe con los datos agrupar por organización y contar los registros generales
    entidades_registros['registros']= registros.groupby(['organization','year'])['gbifID'].count()
    
    ##Crear conjunto de datos con especies y entidad publicadora general
    ##Contar número de especies para entidad publicadora general
    entidades_sp= registros.groupby(['organization','year','species'])['species'].count().to_frame(name = 'especies').reset_index()
    entidades_sp= entidades_sp.groupby(['organization','year'])['especies'].count().to_frame(name = 'especies').reset_index() 

    ##Unir los conteos de registros y especies por entidad general
    entidades_total=pd.merge(entidades_registros,entidades_sp, on=['organization','year'],how='left').sort_values('organization').fillna('')
    

##Quitar los .0
entidades_total=entidades_total.astype(str)
entidades_total=entidades_total.replace(to_replace='\.0+$',value="",regex=True)
print(entidades_total.columns)

##Exportar los datos a archivos tsv y xlsx
if tipo =='MCDM' or tipo =='DCDM':
    entidades_total.columns=['oganizacion','anio','especies','especies_salobres','especies_marinas','especies_continentales','registros','registros_marinos','registros_salobres','registros_continentales']
    entidades_total=entidades_total[['oganizacion','anio','registros','registros_continentales','registros_marinos','registros_salobres','especies','especies_continentales','especies_marinas','especies_salobres']]
else:
    entidades_total.columns=['oganizacion','anio','especies','registros']
    entidades_total=entidades_total[['oganizacion','anio','registros','especies']]
    
entidades_total.to_csv(nombre+'cifrasOrganizaciones.tsv',sep='\t', index=False )
entidades_total.to_excel(nombre+'cifrasOrganizaciones.xlsx', sheet_name='cifrasEntidades', index=False )

entidades_total_pc.to_csv(nombre+'cifraspaisPublicacion.tsv',sep='\t', index=False )
entidades_total_pc.to_excel(nombre+'cifraspaisPublicacion.xlsx', sheet_name='cifraspaispublicador', index=False )


##-------------------------------------------------Loop calculo cifras ---------------------------------------------------##

## Define una lista con todos las categorias taxonomicas
## Asegurese que las columnas en el archivo tengan los mismos nombres encontrados en la lista
taxon = ['kingdom','phylum','class','order','family','genus'] 

## Dataframe para especies
rb_taxon_categoria_total=pd.DataFrame()
sp_taxon_categoria_total=pd.DataFrame()

for j in taxon:    
    
    ## Número de taxones por grupo taxonómico en la categoría general
    rb_numero = registros.groupby([j,'year'])['year'].count().to_frame(name = 'taxones' ).reset_index()
    #rb_numero['thematic']=referencia_tematicas[categoria]
    rb_numero=rb_numero.rename(columns={j:'grupoTax'})
    rb_numero['taxonRank']=j
    
    ## Número de taxones por grupo taxonómico en la categoría general
    sp_numero = registros.groupby(['species',j,'year'])['year'].count().to_frame(name = 'taxones' ).reset_index()
    #rb_numero['thematic']=referencia_tematicas[categoria]
    sp_numero=sp_numero.rename(columns={j:'grupoTax'})
    sp_numero['taxonRank']=j
    sp_numero = sp_numero.groupby(['taxonRank','year'])['species'].count().to_frame(name = 'taxones' ).reset_index()

    
  
    rb_taxon_categoria_total=pd.concat([rb_taxon_categoria_total,rb_numero])
    sp_taxon_categoria_total=pd.concat([sp_taxon_categoria_total,rb_numero])


#print(sp_taxon_categoria_total)    
##---------------------Transformación número de especies por categoría taxonómica a número de especies por grupos biológicos----------------------------##
grupos_biologicost=grupos_biologicos[['grupo_label','grupoTax','taxonRank']]

## Resumen cifras taxones por grupo biológico general
rb_grupos_biologicos=pd.merge(rb_taxon_categoria_total,grupos_biologicost, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 
rb_grupos_biologicos=rb_grupos_biologicos[['grupoTax','year','taxones','grupo_label']]


sb_grupos_biologicos=pd.merge(sp_taxon_categoria_total,grupos_biologicost, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 
sb_grupos_biologicos=sb_grupos_biologicos[['grupoTax','year','taxones','grupo_label']]


#Especies total para la tematica por grupo biologico

rb_tem_gp=rb_grupos_biologicos.groupby(['grupo_label','year']).sum().reset_index()
sb_tem_gp=sb_grupos_biologicos.groupby(['grupo_label','year']).sum().reset_index()

print(sb_tem_gp)

##Quitar los .0
rb_tem_gp=rb_tem_gp.astype(str)
rb_tem_gp=rb_tem_gp.replace(to_replace='\.0+$',value="",regex=True)
rb_tem_gp=rb_tem_gp.astype(str)
rb_tem_gp=rb_tem_gp.replace(to_replace='\.0+$',value="",regex=True)

sb_tem_gp=sb_tem_gp.astype(str)
sb_tem_gp=sb_tem_gp.replace(to_replace='\.0+$',value="",regex=True)
sb_tem_gp=sb_tem_gp.astype(str)
sb_tem_gp=sb_tem_gp.replace(to_replace='\.0+$',value="",regex=True)

##Exportar los archivo resultantes
rb_tem_gp.to_csv(nombre+'PorGrupoBiologico.tsv',sep='\t', index=False )
rb_tem_gp.to_excel(nombre+'PorGrupoBiologico.xlsx', sheet_name='PorGrupoBiologico', index=False )


sb_tem_gp.to_csv(nombre+'PorGrupoBiologicoSp.tsv',sep='\t', index=False )
sb_tem_gp.to_excel(nombre+'PorGrupoBiologicoSp.xlsx', sheet_name='PorGrupoBiologico', index=False )



##---------------------------------------------Cifras conjunto de datos---------------------------------##
'''
Se obtienen las cifras de especies y registros para todas las organizaciones publicadoras que se encuentran representadas en 
el conjunto de datos
'''

##Quitar el texto: "País publicador:" del campo 'publishingCountry'
registros=registros.replace('País publicador: ','',regex=True)

##Reemplazar los nombres y la url del logo para las entidades que lo requieran
registros.loc[registros.organization=='iNaturalist.org','organization']='Naturalista Colombia'
registros.loc[registros.organization=='Cornell Lab of Ornithology','organization']='eBird Colombia'


##Crear dataframes vacíos para almacenar las cifras de entidades publicadoras
entidades_registros=pd.DataFrame()
entidades_total=pd.DataFrame()


 ## Cifras registros por organización publicadora: Crear un dataframe con los datos agrupar por organización y contar los registros generales
entidades_registros['registros']= registros.groupby(['datasetTitle','year'])['gbifID'].count()

##Crear conjunto de datos con especies y entidad publicadora general
##Contar número de especies para entidad publicadora general
entidades_sp= registros.groupby(['datasetTitle','year','species'])['species'].count().to_frame(name = 'especies').reset_index()
entidades_sp= entidades_sp.groupby(['datasetTitle','year'])['especies'].count().to_frame(name = 'especies').reset_index() 

##Unir los conteos de registros y especies por entidad general
entidades_total=pd.merge(entidades_registros,entidades_sp, on=['datasetTitle','year'],how='left').sort_values('datasetTitle').fillna('')

##Quitar los .0
entidades_total=entidades_total.astype(str)
entidades_total=entidades_total.replace(to_replace='\.0+$',value="",regex=True)
print(entidades_total.columns)

entidades_total.to_excel(nombre+'cifrasConjuntoDatos.xlsx', sheet_name='cifrasEntidades', index=False )
