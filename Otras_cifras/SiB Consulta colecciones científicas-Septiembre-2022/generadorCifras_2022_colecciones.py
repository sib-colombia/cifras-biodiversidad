'''
Created: 2018-10-31
Last update: 2022-02-02

@author: camila.plata
@editor: Nerieth Leuro

Este script fue creado por el SiB Colombia para la obtención de cifras de especies a nivel municipal o departamental.
El archivo que se utilice para la ejecución de este script debe haber pasado por un proceso de validación, limpieza 
y una revisión del equipo de trabajo para asegurar la presencia de los campos necesarios para la obtención de cada una 
de las cifras. Adicionalmente, se debe contar con los archivos complementarios, correspondientes a DIVIPOLA y listas de referencia:
CITES, IUCN, especies amenazadas(resolución 1912 de 2017), GRIIS, MADS y listas de referencia grupos biologicos(Aves,
peces, mamíferos y plantas).

Al terminar la ejecución del script se obtendran diferentes archivos que contendran la información agrupada por tematicas, grupos
biológicos, Geografía (Municipios o Departamentos) y entidades.

Tenga en cuenta que dentro del script podra encontrar algunas secciones identificadas con ##, estas corresponden a líneas explicativas. 
Y partes del código que se encuentran comentariadas con # que corresponden a secciones de código que se pueden habilitar o deshabilitar
según los datos que se deseen obtener.

'''

##-----------------------------------------------------Preparar el entorno---------------------------------------------##
##Importar las librerias necesarias para el uso del script
import pandas as pd 
import os 
import time
import numpy as np


##Iniciar el conteo de tiempo de ejecución del script
inicio=time.time()

##Mostrar la carpeta principal donde se encuentra alojado el script
print(os.getcwd())
##Establecer la carpeta de trabajo
##Tenga en cuenta que en el momento de diligenciar la ubicación de la carpeta de trabajo se deben documentar doble barra invertida "\\" para evitar errores de sintaxis
os.chdir("D:\\Consulta colecciones")


##Crear una variable string con el nombre del conjunto de datos. Este nombre debe incluir: la geografía, el año y el corte trimestral utilizado 
nombre='2_ColeccionesDept2022T2_'

##----------------------------------------------------Cargar archivos--------------------------------------------------##
'''
Para conjuntos de datos para obtener cifras municipales y equipos con poca memoria RAM (8 gb o menos)
Se agrega dtype (asignar el tipo de dato a cada columna)
Con el fin de evitar el error: pandas.errors.ParserError: Error tokenizing data. C error: out of memory
En el caso de conjuntos de datos para cifras departamentales realice la ejecución en un equipo con memoria RAM de 16 gb

dtype: se especifica cada columna y el tipo de dato que corresponde, el orden de los campos no afecta la ejecución, pero debe actualizarse
cada que se agreguen y quiten campos para hacer más eficiente el proceso.
    str: Se utiliza para campos que contengan texto y símbolos
    float: Aplica en campos que contengan números decimales
Adicionalmente, se especifican las columnas a cargar con el fin de dejar solo las columnas necesarias
para la obtención de cifras y no sobrecargar la memoria RAM y hacer el proceso más eficiente.
'''

##Crear un objeto cargando la tabla de datos(formato tsv o txt)

registros = pd.read_table('registros_Colecciones2022T2.tsv', sep='\t', encoding = "utf8",dtype=str)    

#print(registros.loc[registros.loc[:, 'datasetKey'] == '95ef142c-05f0-4927-943c-239cfe07b1e7'])

#registros = pd.read_table('dwc_co_20220330.txt', sep='\t', encoding = "utf8",usecols=['gbifID', 'organization','publishingOrgKey','publishingCountry','logoUrl','county', 'stateProvince','municipality',
#'locality','scientificName','species', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'taxonRank','threatStatus_UICN', 'especies_invasoras', 'appendixCITES', 
# 'threatStatus_MADS', 'especies_exoticas', 'especies_exotica_riesgo_invasion', 'endemic', 'migratory', 'Municipio-ubicacionCoordenada','Departamento-ubicacionCoordenada',
# 'ZonaMaritima'],dtype=str)     

##Reemplaza los campos vacios por NaN, esto para evitar errores de conversión de datos al momento de
## hacer los calculos
registros = registros.replace(r'^\s*$', np.nan, regex=True)

## Cargar el archivo de grupos biologicos que relacionan la taxonomía con los grupos biológicos
grupos_biologicos = pd.read_table('grupos_biologicos\\gruposBiologicos_202109_actual.csv', sep=';', encoding = "utf8")

##Cargar archivo de referencia geográfica 
staProv_divipola = pd.read_table('DIVIPOLA_20200311_unique.txt', encoding = "utf8")

##Archivo del último reporte mensual
entidades_reporte= pd.read_table('reporteMensual\\datasetCO-2022-09-01.tsv', sep='\t', encoding = "utf8",usecols=['organization','Logo','typeOrg','URLSocio','NombreCorto_Org'])

##----------------------------------------------------Tipo de ejecución--------------------------------------------------##
'''
La variable 'tipo' permite condicionar los procesos teniendo en cuenta si se van a sacar cifras departamentales o municipales y si 
el conjunto de datos contiene información de registros marítimos
Departamental con datos marinos='DCDM'
Departamental sin datos marinos='DSDM'
Municipal con datos marinos='MCDM'
Municipal sin datos marinos='MSDM'
Otros='OT'
Al seleccionar alguna de las opciones sin datos marinos, las cifras se calculan en forma general, sin discriminar por los hábitat marino, 
terrestre y salobre

Para las opciones con datos marinos, se realiza el cálculo de cifras general y adicionalmente el cálculo para los hábitat marino, terrestre 
y salobre. Es importante aclarar que debido a los hábitos y distribución de las especies se pueden encontrar especies presentes en más de 
un hábitat por lo tanto, el valor general no corresponde a la suma de los valores para cada hábitat.

'''
#tipo='DCDM'
tipo='DSDM'
#tipo='MCDM'
#tipo='MSDM'
#tipo='OT'

'''
En el caso de ejecutar Otros, debe asignar el nombre de la variable que contiene la unidad geografica a trabajar. Ej: 'cmpljnom' para complejos de
páramos
'''
#geografia='cmpljnom'


##Este sección solo se utiliza si el corte trimestral tiene las columnas de isTerrestrial
##isMarine isBrackish y no se procesa primero con el script crecue_WoRMS 
'''
if tipo =='MCDM' or tipo =='DCDM':
    ## Aplica para datos departamentales
    ##Cuando no se cruza con WoRMS
    registros['tipoRegistro']='Continental'
    

    registros.loc[(registros['isTerrestrial'] != '0.0'), 'isTerrestrial'] = 'Terrestrial'
    registros.loc[(registros['isMarine'] == '1.0'), 'isMarine'] = 'Marine'
    registros.loc[(registros['isBrackish'] == '1.0'), 'isBrackish'] = 'Brackish'   
    

    
    ##Eliminar los valores de 0.0 para que no se incluyan dentro de los conteos
    registros.loc[(registros['isMarine'] == '0.0'), 'isMarine'] = np.nan
    registros.loc[(registros['isBrackish'] == '0.0'), 'isBrackish'] = np.nan
    registros.loc[(registros['isTerrestrial'] == '0.0'), 'isTerrestrial'] = np.nan
'''
##--------------------------------------------Ajustes preliminares de los datos ---------------------------------------------##
'''
Cuando se realiza la ejecución de este script sin realizar un proceso de limpieza en el campo de stateProvince se debe realizar el cruce con el
archivo de referencia geográfica DIVIPOLA. Lo cual permitirá asegurar que en este campo solo se encuentren los departamentos válidos para el
país.
'''
##Reajustar campo stateProvince (asignar al campo verbatimStateProvince el valor de stateProvince)
registros['verbatimStateProvince']=registros['stateProvince']


##Quitar comillas
registros['stateProvince']=registros.stateProvince.str.replace('"','')

## Unir las tablas por medio del campo stateProvince
registros=pd.merge(registros,staProv_divipola, on='stateProvince',how='left') 

#Reemplazar los valores null de stateProvince_divipola por los valores de Departamento-ubicacionCoordenada
registros['stateProvince']=registros['stateProvince_divipola'].mask(pd.isnull, registros['Departamento-ubicacionCoordenada'])

#print(list(registros['stateProvince'].unique()))

##Ajuste de las categorías threathStatus para asegurar integridad de las cifras MADS e IUCN en los conteos
registros.threatStatus_UICN=registros.threatStatus_UICN + '_IUCN'
registros.threatStatus_MADS=registros.threatStatus_MADS + '_MADS'

##SubsetTaxonómico si se necesitan cifras totales por reinos (obtener solo los datos que pertenezcan a ese reino)
#registros= registros[(registros['kingdom'] =='Animalia')]

##Obtener el número de registros para cada reino
#numSpEndemicas=registros.groupby(["kingdom"])["kingdom"].count()

##Condicional para registros marinos, continentales y salobres
##Este permite eliminar la categoría de marino para registros que no pertenecen a municipios con zonas marinas
if tipo =='MCDM' or tipo =='DCDM':   
    registros.loc[(registros['stateProvince'].isin(['Amazonas','Bogotá, D.C.','Caquetá','Cundinamarca'
    ,'Huila','Putumayo','Santander','Vaupés','Arauca','Boyacá','Casanare', 'Guainía','Meta','Quindío',
    'Tolima','Vichada','Caldas','Cesar','Guaviare','Norte de Santander','Risaralda'])), 'isMarine'] = np.nan

#registros=registros.loc[registros.loc[:, 'stateProvince'] == 'Putumayo']

##----------------------------------------------------Cifras totales -----------------------------------------------------------##
'''
En esta sección se calculan las cifras totales para el departamento o el país sin tener en cuenta temáticas de interés, publicadores
o grupos biológicos

'''
##Crear un dataframe vacío
total_cifras=pd.DataFrame(index=[0])

##Crear variable para los conteos por especie general
variable_conteos = registros[registros['species'].notna()].drop_duplicates('species').sort_values(by=['species']) 

##Crear la columna registros y especies general contando el campo gbifID
total_cifras['registros'],total_cifras['especies']=registros['gbifID'].count(),variable_conteos['gbifID'].count()

##Condicional para registros marinos, continentales y salobres
if tipo =='MCDM' or tipo =='DCDM':
    ##Crear un subconjunto de datos para los registros marinos, continentales y salobres
    registros_marinos=registros[registros['isMarine']=='Marine']
    registros_continentales=registros[registros['isTerrestrial']=='Terrestrial']
    registros_salobres=registros[registros['isBrackish']=='Brackish']

    ##Crear variables para los conteos por especie de las categorias marinos, continentales y salobres
    variable_conteos_marinos = registros_marinos[registros_marinos['species'].notna()].drop_duplicates('species').sort_values(by=['species']) #se puede dejar en una sola variable
    variable_conteos_continentales = registros_continentales[registros_continentales['species'].notna()].drop_duplicates('species').sort_values(by=['species']) #se puede dejar en una sola variable
    variable_conteos_salobres = registros_salobres[registros_salobres['species'].notna()].drop_duplicates('species').sort_values(by=['species']) #se puede dejar en una sola variable

    ##Crear las columnas registros marinos y especies marinas
    total_cifras['registrosMarinos'],total_cifras['especiesMarinas']=registros['isMarine'].count(),variable_conteos['isMarine'].count()
    
    ##Crear la columna registros salobres y especies salobres
    total_cifras['registrosSalobres'],total_cifras['especiesSalobres']=registros['isBrackish'].count(),variable_conteos['isBrackish'].count()

    ##Crear las columnas registros continentales y especies continentales
    total_cifras['registrosContinentales'],total_cifras['especiesContinentales']=registros['isTerrestrial'].count(),variable_conteos['isTerrestrial'].count()
    total_cifras.columns=['registros','especies','registros_marinos','especies_marinas','registros_salobres', 'especies_salobres', 'registros_continentales', 'especies_continentales']
    total_cifras=total_cifras[['registros','registros_continentales','registros_marinos','registros_salobres','especies','especies_continentales', 'especies_marinas', 'especies_salobres']]

##crear archivo tsv 
total_cifras.to_csv(nombre+'cifrasTotales.tsv',sep='\t', index=False ) 

##crear archivo excel y la hoja Cifras totales
total_cifras.to_excel(nombre +'cifrasTotales.xlsx', sheet_name='Cifras totales', index=False)


##---------------------------------------------Cifras generales geográficas --------------------------------------------------##
'''
Dentro de esta sección se define el alcance geográfico del conjunto de datos. Se selecciona si se va a trabajar a nivel nacional o
departamental. En caso de que se trabaje a nivel nacional, las cifras geográficas harán referencia a los departamentos. Para el nivel 
departamental las cifras se obtendrán para cada uno de los municipios encontrados dentro del departamento de interés
Dentro de los nombres se encuentran las siguientes convenciones:
rb= Registros biologicos
sp= especies
geo= geografía

'''
## Definir el alcance de las cifras geográficas
## Seleccionar 'stateProvince' para cifras Nacionales, 'county' para cifras departamentales
if tipo =='MCDM' or tipo =='MSDM':
    geografia='county' 
if tipo =='DCDM' or tipo =='DSDM':
    geografia='stateProvince' 

##Crear dataframes vacíos para almacenar las cifras geográficas de especies y registros
geo_rb_total=pd.DataFrame()
geo_sp_total=pd.DataFrame()

##Cifras registros por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geográficas y contar los registros generales
##Crear columna registros
geo_rb_total['registros']= registros.groupby(geografia)['gbifID'].count()


##Cifras especies por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geografica y contar los registros por species
##Crear un dataframe con los datos. Agrupar por entidad geografica y contar los registros por species
geo_sp_total= registros.groupby([geografia,'species'])['species'].count().to_frame(name = 'registros').reset_index()
geo_sp_total= geo_sp_total.groupby([geografia])['species'].count().to_frame(name = 'especies').reset_index().fillna('').sort_values(geografia)



if tipo =='MCDM' or tipo =='DCDM':
    
    ##Cifras registros por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geográfica y contar los registros para cada una de las categorías (marinos, salobres y continentales)
    ##Crear columna registros para datos marinos
    geo_rb_total['registrosMarinos']= registros.groupby(geografia)['isMarine'].count()
    ##Crear columna registros para datos salobres
    geo_rb_total['registrosSalobres']= registros.groupby(geografia)['isBrackish'].count()
    ##Crear columna registros para datos continentales
    geo_rb_total['registrosContinentales']= registros.groupby(geografia)['isTerrestrial'].count()

    ##Cifras especies por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geográfica y contar los registros por species para cada una de las categorías (marinos, salobres y continentales)
    ##Crear conjunto de datos con especies y geografía para datos marinos y Contar número de especies marinos para entidad geográfica 
    geo_sp_marino=registros.groupby([geografia,'species','isMarine'])['isMarine'].count().to_frame(name = 'registros').reset_index()
    #print(geo_sp_marino.groupby(geografia).count())
    geo_sp_marino=geo_sp_marino.groupby(geografia)['registros'].count().to_frame(name = 'especiesMarinas').reset_index()
    
    ##Crear conjunto de datos con especies y geografía para datos salobres y Contar número de especies salobres para entidad geográfica 
    geo_sp_salobre=registros.groupby([geografia,'species','isBrackish'])['isBrackish'].count().to_frame(name = 'registros').reset_index()
    geo_sp_salobre=geo_sp_salobre.groupby(geografia)['registros'].count().to_frame(name = 'especiesSalobres').reset_index()

    ##Crear conjunto de datos con especies y geografía para datos continentales y Contar número de especies continentales para entidad geográfica 
    ##Contar número de especies salobres para entidad geográfica 
    geo_sp_continental=registros.groupby([geografia,'species','isTerrestrial'])['isTerrestrial'].count().to_frame(name = 'registros').reset_index()
    geo_sp_continental=geo_sp_continental.groupby(geografia)['registros'].count().to_frame(name = 'especiesContinentales').reset_index()

    ##Unir todos los conjuntos de datos, Reemplazar los valores NaN y ordenar las entidades geográficas
    geo_sp_total=pd.merge(geo_sp_total,geo_sp_salobre, on=[geografia],how='left').merge(geo_sp_marino, on=[geografia],how='left').merge(geo_sp_continental, on=[geografia],how='left').fillna('').sort_values(geografia)

##Quitar los .0
geo_sp_total=geo_sp_total.astype(str)
geo_sp_total=geo_sp_total.replace(to_replace='\.0+$',value="",regex=True)
geo_rb_total=geo_rb_total.astype(str)
geo_rb_total=geo_rb_total.replace(to_replace='\.0+$',value="",regex=True)

##---------------------------------------------Cifras generales organizaciones publicadoras---------------------------------##
'''
Se obtienen las cifras de especies y registros para todas las organizaciones publicadoras que se encuentran representadas en 
el conjunto de datos

'''
##Quitar el texto: "País publicador:" del campo 'publishingCountry'
registros=registros.replace('País publicador: ','',regex=True)
registros.loc[registros.organization=='iNaturalist.org','publishingCountry']='CO'

##Crear dataframes vacíos para almacenar las cifras de entidades publicadoras
entidades_registros=pd.DataFrame()
entidades_total=pd.DataFrame()

##Crear dataframe con los nombres, país y logo de las organizaciones publicadoras
entidades =pd.DataFrame()
#entidades['publishingOrgKey'],entidades['organization'],entidades['publishingCountry'],entidades['logoUrl']=registros['publishingOrgKey'],registros['organization'],registros['publishingCountry'],registros['logoUrl']
entidades['publishingOrgKey'],entidades['organization'],entidades['publishingCountry'],entidades['logoUrl']=registros['publishingOrgKey'],registros['organization'],registros['publishingCountry'],registros['logoUrl']

##Condicional para registros marinos, continentales y salobres
if tipo =='MCDM' or tipo =='DCDM':
    ## Cifras registros por organización publicadora: Crear un dataframe con los datos, agrupar por organización y contar los registros para cada una de las categorías (marinos, salobres y continentales)
    ##Crear conjunto de datos con registros y organización para datos marinos, salobres y continentales
    entidades_registros['registros'],entidades_registros['registrosMarinos'],entidades_registros['registrosSalobres'],entidades_registros['registrosContinentales']= registros.groupby(['organization'])['gbifID'].count(),registros.groupby(['organization'])['isMarine'].count(),registros.groupby(['organization'])['isBrackish'].count(),registros.groupby(['organization'])['isTerrestrial'].count()
    #entidades_registros['registros'],entidades_registros['registrosMarinos'],entidades_registros['registrosSalobres'],entidades_registros['registrosContinentales']= registros.groupby(['organization'])['gbifID'].count(),registros.groupby(['organization'])['isMarine'].count(),registros.groupby(['organization'])['isBrackish'].count(),registros.groupby(['organization'])['isTerrestrial'].count()
    
    ## Cifras especies por organización publicadora: Crear un dataframe con los datos, agrupar por organización y especie, contar los registros por species para cada una de las categorías (marinos, salobres y continentales)
    entidades_sp= registros.groupby(['organization','species'])['species'].count().to_frame(name = 'especies').reset_index()
    entidades_sp= entidades_sp.groupby(['organization'])['especies'].count().to_frame(name = 'especies').reset_index() 
    
    ##Crear conjunto de datos con especies y entidad publicadora para datos marinos
    ##Contar número de especies marinos para entidad publicadora 
    ent_sp_marino= registros.groupby(['organization','species','isMarine'])['species'].count().to_frame(name = 'especies').reset_index()
    ent_sp_marino= ent_sp_marino.groupby(['organization'])['especies'].count().to_frame(name = 'especiesMarinas').reset_index() 

    ##Crear conjunto de datos con especies y entidad publicadora para datos salobres
    ##Contar número de especies salobres para entidad publicadora 
    ent_sp_salobre= registros.groupby(['organization','species','isBrackish'])['species'].count().to_frame(name = 'especies').reset_index()
    ent_sp_salobre= ent_sp_salobre.groupby(['organization'])['especies'].count().to_frame(name = 'especiesSalobres').reset_index() 

    ##Crear conjunto de datos con especies y entidad publicadora para datos continentales
    ##Contar número de especies continentales para entidad publicadora
    ent_sp_continental= registros.groupby(['organization','species','isTerrestrial'])['species'].count().to_frame(name = 'especies').reset_index() 
    ent_sp_continental= ent_sp_continental.groupby(['organization'])['especies'].count().to_frame(name = 'especiesContinentales').reset_index() 

    ##Unir todos los conjuntos de datos, Reemplazar los valores NaN y ordenar las entidades geográficas
    entidades_total=registros.groupby(['organization','publishingCountry'])['organization'].count().to_frame(name = 'especies').reset_index().drop('especies', axis=1)  
    entidades_total=pd.merge(entidades_total,entidades_sp, on=['organization'],how='left').merge(ent_sp_salobre, on=['organization'],how='left').merge(ent_sp_marino, on=['organization'],how='left').merge(ent_sp_continental, on=['organization'],how='left').merge(entidades_registros, on=['organization'],how='left').sort_values('organization').fillna('')

    ##Agregar la información del logo y seleccionar las columnas finales
    entidades_total=pd.merge(entidades_total,entidades, on=['organization','publishingCountry'],how='left')   
    #aggregation_functions = {'publishingCountry': 'first', 'logoUrl':'first','registros':'first','registrosContinentales':'first','registrosSalobres':'first','registrosMarinos':'first','especies': 'first','especiesSalobres': 'first','especiesMarinas': 'first','especiesContinentales': 'first'}
    aggregation_functions = {'publishingOrgKey': 'first', 'publishingCountry': 'first', 'logoUrl':'first','registros':'first','registrosContinentales':'first','registrosSalobres':'first','registrosMarinos':'first','especies': 'first','especiesSalobres': 'first','especiesMarinas': 'first','especiesContinentales': 'first'}
    entidades_total = entidades_total.groupby(['organization']).aggregate(aggregation_functions).reset_index().fillna('')
    
    ##completar con la información del reporte mensual
    entidades_total=pd.merge(entidades_total,entidades_reporte, on=['organization'],how='left')
    ##Asignar la URL del logo proveniente del reporte
    entidades_total.loc[(entidades_total.logoUrl!=entidades_total.Logo) & (entidades_total.Logo!=np.nan),'logoUrl']=entidades_total.Logo
    entidades_total=entidades_total.drop(['Logo'], axis='columns')
    aggregation_functions = {'publishingOrgKey': 'first', 'publishingCountry': 'first', 'logoUrl':'first','URLSocio':'first', 'typeOrg':'first','registros':'first','registrosSalobres':'first','registrosMarinos':'first','registrosContinentales':'first','especies': 'first','especiesSalobres': 'first','especiesMarinas': 'first','especiesContinentales': 'first'}
    #aggregation_functions = {'publishingCountry': 'first', 'logoUrl':'first', 'typeOrg':'first','registros':'first','registrosSalobres':'first','registrosMarinos':'first','registrosContinentales':'first','especies': 'first','especiesSalobres': 'first','especiesMarinas': 'first','especiesContinentales': 'first'}
    entidades_total = entidades_total.groupby(['organization']).aggregate(aggregation_functions).reset_index().fillna('')

else:
    ## Cifras registros por organización publicadora: Crear un dataframe con los datos agrupar por organización y contar los registros generales
    entidades_registros['registros']= registros.groupby(['organization'])['gbifID'].count()
    
    ##Crear conjunto de datos con especies y entidad publicadora general
    ##Contar número de especies para entidad publicadora general
    entidades_sp= registros.groupby(['organization','species'])['species'].count().to_frame(name = 'especies').reset_index()
    entidades_sp= entidades_sp.groupby(['organization'])['especies'].count().to_frame(name = 'especies').reset_index() 

    ##Unir los conteos de registros y especies por entidad general
    entidades_total=pd.merge(entidades_registros,entidades_sp, on=['organization'],how='left').sort_values('organization').fillna('')
    
    ##Agregar la información del logo y seleccionar las columnas finales
    entidades_total=pd.merge(entidades_total,entidades, on=['organization'],how='left')
    aggregation_functions = {'publishingOrgKey': 'first', 'publishingCountry': 'first', 'logoUrl':'first','registros':'first','especies': 'first'}
    #aggregation_functions = {'publishingCountry': 'first', 'logoUrl':'first','registros':'first','especies': 'first'}
    entidades_total = entidades_total.groupby(['organization']).aggregate(aggregation_functions).reset_index().fillna('')

    ##completar con la información del reporte mensual
    entidades_total=pd.merge(entidades_total,entidades_reporte, on=['organization'],how='left')
    ##Asignar la URL del logo proveniente del reporte
    entidades_total.loc[(entidades_total.logoUrl!=entidades_total.Logo) & (entidades_total.Logo!=np.nan),'logoUrl']=entidades_total.Logo
    entidades_total=entidades_total.drop(['Logo'], axis='columns')
    aggregation_functions = {'publishingOrgKey': 'first', 'publishingCountry': 'first', 'logoUrl':'first','URLSocio':'first', 'typeOrg':'first','registros':'first','especies': 'first'}
    #aggregation_functions = {'publishingCountry': 'first', 'logoUrl':'first', 'typeOrg':'first','registros':'first','especies': 'first'}
    entidades_total = entidades_total.groupby(['organization']).aggregate(aggregation_functions).reset_index().fillna('')

##Quitar los .0
entidades_total=entidades_total.astype(str)
entidades_total=entidades_total.replace(to_replace='\.0+$',value="",regex=True)

##Reemplazar los nombres y la url del logo para las entidades que lo requieran

entidades_total.loc[entidades_total.organization=='iNaturalist.org','organization']='Naturalista Colombia'
entidades_total.loc[entidades_total.publishingCountry=='CO','tipoPublicador']='Nacional'
entidades_total.loc[entidades_total.publishingCountry!='CO','tipoPublicador']='Internacional'
entidades_total.loc[entidades_total.organization=='Cornell Lab of Ornithology','typeOrg']='Redes/Iniciativas'
entidades_total.loc[entidades_total.organization=='Cornell Lab of Ornithology','organization']='eBird Colombia'
entidades_total.loc[entidades_total.tipoPublicador=='Internacional','logoUrl']='https://statics.sibcolombia.net/sib-resources/images/santander/world.png'
entidades_total.loc[(entidades_total.URLSocio==np.nan),'URLSocio']='https://www.gbif.org/publisher/'+str(entidades_total.publishingOrgKey)

##Exportar los datos a archivos tsv y xlsx
if tipo =='MCDM' or tipo =='DCDM':
    entidades_total.columns=['oganizacion','organizacion_id','pais_publicacion','url_logo','url_socio','tipo_organizacion','registros','registros_salobres','registros_marinos','registros_continentales','especies','especies_salobres','especies_marinas','especies_continentales','tipo_publicador']
    entidades_total=entidades_total[['oganizacion','organizacion_id','pais_publicacion','tipo_organizacion','tipo_publicador','url_logo','url_socio','registros','registros_continentales','registros_marinos','registros_salobres','especies','especies_continentales','especies_marinas','especies_salobres']]
else:
    #entidades_total.columns=['oganizacion','organizacion_id','pais_publicacion','url_logo','tipo_organizacion','registros','especies','tipo_publicador']
    #entidades_total=entidades_total[['organizacion_id','oganizacion','pais_publicacion','tipo_organizacion','tipo_publicador','url_logo','registros','especies']]

    entidades_total.columns=['oganizacion','organizacion_id','pais_publicacion','url_logo','url_socio','tipo_organizacion','registros','especies','tipo_publicador']
    entidades_total=entidades_total[['oganizacion','organizacion_id','pais_publicacion','tipo_organizacion','tipo_publicador','url_logo','url_socio','registros','especies']]

entidades_total.to_csv(nombre+'cifrasOrganizaciones.tsv',sep='\t', index=False )
entidades_total.to_excel(nombre+'cifrasOrganizaciones.xlsx', sheet_name='cifrasEntidades', index=False )

##-------------------------------------------------Loop calculo cifras ---------------------------------------------------##
'''
El loop permite iterar en el conjunto de datos, realizando diferentes procesos y creación de variables, teniendo en cuenta 
las categorias tematicas y los grupos taxonómicos 

Durante el loop se realiza el conteo de los registros biologicos y el número de especies para las diferenes categorias. Dentro de 
los nombres se encuentran las siguientes convenciones:
rb= Registros biologicos
sp= especies
geo= geografía
'''

## Crear listas con las categorías tematicas sobre las cuales se corre el loop
tematica =['threatStatus_UICN','especies_invasoras','appendixCITES','threatStatus_MADS','especies_exoticas','especies_exotica_riesgo_invasion','endemic','migratory'] 

## Define una lista con todos las categorias taxonomicas
taxon = ['kingdom','phylum','class','order','family','genus','species'] 

## Creación de dataframes vacíos para guardar las cifras generadas en el loop
## Dataframes para registros
rb_taxon_total=pd.DataFrame()
rb_tematica_total=pd.DataFrame()
rb_taxon_tematica_total=pd.DataFrame()
rb_taxon_categoria_total=pd.DataFrame()

## Dataframes para especies
sp_taxon_total=pd.DataFrame()
sp_tematica_total=pd.DataFrame()
sp_taxon_tematica_total=pd.DataFrame()
sp_taxon_categoria_total=pd.DataFrame()

## Dataframes geográficos
geo_tematica_rb_total=pd.DataFrame() 
geo_tematica_sp_total=pd.DataFrame()
geo_categoria_rb_total=pd.DataFrame()
geo_categoria_sp_total=pd.DataFrame()


## La primera parte del loop recorre las categorias temáticas 
for i in tematica:
    
    ## Cifras registros: Agrupar por categorías temáticas y contar por gbifID generales
    rb_numero = registros.groupby(i)['gbifID'].count().to_frame(name = 'registros' ).reset_index()
    ##Asignar al campo 'thematic' el valor que tiene i 
    rb_numero['thematic']=i
    rb_numero=rb_numero.rename(columns={i:'category'})

    ##Condicional para registros marinos, continentales y salobres
    if tipo =='MCDM' or tipo =='DCDM':
        ## Cifras registros marinos: Agrupar por categorías temáticas y contar por gbifID con categoría marinos
        rb_numero_marino = registros_marinos.groupby([i,'isMarine'])['gbifID'].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
        rb_numero_marino['thematic']=i
        rb_numero_marino=rb_numero_marino.rename(columns={i:'category'})

        ## Cifras registros continentales: Agrupar por categorías temáticas y contar por gbifID con categoría continental
        rb_numero_continental = registros_continentales.groupby([i,'isTerrestrial'])['gbifID'].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
        rb_numero_continental['thematic']=i
        rb_numero_continental=rb_numero_continental.rename(columns={i:'category'})

        ## Cifras registros salobres: Agrupar por categorías temáticas y contar por gbifID con categoría salobre    
        rb_num_salobre = registros_salobres.groupby([i,'isBrackish'])['gbifID'].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
        rb_num_salobre['thematic']=i
        rb_num_salobre=rb_num_salobre.rename(columns={i:'category'})

        ##Agrupar las cifras marinas, continentales y salobres
        rb_tematica=pd.merge(rb_numero,rb_numero_continental, on=['category','thematic'],how='left').merge(rb_numero_marino, on=['category','thematic'],how='left').merge(rb_num_salobre, on=['category','thematic'],how='left')
        ##Almacenar la información de cada recorrido del loop
        rb_tematica_total=pd.concat([rb_tematica_total,rb_tematica])

    else:
        ##Almacenar la información de cada recorrido del loop
        rb_tematica_total=pd.concat([rb_tematica_total,rb_numero])

    ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID generales
    sp_numero = variable_conteos.groupby(i)['gbifID'].count().to_frame(name = 'especies' ).reset_index()
    sp_numero['thematic']=i
    sp_numero=sp_numero.rename(columns={i:'category'})

    ##Condicional para registros marinos, continentales y salobres
    if tipo =='MCDM' or tipo =='DCDM':
        ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID para la categoría marinos
        sp_numero_marino = variable_conteos_marinos.groupby([i,'isMarine'])['gbifID'].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
        sp_numero_marino['thematic']=i
        sp_numero_marino=sp_numero_marino.rename(columns={i:'category'})    

        ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID para la categoría continental
        sp_numero_continental = variable_conteos_continentales.groupby([i,'isTerrestrial'])['gbifID'].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
        sp_numero_continental['thematic']=i
        sp_numero_continental=sp_numero_continental.rename(columns={i:'category'})  

        ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID para la categoría salobre
        sp_numero_salobre = variable_conteos_salobres.groupby([i,'isBrackish'])['gbifID'].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
        sp_numero_salobre['thematic']=i
        sp_numero_salobre=sp_numero_salobre.rename(columns={i:'category'})  

        ##Agrupar las cifras marinas, continentales y salobres
        sp_tematica=pd.merge(sp_numero,sp_numero_continental, on=['category','thematic'],how='left').merge(sp_numero_marino, on=['category','thematic'],how='left').merge(sp_numero_salobre, on=['category','thematic'],how='left')
        sp_tematica_total=pd.concat([sp_tematica_total,sp_tematica])

    else:
        sp_tematica_total=pd.concat([sp_tematica_total,sp_numero])

    ## Cifras registros por entidad geográfica: Agrupar por geografía y categorías temáticas, contar por temática generales
    geo_rb= registros.groupby([geografia,i])[i].count().to_frame(name = 'registros').reset_index()
    geo_rb['thematic']=i
    geo_rb=geo_rb.rename(columns={i:'category'})

    ##Condicional para registros marinos, continentales y salobres
    if tipo =='MCDM' or tipo =='DCDM':
        ## Cifras registros por entidad geográfica: Agrupar por geografía y categorías temáticas, contar por temática para marinos
        geo_rb_marino= registros_marinos.groupby([geografia,i,'isMarine'])[i].count().to_frame(name = 'registrosMarinos').reset_index().drop(['isMarine'], axis=1)
        geo_rb_marino['thematic']=i
        geo_rb_marino=geo_rb_marino.rename(columns={i:'category'})

        ## Cifras registros por entidad geográfica: Agrupar por geografía y categorías temáticas, contar por temática para continentales
        geo_rb_continental= registros_continentales.groupby([geografia,i,'isTerrestrial'])[i].count().to_frame(name = 'registrosContinentales').reset_index().drop(['isTerrestrial'], axis=1)
        geo_rb_continental['thematic']=i
        geo_rb_continental=geo_rb_continental.rename(columns={i:'category'})

        ## Cifras registros por entidad geográfica: Agrupar por geografía y categorías temáticas, contar por temática para salobres
        geo_rb_salobre= registros_salobres.groupby([geografia,i,'isBrackish'])[i].count().to_frame(name = 'registrosSalobres').reset_index().drop(['isBrackish'], axis=1)
        geo_rb_salobre['thematic']=i
        geo_rb_salobre=geo_rb_salobre.rename(columns={i:'category'})

        ##Agrupar las cifras marinas, continentales y salobres
        geo_categoria_rb=pd.merge(geo_rb,geo_rb_continental, on=[geografia,'category','thematic'],how='left').merge(geo_rb_marino, on=[geografia,'category','thematic'],how='left').merge(geo_rb_salobre, on=[geografia,'category','thematic'],how='left')
        geo_categoria_rb_total=pd.concat([geo_categoria_rb_total,geo_categoria_rb])

    else:    
        geo_categoria_rb_total=pd.concat([geo_categoria_rb_total,geo_rb])
    
    ## Cifras especies por entidad geográfica: Agrupar por geografía y categorías temáticas y categorías de cada temática general categorías temáticas, contar por temática para marinos, continentales y salobres
    geo_sp= registros.groupby([geografia,i,'species']).size().reset_index().groupby([geografia,i]).size().to_frame(name = 'especies').reset_index()
    geo_sp['thematic']=i
    geo_sp= geo_sp.rename(columns={i:'category'})

    ##Condicional para registros marinos, continentales y salobres
    if tipo =='MCDM' or tipo =='DCDM':
        
        ## Cifras especies por entidad geográfica: Agrupar por geografia y categorias temáticas y categorías de cada temática para marinos
        geo_sp_marino= registros_marinos.groupby([geografia,i,'species','isMarine']).size().reset_index().groupby([geografia,i]).size().to_frame(name = 'especiesMarinas').reset_index()
        geo_sp_marino['thematic']=i
        geo_sp_marino= geo_sp_marino.rename(columns={i:'category'})

        ## Cifras especies por entidad geográfica: Agrupar por geografia y categorias temáticas y categorías de cada temática para continentales
        geo_sp_continental= registros_continentales.groupby([geografia,i,'species','isTerrestrial']).size().reset_index().groupby([geografia,i]).size().to_frame(name = 'especiesContinentales').reset_index()
        geo_sp_continental['thematic']=i
        geo_sp_continental= geo_sp_continental.rename(columns={i:'category'})

        ## Cifras especies por entidad geográfica: Agrupar por geografia y categorias temáticas y categorías de cada temática para salobres
        geo_sp_salobre= registros_salobres.groupby([geografia,i,'species','isBrackish']).size().reset_index().groupby([geografia,i]).size().to_frame(name = 'especiesSalobres').reset_index()
        geo_sp_salobre['thematic']=i
        geo_sp_salobre= geo_sp_salobre.rename(columns={i:'category'})
    
        ##Agrupar las cifras marinas, continentales y salobres
        geo_categoria_sp=pd.merge(geo_sp,geo_sp_continental, on=[geografia,'category','thematic'],how='left').merge(geo_sp_marino, on=[geografia,'category','thematic'],how='left').merge(geo_sp_salobre, on=[geografia,'category','thematic'],how='left')
        geo_categoria_sp_total=pd.concat([geo_categoria_sp_total,geo_categoria_sp])
    
    else:
        geo_categoria_sp_total=pd.concat([geo_categoria_sp_total,geo_sp])

    ## Conteo de total registros biológicos para todas las categorías de cada temática general
    geo_rb = registros.groupby([geografia])[i].count().to_frame(name = 'registros').reset_index()
    geo_rb['thematic']=i
    geo_rb=geo_rb.rename(columns={i:'category'})

    ##Condicional para registros marinos, continentales y salobres
    if tipo =='MCDM' or tipo =='DCDM':
 
        ## Conteo de total registros biológicos para todas las categorías de cada temática para marinos
        geo_rb_marino = registros_marinos.groupby([geografia,'isMarine'])[i].count().to_frame(name = 'registrosMarinos').reset_index().drop(['isMarine'], axis=1)
        geo_rb_marino['thematic']=i
        geo_rb_marino=geo_rb_marino.rename(columns={i:'category'})

        ## Conteo de total registros biológicos para todas las categorías de cada temática para continentales
        geo_rb_continental = registros_continentales.groupby([geografia,'isTerrestrial'])[i].count().to_frame(name = 'registrosContinentales').reset_index().drop(['isTerrestrial'], axis=1)
        geo_rb_continental['thematic']=i
        geo_rb_continental=geo_rb_continental.rename(columns={i:'category'})

        ## Conteo de total registros biológicos para todas las categorías de cada temática para salobres
        geo_rb_salobre = registros_salobres.groupby([geografia,'isBrackish'])[i].count().to_frame(name = 'registrosSalobres').reset_index().drop(['isBrackish'], axis=1)
        geo_rb_salobre['thematic']=i
        geo_rb_salobre=geo_rb_salobre.rename(columns={i:'category'})    
    
        ##Agrupar las cifras marinas, continentales y salobres
        geo_the_rb=pd.merge(geo_rb,geo_rb_continental, on=[geografia,'thematic'],how='left').merge(geo_rb_marino, on=[geografia,'thematic'],how='left').merge(geo_rb_salobre, on=[geografia,'thematic'],how='left')
        geo_tematica_rb_total=pd.concat([geo_tematica_rb_total,geo_the_rb])

    else:
        geo_tematica_rb_total=pd.concat([geo_tematica_rb_total,geo_rb])

    ## Conteo de total especies únicas para todas las categorías temáticas generales
    geo_sp= registros.groupby([geografia,i,'species']).size().reset_index().groupby([geografia])[i].count().to_frame(name = 'especies').reset_index()
    geo_sp['thematic']=i
    geo_sp=geo_sp.rename(columns={i:'category'})

    ##Condicional para registros marinos, continentales y salobres
    if tipo =='MCDM' or tipo =='DCDM':

        ## Conteo de total especies únicas para todas las categorias para marinos
        geo_sp_marino= registros_marinos.groupby([geografia,i,'species','isMarine']).size().reset_index().groupby([geografia])[i].count().to_frame(name = 'especiesMarinas').reset_index()
        geo_sp_marino['thematic']=i
        geo_sp_marino=geo_sp_marino.rename(columns={i:'category'})

        ## Conteo de total especies únicas para todas las categorias para continentales
        geo_sp_continental= registros_continentales.groupby([geografia,i,'species','isTerrestrial']).size().reset_index().groupby([geografia])[i].count().to_frame(name = 'especiesContinentales').reset_index()
        geo_sp_continental['thematic']=i
        geo_sp_continental=geo_sp_continental.rename(columns={i:'category'})

        ## Conteo de total especies únicas para todas las categorias para salobres
        geo_sp_salobre= registros_salobres.groupby([geografia,i,'species','isBrackish']).size().reset_index().groupby([geografia])[i].count().to_frame(name = 'especiesSalobres').reset_index()
        geo_sp_salobre['thematic']=i
        geo_sp_salobre=geo_sp_salobre.rename(columns={i:'category'})

        ##Agrupar las cifras marinas, continentales y salobres
        geo_the_sp=pd.merge(geo_sp,geo_sp_continental, on=[geografia,'thematic'],how='left').merge(geo_sp_marino, on=[geografia,'thematic'],how='left').merge(geo_sp_salobre, on=[geografia,'thematic'],how='left')
        geo_tematica_sp_total=pd.concat([geo_tematica_sp_total,geo_the_sp])

    else:
        geo_tematica_sp_total=pd.concat([geo_tematica_sp_total,geo_sp])

    ## La segunda parte del loop recorre los grupos taxonómicos
    for j in taxon:    
        ##Número de registros por grupo taxonómico general
        rb_numero = registros.groupby(j)['gbifID'].count().to_frame(name = 'registros' ).reset_index()
        rb_numero['taxonRank']=j
        rb_numero=rb_numero.rename(columns={j:'grupoTax'}) 

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='MCDM' or tipo =='DCDM': 

            ##Número de registros por grupo taxonómico para marinos
            rb_numero_marino = registros_marinos.groupby([j,'isMarine'])['gbifID'].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
            rb_numero_marino['taxonRank']=j
            rb_numero_marino=rb_numero_marino.rename(columns={j:'grupoTax'}) 

            ##Número de registros por grupo taxonómico para continentales
            rb_numero_continental = registros_continentales.groupby([j,'isTerrestrial'])['gbifID'].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            rb_numero_continental['taxonRank']=j
            rb_numero_continental=rb_numero_continental.rename(columns={j:'grupoTax'}) 

            ##Número de registros por grupo taxonómico para salobres
            rb_num_salobre = registros_salobres.groupby([j,'isBrackish'])['gbifID'].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            rb_num_salobre ['taxonRank']=j
            rb_num_salobre=rb_num_salobre.rename(columns={j:'grupoTax'}) 

            ##Agrupar las cifras marinas, continentales y salobres
            rb_tax=pd.merge(rb_numero,rb_numero_continental, on=['taxonRank','grupoTax'],how='left').merge(rb_numero_marino, on=['taxonRank','grupoTax'],how='left').merge(rb_num_salobre, on=['taxonRank','grupoTax'],how='left')
            rb_taxon_total=pd.concat([rb_taxon_total,rb_tax])
        else:
            rb_taxon_total=pd.concat([rb_taxon_total,rb_numero])

        ## Número de registros por grupo taxonómico en la categoría general
        rb_numero = registros.groupby(j)[i].count().to_frame(name = 'registros' ).reset_index()
        rb_numero['thematic']=i
        rb_numero['taxonRank']=j
        rb_numero=rb_numero.rename(columns={j:'grupoTax'})
        rb_numero=rb_numero.rename(columns={i:'category'})

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='MCDM' or tipo =='DCDM':   
            ## Número de registros por grupo taxonómico en la categoría para marinos
            rb_numero_marino = registros_marinos.groupby([j,'isMarine'])[i].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
            rb_numero_marino['thematic']=i
            rb_numero_marino['taxonRank']=j
            rb_numero_marino=rb_numero_marino.rename(columns={j:'grupoTax'})
            rb_numero_marino=rb_numero_marino.rename(columns={i:'category'})

            ## Número de registros por grupo taxonómico en la categoría para continentales
            rb_numero_continental = registros_continentales.groupby([j,'isTerrestrial'])[i].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            rb_numero_continental['thematic']=i
            rb_numero_continental['taxonRank']=j
            rb_numero_continental=rb_numero_continental.rename(columns={j:'grupoTax'})
            rb_numero_continental=rb_numero_continental.rename(columns={i:'category'})

            ## Número de registros por grupo taxonómico en la categoría para salobres
            rb_num_salobre = registros_salobres.groupby([j,'isBrackish'])[i].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            rb_num_salobre['thematic']=i
            rb_num_salobre['taxonRank']=j
            rb_num_salobre=rb_num_salobre.rename(columns={j:'grupoTax'})
            rb_num_salobre=rb_num_salobre.rename(columns={i:'category'})        
        
            ##Agrupar las cifras marinas, continentales y salobres
            rb_tax_the=pd.merge(rb_numero,rb_numero_continental, on=['taxonRank','grupoTax','thematic'],how='left').merge(rb_numero_marino, on=['taxonRank','grupoTax','thematic'],how='left').merge(rb_num_salobre, on=['taxonRank','grupoTax','thematic'],how='left')
            rb_taxon_tematica_total=pd.concat([rb_taxon_tematica_total,rb_tax_the])
        else:
            rb_taxon_tematica_total=pd.concat([rb_taxon_tematica_total,rb_numero])

        ## Número de registros por grupo taxonómico y categoría temática en general
        rb_numero = registros.groupby([j,i])[i].count().to_frame(name = 'registros' ).reset_index()
        rb_numero['thematic']=i
        rb_numero['taxonRank']=j
        rb_numero=rb_numero.rename(columns={j:'grupoTax'})
        rb_numero=rb_numero.rename(columns={i:'category'})

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='MCDM' or tipo =='DCDM':
            ## Número de registros por grupo taxonómico y categoría temática para marinos
            rb_numero_marino = registros_marinos.groupby([j,i,'isMarine'])[i].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
            rb_numero_marino['thematic']=i
            rb_numero_marino['taxonRank']=j
            rb_numero_marino=rb_numero_marino.rename(columns={j:'grupoTax'})
            rb_numero_marino=rb_numero_marino.rename(columns={i:'category'})

            ## Número de registros por grupo taxonómico y categoría temática para continentales
            rb_numero_continental = registros_continentales.groupby([j,i,'isTerrestrial'])[i].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            rb_numero_continental['thematic']=i
            rb_numero_continental['taxonRank']=j
            rb_numero_continental=rb_numero_continental.rename(columns={j:'grupoTax'})
            rb_numero_continental=rb_numero_continental.rename(columns={i:'category'})    

            ## Número de registros por grupo taxonómico y categoría temática para salobres
            rb_num_salobre = registros_salobres.groupby([j,i,'isBrackish'])[i].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            rb_num_salobre['thematic']=i
            rb_num_salobre['taxonRank']=j
            rb_num_salobre=rb_num_salobre.rename(columns={j:'grupoTax'})
            rb_num_salobre=rb_num_salobre.rename(columns={i:'category'})    

            ##Agrupar las cifras marinas, continentales y salobres
            rb_taxon_categoria=pd.merge(rb_numero,rb_numero_continental, on=['taxonRank','grupoTax','category','thematic'],how='left').merge(rb_numero_marino, on=['taxonRank','grupoTax','category','thematic'],how='left').merge(rb_num_salobre, on=['taxonRank','grupoTax','category','thematic'],how='left') 
            rb_taxon_categoria_total=pd.concat([rb_taxon_categoria_total,rb_taxon_categoria])
        else:
            rb_taxon_categoria_total=pd.concat([rb_taxon_categoria_total,rb_numero])

        ## Especies 
        ## Número de especies por grupo taxonómico general
        sp_numero = variable_conteos.groupby(j)['gbifID'].count().to_frame(name = 'especies' ).reset_index()
        sp_numero['taxonRank']=j
        sp_numero=sp_numero.rename(columns={j:'grupoTax'})

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='MCDM' or tipo =='DCDM':
            ## Número de especies por grupo taxonómico para marinos
            sp_numero_marino = variable_conteos_marinos.groupby([j,'isMarine'])['gbifID'].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            sp_numero_marino['taxonRank']=j
            sp_numero_marino=sp_numero_marino.rename(columns={j:'grupoTax'})        

            ## Número de especies por grupo taxonómico para continentales
            sp_numero_continental = variable_conteos_continentales.groupby([j,'isTerrestrial'])['gbifID'].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            sp_numero_continental['taxonRank']=j
            sp_numero_continental=sp_numero_continental.rename(columns={j:'grupoTax'})   

            ## Número de especies por grupo taxonómico para salobres
            sp_numero_salobre = variable_conteos_salobres.groupby([j,'isBrackish'])['gbifID'].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            sp_numero_salobre['taxonRank']=j
            sp_numero_salobre=sp_numero_salobre.rename(columns={j:'grupoTax'})   

            ##Agrupar las cifras marinas, continentales y salobres
            sp_taxon=pd.merge(sp_numero,sp_numero_continental, on=['taxonRank','grupoTax'],how='left').merge(sp_numero_marino, on=['taxonRank','grupoTax'],how='left').merge(sp_numero_salobre, on=['taxonRank','grupoTax'],how='left')  
            sp_taxon_total=pd.concat([sp_taxon_total,sp_taxon])   
        else:
            sp_taxon_total=pd.concat([sp_taxon_total,sp_numero])

        ## Número de especies por grupo taxonómico, categoría y temática, general
        sp_numero = variable_conteos.groupby(j)[i].count().to_frame(name = 'especies' ).reset_index()
        sp_numero['thematic']=i
        sp_numero['taxonRank']=j
        sp_numero=sp_numero.rename(columns={j:'grupoTax'})
        sp_numero=sp_numero.rename(columns={i:'category'})

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='MCDM' or tipo =='DCDM':
            ## Número de especies por grupo taxonómico, categoría y temática para marinos
            sp_numero_marino = variable_conteos_marinos.groupby([j,'isMarine'])[i].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            sp_numero_marino['thematic']=i
            sp_numero_marino['taxonRank']=j
            sp_numero_marino=sp_numero_marino.rename(columns={j:'grupoTax'})
            sp_numero_marino=sp_numero_marino.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría y temática para continentales
            sp_numero_continental = variable_conteos_continentales.groupby([j,'isTerrestrial'])[i].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            sp_numero_continental['thematic']=i
            sp_numero_continental['taxonRank']=j
            sp_numero_continental=sp_numero_continental.rename(columns={j:'grupoTax'})
            sp_numero_continental=sp_numero_continental.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría y temática para salobres
            sp_numero_salobre = variable_conteos_salobres.groupby([j,'isBrackish'])[i].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            sp_numero_salobre['thematic']=i
            sp_numero_salobre['taxonRank']=j
            sp_numero_salobre=sp_numero_salobre.rename(columns={j:'grupoTax'})
            sp_numero_salobre=sp_numero_salobre.rename(columns={i:'category'})

            ##Agrupar las cifras marinas, continentales y salobres
            sp_taxon_tematica=pd.merge(sp_numero,sp_numero_continental, on=['taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_marino, on=['taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_salobre, on=['taxonRank','thematic','grupoTax'],how='left')                
            sp_taxon_tematica_total=pd.concat([sp_taxon_tematica_total,sp_taxon_tematica])   
        else:
            sp_taxon_tematica_total=pd.concat([sp_taxon_tematica_total,sp_numero]) 

        ## Número de especies por grupo taxonómico, categoría temática y temática, general
        sp_numero = variable_conteos.groupby([j,i])[i].count().to_frame(name = 'especies' ).reset_index()
        sp_numero['thematic']=i
        sp_numero['taxonRank']=j
        sp_numero=sp_numero.rename(columns={j:'grupoTax'})
        sp_numero=sp_numero.rename(columns={i:'category'})

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='MCDM' or tipo =='DCDM':
            ## Número de especies por grupo taxonómico, categoría temática y temática, general para marinos
            sp_numero_marino = variable_conteos_marinos.groupby([j,i,'isMarine'])[i].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            sp_numero_marino['thematic']=i
            sp_numero_marino['taxonRank']=j
            sp_numero_marino=sp_numero_marino.rename(columns={j:'grupoTax'})
            sp_numero_marino=sp_numero_marino.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría temática y temática, general para continentales
            sp_numero_continental = variable_conteos_continentales.groupby([j,i,'isTerrestrial'])[i].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            sp_numero_continental['thematic']=i
            sp_numero_continental['taxonRank']=j
            sp_numero_continental=sp_numero_continental.rename(columns={j:'grupoTax'})
            sp_numero_continental=sp_numero_continental.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría temática y temática, general para salobres
            sp_numero_salobre = variable_conteos_salobres.groupby([j,i,'isBrackish'])[i].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            sp_numero_salobre['thematic']=i
            sp_numero_salobre['taxonRank']=j
            sp_numero_salobre=sp_numero_salobre.rename(columns={j:'grupoTax'})
            sp_numero_salobre=sp_numero_salobre.rename(columns={i:'category'})

            ##Agrupar las cifras marinas, continentales y salobres
            sp_taxon_categoria=pd.merge(sp_numero,sp_numero_continental, on=['category','taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_marino, on=['category','taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_salobre, on=['category','taxonRank','thematic','grupoTax'],how='left')  
            sp_taxon_categoria_total=pd.concat([sp_taxon_categoria_total,sp_taxon_categoria])
        else:
            sp_taxon_categoria_total=pd.concat([sp_taxon_categoria_total,sp_numero])
##Fin del loop para realizar conteos

## Agrupar la información: Número de registros y especies por grupo taxonómico en la temática
taxon_tematica=pd.merge(rb_taxon_tematica_total,sp_taxon_tematica_total,on=['taxonRank','thematic','grupoTax'],how='left')

## Agrupar la información: Número de registros y especies por grupo taxonómico en la temática y categoría
taxon_categoria=pd.merge(rb_taxon_categoria_total,sp_taxon_categoria_total,on=['taxonRank','thematic','grupoTax','category'],how='left')

## Guardar archivos de resultado sobre los cuales se puede recalcular cifras según los grupos biológicos
rb_taxon_total.to_csv('registrosBiologicosTaxonTotal.tsv',sep='\t', index=False )
sp_taxon_total.to_csv('especiesTaxonTotal.tsv',sep='\t', index=False )
rb_taxon_tematica_total.to_csv('registrosBiologicosTaxonTematicas.tsv',sep='\t', index=False )
sp_taxon_tematica_total.to_csv('especiesTaxonTematicas.tsv',sep='\t', index=False )
rb_taxon_categoria_total.to_csv('registrosBiologicosTaxonCategoriasTotal.tsv',sep='\t', index=False )
sp_taxon_categoria_total.to_csv('especiesTaxonCategoriasTotal.tsv',sep='\t', index=False )             

##-----------------------------Organización en un único dataframe de las cifras temáticas totales------------------------------##
'''
Se crea el archivo final con el número de especies y registros para todas las temáticas y sus respectivas categorías 

'''
##Unir los conjuntos de datos teniendo en cuenta los campos de thematic y category
tematica_cifras=pd.merge(rb_tematica_total,sp_tematica_total, on=['thematic','category'],how='left') 

##Exportar el archivo a formato tsv o xlsx
if tipo =='MCDM' or tipo =='DCDM':
    tematica_cifras.columns=['categoria','registros','tematica','registros_continentales','registros_marinos', 'registros_salobres','especies','especies_continentales', 'especies_marinas', 'especies_salobres']
    tematica_cifras=tematica_cifras[['tematica','categoria','registros','registros_continentales','registros_marinos', 'registros_salobres','especies','especies_continentales', 'especies_marinas', 'especies_salobres']]
    
else:
    tematica_cifras.columns=['categoria','registros','tematica','especies']
    tematica_cifras=tematica_cifras[['tematica','categoria','registros','especies']]
    
tematica_cifras.to_csv(nombre+'cifrasTematicasTotales.tsv',sep='\t')
tematica_cifras.to_excel(nombre+'cifrasTematicasTotales.xlsx', sheet_name='tematica_cifras', index=False)

##-----------------------------Organización en un único dataframe de las cifras geográficas------------------------------------##
'''
En esta sección se crea el conjunto de datos final para la obtención de las cifras temáticas para cada una de las entidades geográficas
Dentro de los nombres se encuentran las siguientes convenciones:
rb= Registros biologicos
sp= especies
geo= geografía
'''
## Reorganizar los conjuntos de datos
geo_tematica_rb_total=geo_tematica_rb_total.pivot(geografia,'thematic').fillna('')
geo_tematica_sp_total=geo_tematica_sp_total.pivot(geografia,'thematic').fillna('')

##Eliminar columna 'thematic' y reordenar
geo_categoria_rb_total=geo_categoria_rb_total.drop(['thematic'],axis=1)
geo_categoria_rb_total=geo_categoria_rb_total.pivot(geografia,'category').fillna('')
geo_categoria_sp_total=geo_categoria_sp_total.drop(['thematic'],axis=1)
geo_categoria_sp_total=geo_categoria_sp_total.pivot(geografia,'category').fillna('')

##Conjunto de datos final para cifras geográficas
geografia_total=pd.merge(geo_rb_total,geo_sp_total,on=geografia,how='left').merge(geo_tematica_rb_total,on=geografia,how='left').merge(geo_tematica_sp_total,on=geografia,how='left').merge(geo_categoria_rb_total,on=geografia,how='left').merge(geo_categoria_sp_total,on=geografia,how='left')
geografia_total=geografia_total.replace(np.nan,'',regex=True)

##---------------------Transformación cifras por categoría taxonómica a cifras por grupos biológicos----------------------------##
'''
Se carga el archivo guía de grupos biologicos y con este se calculan las cifras tematicas para cada uno de estos
Dentro de los nombres se encuentran las siguientes convenciones:
rb= Registros biologicos
sp= especies
'''
#Asignar los grupos biologicos correspondientes a cada caso
if tipo == 'DCDM' or tipo =='DSDM':
    grupos_biologicosT=grupos_biologicos.loc[grupos_biologicos.loc[:, 'COL'] == 'Aplica']
if tipo =='MCDM':
    grupos_biologicosT=grupos_biologicos.loc[grupos_biologicos.loc[:, 'VRT'] == 'Aplica']
if tipo =='MSDM':
    grupos_biologicos=grupos_biologicos.loc[grupos_biologicos.loc[:, 'VRT'] == 'Aplica']
    
if tipo =='MCDM' or tipo =='DCDM':
    grupos_biologicosM =grupos_biologicos.loc[grupos_biologicos.loc[:, 'SIBM'] == 'Aplica']
    grupos_biologicos = pd.concat([grupos_biologicosT,grupos_biologicosM])

## Resumen cifras registros por grupo biológico general
rb_grupos_biologicos=pd.merge(rb_taxon_total,grupos_biologicos, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 

## Condicional para registros marinos, continentales y salobres
if tipo =='MCDM' or tipo =='DCDM':
    ##Resumen cifras registros por grupo biológico para marinos, continentales y salobres
    rb_grupos_biologicos=rb_grupos_biologicos[['registros','registrosContinentales','registrosMarinos','registrosSalobres','grupoTax','grupoBio']]
else:
    ## Seleccionar las columnas deseadas 
    rb_grupos_biologicos=rb_grupos_biologicos[['registros','grupoTax','grupoBio']]
## Agrupar registros por grupo biológico
rb_cifras=rb_grupos_biologicos.groupby('grupoBio').sum().reset_index()

## Resumen cifras especies por grupo biológico general
sp_grupos_biologicos=pd.merge(sp_taxon_total,grupos_biologicos, on=['grupoTax','taxonRank'],how='left').drop_duplicates()

## Condicional para registros marinos, continentales y salobres
if tipo =='MCDM' or tipo =='DCDM':
    ##Resumen cifras especies por grupo biológico para marinos, continentales y salobres
    sp_grupos_biologicos=sp_grupos_biologicos[['especies','especiesMarinas','especiesContinentales','especiesSalobres','grupoTax','grupoBio']]
else:
    ##Seleccionar las columnas deseadas y agrupar por grupo biológico
    sp_grupos_biologicos=sp_grupos_biologicos[['especies','grupoTax','grupoBio']]


sp_cifras=sp_grupos_biologicos.groupby('grupoBio').sum().reset_index()

## Unión de las cifras de registros y especies para cada grupo biológico
taxon_cifras=pd.merge(rb_cifras,sp_cifras, on=['grupoBio'],how='left') 


## Resumen cifras por grupo biológico y temática general
taxon_tematica_cifras=pd.merge(taxon_tematica,grupos_biologicos, on=['grupoTax','taxonRank'],how='left').drop_duplicates()


## Condicional para registros marinos, continentales y salobres
if tipo =='MCDM' or tipo =='DCDM':
    ##Resumen cifras por grupo biológico y temática para marinos, continentales y salobres
    taxon_tematica_cifras=taxon_tematica_cifras[['registros','registrosContinentales','registrosMarinos','registrosSalobres','especies','especiesMarinas','especiesContinentales','especiesSalobres','grupoBio','thematic']]

else:
    ## Seleccionar las columnas deseadas y agrupar por grupo biológico y temática
    taxon_tematica_cifras=taxon_tematica_cifras[['registros','especies','grupoBio','thematic']]

## Agrupar las cifras por grupo biológico y temática, y reordenar el conjunto de datos
taxon_tematica_cifras=taxon_tematica_cifras.groupby(['grupoBio','thematic']).sum().reset_index()
taxon_tematica_cifras=taxon_tematica_cifras.pivot('grupoBio','thematic').fillna('-') 


## Resumen cifras de especies y registros por grupo biológico y por categoría dentro de cada temática general
taxon_categoria_cifras=pd.merge(taxon_categoria,grupos_biologicos, on=['grupoTax','taxonRank'],how='left').drop_duplicates()

##Condicional para registros marinos, continentales y salobres
if tipo =='MCDM' or tipo =='DCDM':
    #Resumen cifras de especies y registros por grupo biológico y por categoría dentro de cada temática para marinos, continentales y salobres
    taxon_categoria_cifras=taxon_categoria_cifras[['registros','registrosContinentales','registrosMarinos','registrosSalobres','especies','especiesMarinas','especiesContinentales','especiesSalobres','grupoBio','thematic','category']]
else:
    ##Seleccionar las columnas deseadas 
    taxon_categoria_cifras=taxon_categoria_cifras[['registros','especies','grupoBio','thematic','category']]


## Agrupar por grupo biológico y temática, eliminar la columna temática y reordenar el conjunto de datos   
taxon_categoria_cifras=taxon_categoria_cifras.groupby(['grupoBio','thematic','category']).sum().reset_index()
taxon_categoria_cifras=taxon_categoria_cifras.drop(['thematic'],axis=1).pivot('grupoBio','category').fillna(0) 



##Unión de las cifras de especies y registros para los dos grupos biológicos
grupos_biologicos_total=pd.merge(taxon_cifras,taxon_tematica_cifras,on='grupoBio',how='left').merge(taxon_categoria_cifras,on='grupoBio',how='left') ###FinalBiologicalGroupFile
grupos_biologicos_total=grupos_biologicos_total.replace(np.nan,'',regex=True)


'''
Esta sección esta comentariada hasta que se definan los grupos biologicos marinos y el oceano al que aplica cada uno
if tipo =='MCDM' or tipo =='DCDM':
    zm=registros['ZonaMaritima'].unique().dropna()
    for i in zm:
        if i !='Mar Caribe':
            grupos_biologicos_totalq=grupos_biologicos_total[grupos_biologicos_total["grupoBio"]=='Tortugas marinas'].index
            grupos_biologicos_total=grupos_biologicos_total.drop(grupos_biologicos_totalq)
        if i !='Oceano Pacífico':
            grupos_biologicos_totalq=grupos_biologicos_total[grupos_biologicos_total["grupoBio"]=='Tortugas marinas'].index
            grupos_biologicos_total=grupos_biologicos_total.drop(grupos_biologicos_totalq)
'''

##-----------------------Renombrar y reorganizar las columnas del dataframe de cifras por grupo biológico y geograficas-----------------##
'''
En esta sección se crea una función para renombrar y reorganizar los conjuntos de datos obtenidos para grupos biológicos y cifras geográficas.
Según la información deseada y los campos que se requieran se debe ajustar el diccionario 'names'.
El primera campo corresponde al nombre que tiene la columna en el conjunto de datos y el segundo al nombre que desea que tenga.}
Los nombres que no se encuentren dentro del diccionario pero estan en el conjunto de datos inical no quedaran en el archivo final.
Si se desea modificar el orden de los campos, solo se debe ajustar el orden dentro del diccionario. 
Esta función cuenta con dos parametros de entrada: 
archivo: corresponde al conjunto de datos grupo biológico:grupos_biologicos_total cifras geográficas: geografia_total
tipo: Corresponde a un parametro númerico donde 1: grupo biológico y 2: cifras geográficas. Este parametro permite a la función deteminar el valor
de la primera columna (grupoBio o (county o stateProvince))
'''

##Creación de la función con dos parametros de entrada

def ajuste_nombres (archivo, tipo):

    ## Para grupos biologicos: Si el valor de tipo es 1 asigna el valor de 'grupoBio' a las variables llave y valor
    if tipo== 1:
        llave='grupoBio'
        valor='grupoBio'

    ## Para cifras geograficas: Si el valor de tipo es 2 asigna el valor de 'county o stateProvince' a las variables llave y valor    
    if tipo==2:
        llave=geografia
        valor=geografia


    ##Diccionario de datos con los nombres provenientes del conjunto de datos y su correspondiente valor ajustado para exportar 
    names= {llave:valor, 
    'especies':'especies', 
    'especiesContinentales':'especies_continentales', 
    'especiesMarinas':'especies_marinas', 
    'especiesSalobres':'especies_salobres',     
    ('especies', 'threatStatus_MADS'):'especies_amenazadas_nacional', 
    ('especies', 'CR_MADS'):'especies_amenazadas_nacional_cr',
    ('especies', 'EN_MADS'):'especies_amenazadas_nacional_en', 
    ('especies', 'VU_MADS'):'especies_amenazadas_nacional_vu',
    ('especies', 'appendixCITES'):'especies_cites', 
    ('especies', 'I'):'especies_cites_i', 
    ('especies', 'I/II'):'especies_cites_i_ii',
    ('especies', 'II'):'especies_cites_ii', 
    ('especies', 'III'):'especies_cites_iii', 
    ('especies', 'endemic'):'especies_endemicas',  
    ('especies', 'exotic'):'especies_exoticas', 
    ('especies', 'Invasora_MADS'):'especies_invasoras', 
    #('especies', 'Invasora GRIIS'):'especies_invasoras_griis', 
    ('especies', 'Migratorio'):'especies_migratorias',
    ('especies', 'threatStatus_UICN'):'especies_amenazadas_global',  
    ('especies', 'EX_IUCN'):'especies_amenazadas_global_ex', 
    ('especies', 'EW_IUCN'):'especies_amenazadas_global_ew', 
    ('especies', 'CR_IUCN'):'especies_amenazadas_global_cr', 
    ('especies', 'EN_IUCN'):'especies_amenazadas_global_en', 
    ('especies', 'VU_IUCN'):'especies_amenazadas_global_vu',
    ('especies', 'NT_IUCN'):'especies_amenazadas_global_nt', 
    ('especies', 'LC_IUCN'):'especies_amenazadas_global_lc', 
    ('especies', 'DD_IUCN'):'especies_amenazadas_global_dd',
    #('especies', 'LR/lc_IUCN'):'especies_amenazadas_global_lr_lc', 
    #('especies', 'LR/nt_IUCN'):'especies_amenazadas_global_lr_nt',
    #('especies', 'Residente'):'especies_residente',  
    'registros':'registros', 
    'registrosContinentales':'registros_continentales', 
    'registrosMarinos':'registros_marinos', 
    'registrosSalobres':'registros_salobres',
    ('registros', 'threatStatus_MADS'):'registros_amenazadas_nacional', 
    ('registros', 'CR_MADS'):'registros_amenazadas_nacional_cr', 
    ('registros', 'EN_MADS'):'registros_amenazadas_nacional_en',
    ('registros', 'VU_MADS'):'registros_amenazadas_nacional_vu',  
    ('registros', 'appendixCITES'):'registros_cites', 
    ('registros', 'I'):'registros_cites_i', 
    ('registros', 'I/II'):'registros_cites_i_ii',
    ('registros', 'II'):'registros_cites_ii', 
    ('registros', 'III'):'registros_cites_iii', 
    ('registros', 'endemic'):'registros_endemicas', 
    ('registros', 'exotic'):'registros_exoticas', 
    ('registros', 'Invasora_MADS'):'registros_invasoras', 
    #('registros', 'Invasora GRIIS'):'registros_invasoras_griis',
    ('registros', 'Migratorio'):'registros_migratorias',  
    ('registros', 'threatStatus_UICN'):'registros_amenazadas_global',
    ('registros', 'EX_IUCN'):'registros_amenazadas_global_ex', 
    ('registros', 'EW_IUCN'):'registros_amenazadas_global_ew', 
    ('registros', 'CR_IUCN'):'registros_amenazadas_global_cr', 
    ('registros', 'EN_IUCN'):'registros_amenazadas_global_en', 
    ('registros', 'VU_IUCN'):'registros_amenazadas_global_vu', 
    ('registros', 'NT_IUCN'):'registros_amenazadas_global_nt', 
    ('registros', 'LC_IUCN'):'registros_amenazadas_global_lc',
    ('registros', 'DD_IUCN'):'registros_amenazadas_global_dd',    
    #('registros', 'LR/lc_IUCN'):'registros_amenazadas_global_lr_lc', 
    #('registros', 'LR/nt_IUCN'):'registros_amenazadas_global_lr_nt', 
    #('registros', 'Residente'):'registros_residente', 
    #('registros', 'isInvasive_griis'):'registros_is:invasive_griis', 
    #('registros', 'isInvasive_mads'):'registros_is_invasive_mads', 
    #('registros', 'migratory'):'registros_migratorios', 
    #('especies', 'isInvasive_griis'):'especies_is_invasive_griis', 
    #('especies', 'isInvasive_mads'):'especies_is_invasive_mads', 
    #('especies', 'migratory'):'especies_migratorias', 
    #('registros', 'Endémica'):'registros_endemicas2', 
    #('registros', 'Errático'):'registros_erraticos',
    #('registros', 'Exótica'):'registros_exoticas2', 
    #('especies', 'Endémica'):'especiesEndemicas2', 
    #('especies', 'Errático'):'especiesErrático',
    #('especies', 'Exótica'):'especiesExoticas2', 
    
    ('especiesContinentales', 'threatStatus_MADS'):'especie_continentales_amenazadas_nacional', 
    ('especiesContinentales', 'CR_MADS'):'especies_continentales_amenazadas_nacional_cr',
    ('especiesContinentales', 'EN_MADS'):'especies_continentales_amenazadas_nacional_en', 
    ('especiesContinentales', 'VU_MADS'):'especies_continentales_amenazadas_nacional_vu',
    ('especiesContinentales', 'appendixCITES'):'especies_continentales_cites', 
    ('especiesContinentales', 'I'):'especies_continentales_cites_i', 
    ('especiesContinentales', 'I/II'):'especies_continentales_cites_i_ii', #Como es el campo
    ('especiesContinentales', 'II'):'especies_continentales_cites_ii', 
    ('especiesContinentales', 'III'):'especies_continentales_cites_iii', 
    ('especiesContinentales', 'endemic'):'especies_continentales_endemicas', 
    ('especiesContinentales', 'exotic'):'especies_continentales_exoticas', 
    ('especiesContinentales', 'Migratorio'):'especies_continentales_migratorias', 
    ('especiesContinentales', 'Invasora_MADS'):'especies_continentales_invasora', 
    #('especiesContinentales', 'Invasora GRIIS'):'especies_continentales_invasora_griis', 
    ('especiesContinentales', 'threatStatus_UICN'):'especies_continentales_amenazadas_global', 
    ('especiesContinentales', 'EX_IUCN'):'especies_continentales_amenazadas_global_ex', 
    ('especiesContinentales', 'EW_IUCN'):'especies_continentales_amenazadas_global_ew', 
    ('especiesContinentales', 'CR_IUCN'):'especies_continentales_amenazadas_global_cr', 
    ('especiesContinentales', 'EN_IUCN'):'especies_continentales_amenazadas_global_en', 
    ('especiesContinentales', 'VU_IUCN'):'especies_continentales_amenazadas_global_vu',
    ('especiesContinentales', 'NT_IUCN'):'especies_continentales_amenazadas_global_nt', 
    ('especiesContinentales', 'LC_IUCN'):'especies_continentales_amenazadas_global_lc', 
    ('especiesContinentales', 'DD_IUCN'):'especies_continentales_amenazadas_global_dd',
    #('especiesContinentales', 'LR/lc_IUCN'):'especies_continentales_amenazadas_global_lr_lc', 
    #('especiesContinentales', 'LR/nt_IUCN'):'especies_continentales_amenazadas_global_lr_nt', 
    #('especiesContinentales', 'Residente'):'especies_continentales_residentes',
    ('registrosContinentales', 'threatStatus_MADS'):'registros_continentales_amenazadas_nacional', 
    ('registrosContinentales', 'CR_MADS'):'registros_continentales_amenazadas_nacional_cr', 
    ('registrosContinentales', 'EN_MADS'):'registros_continentales_amenazadas_nacional_en',
    ('registrosContinentales', 'VU_MADS'):'registros_continentales_amenazadas_nacional_vu', 
    ('registrosContinentales', 'appendixCITES'):'registros_continentales_cites', 
    ('registrosContinentales', 'I'):'registros_continentales_cites_i', 
    ('registrosContinentales', 'I/II'):'registros_continentales_cites_i_ii', #Como es el campo
    ('registrosContinentales', 'II'):'registros_continentales_cites_ii', 
    ('registrosContinentales', 'III'):'registros_continentales_cites_iii', 
    ('registrosContinentales', 'endemic'):'registros_continentales_endemicas', 
    ('registrosContinentales', 'exotic'):'registros_continentales_exoticas', 
    ('registrosContinentales', 'Migratorio'):'registros_continentales_migratorias', 
    ('registrosContinentales', 'Invasora_MADS'):'registros_continentales_invasora', 
    #('registrosContinentales', 'Invasora GRIIS'):'registros_continentales_invasora_griis', 
    ('registrosContinentales', 'threatStatus_UICN'):'registros_continentales_amenazadas_global', 
    ('registrosContinentales', 'EX_IUCN'):'registros_continentales_amenazadas_global_ex', 
    ('registrosContinentales', 'EW_IUCN'):'registros_continentales_amenazadas_global_ew', 
    ('registrosContinentales', 'CR_IUCN'):'registros_continentales_amenazadas_global_cr', 
    ('registrosContinentales', 'EN_IUCN'):'registros_continentales_amenazadas_global_en', 
    ('registrosContinentales', 'VU_IUCN'):'registros_continentales_amenazadas_global_vu', 
    ('registrosContinentales', 'NT_IUCN'):'registros_continentales_amenazadas_global_nt', 
    ('registrosContinentales', 'LC_IUCN'):'registros_continentales_amenazadas_global_lc', 
    ('registrosContinentales', 'DD_IUCN'):'registros_continentales_amenazadas_global_dd',
    #('registrosContinentales', 'LR/lc_IUCN'):'registros_continentales_amenazadas_global_lr_lc', 
    #('registrosContinentales', 'LR/nt_IUCN'):'registros_continentales_amenazadas_global_lr_nt', 
    #('registrosContinentales', 'Residente'):'registros_continentales_residente', 
    #('registrosContinentales', 'isInvasive_griis'):'registros_continentales_is_invasive_griis', 
    #('registrosContinentales', 'isInvasive_mads'):'registros_continentales_is_invasive_mads', 
    #('registrosContinentales', 'migratory'):'registros_continentales_migratorias', 
    #('especiesContinentales', 'isInvasive_griis'):'especies_continentales_is_invasive_griis', 
    #('especiesContinentales', 'isInvasive_mads'):'especies_continentales_is_invasive_mads', 
    #('especiesContinentales', 'migratory'):'especies_continentales_migratorias', 
    #('registrosContinentales', 'Endémica'):'registros_continentales_endemicas2', 
    #('registrosContinentales', 'Errático'):'registros_continentales_erratico',
    #('registrosContinentales', 'Exótica'):'registros_continentales_exoticas2', 
    #('especiesContinentales', 'Endémica'):'especies_continentales_endemicas2', 
    #('especiesContinentales', 'Errático'):'especies_continentales_erratico', #Como es el campo
    #('especiesContinentales', 'Exótica'):'especies_continentales_exoticas2', 
    
    ('especiesMarinas', 'threatStatus_MADS'):'especies_marinas_amenazadas_nacional', 
    ('especiesMarinas', 'EN_MADS'):'especies_marinas_amenazadas_nacional_en', 
    ('especiesMarinas', 'CR_MADS'):'especies_marinas_amenazadas_nacional_cr',
    ('especiesMarinas', 'VU_MADS'):'especies_marinas_amenazadas_nacional_vu',
    ('especiesMarinas', 'appendixCITES'):'especies_marinas_cites', 
    ('especiesMarinas', 'I'):'especies_marinas_cites_i', 
    ('especiesMarinas', 'I/II'):'especies_marinas_cites_i_ii', #Como es el campo
    ('especiesMarinas', 'II'):'especies_marinas_cites_ii', 
    ('especiesMarinas', 'III'):'especies_marinas_cites_iii', 
    ('especiesMarinas', 'endemic'):'especies_marinas_endemicas', 
    ('especiesMarinas', 'exotic'):'especies_marinas_exoticas', 
    ('especiesMarinas', 'Migratorio'):'especies_marinas_migratorias', 
    ('especiesMarinas', 'Invasora_MADS'):'especies_marinas_invasora', 
    #('especiesMarinas', 'Invasora GRIIS'):'especies_marinas_invasora_griis', 
    ('especiesMarinas', 'threatStatus_UICN'):'especies_marinas_amenazadas_global', 
    ('especiesMarinas', 'EX_IUCN'):'especies_marinas_amenazadas_global_ex', 
    ('especiesMarinas', 'EW_IUCN'):'especies_marinas_amenazadas_global_ew', 
    ('especiesMarinas', 'CR_IUCN'):'especies_marinas_amenazadas_global_cr', 
    ('especiesMarinas', 'EN_IUCN'):'especies_marinas_amenazadas_global_en', 
    ('especiesMarinas', 'VU_IUCN'):'especies_marinas_amenazadas_global_vu',
    ('especiesMarinas', 'NT_IUCN'):'especies_marinas_amenazadas_global_nt', 
    ('especiesMarinas', 'LC_IUCN'):'especies_marinas_amenazadas_global_lc', 
    ('especiesMarinas', 'DD_IUCN'):'especies_marinas_amenazadas_global_dd',
    #('especiesMarinas', 'LR/lc_IUCN'):'especies_marinas_amenazadas_global_lr_lc', 
    #('especiesMarinas', 'LR/nt_IUCN'):'especies_marinas_amenazadas_global_lr_nt', 
    #('especiesMarinas', 'Residente'):'especies_marinas_residente', 
    ('registrosMarinos', 'threatStatus_MADS'):'registros_marinas_amenazadas_nacional', 
    ('registrosMarinos', 'CR_MADS'):'registros_marinas_amenazadas_nacional_cr', 
    ('registrosMarinos', 'EN_MADS'):'registros_marinas_amenazadas_nacional_en',
    ('registrosMarinos', 'VU_MADS'):'registros_marinas_amenazadas_nacional_vu',  
    ('registrosMarinos', 'appendixCITES'):'registros_marinas_cites', 
    ('registrosMarinos', 'I'):'registros_marinas_cites_i', 
    ('registrosMarinos', 'I/II'):'registros_marinas_cites_i_ii', #Como es el campo
    ('registrosMarinos', 'II'):'registros_marinas_cites_ii', 
    ('registrosMarinos', 'III'):'registros_marinas_cites_iii', 
    ('registrosMarinos', 'endemic'):'registros_marinas_endemicas', 
    ('registrosMarinos', 'exotic'):'registros_marinas_exoticas', 
    ('registrosMarinos', 'Migratorio'):'registros_marinas_migratorias', 
    ('registrosMarinos', 'Invasora_MADS'):'registros_marinas_invasora', 
    #('registrosMarinos', 'Invasora GRIIS'):'registros_marinas_invasora_griis',
    ('registrosMarinos', 'threatStatus_UICN'):'registros_marinas_amenazadas_global', 
    ('registrosMarinos', 'EX_IUCN'):'registros_marinas_amenazadas_global_ex', 
    ('registrosMarinos', 'EW_IUCN'):'registros_marinas_amenazadas_global_ew', 
    ('registrosMarinos', 'CR_IUCN'):'registros_marinas_amenazadas_global_cr', 
    ('registrosMarinos', 'EN_IUCN'):'registros_marinas_amenazadas_global_en', 
    ('registrosMarinos', 'VU_IUCN'):'registros_marinas_amenazadas_global_vu', 
    ('registrosMarinos', 'NT_IUCN'):'registros_marinas_amenazadas_global_nt', 
    ('registrosMarinos', 'LC_IUCN'):'registros_marinas_amenazadas_global_lc', 
    ('registrosMarinos', 'DD_IUCN'):'registros_marinas_amenazadas_global_dd',
    #('registrosMarinos', 'LR/lc_IUCN'):'registros_marinas_amenazadas_global_lr_lc', 
    #('registrosMarinos', 'LR/nt_IUCN'):'registros_marinas_amenazadas_global_lr_nt', 
    #('registrosMarinos', 'Residente'):'registros_marinas_residente',
    #('registrosMarinos', 'isInvasive_griis'):'registros_marinas_is_invasive_griis', 
    #('registrosMarinos', 'isInvasive_mads'):'registros_marinas_is_invasive_mads', 
    #('registrosMarinos', 'migratory'):'registros_marinas_migratorio', 
    #('especiesMarinas', 'isInvasive_griis'):'especies_marinas_is_invasive_griis', 
    #('especiesMarinas', 'isInvasive_mads'):'especies_marinas_is_invasive_mads', 
    #('especiesMarinas', 'migratory'):'especies_marinas_migratorias', 
    #('registrosMarinos', 'Endémica'):'registros_marinas_endemicas2', 
    #('registrosMarinos', 'Errático'):'registros_marinas_erratico',
    #('registrosMarinos', 'Exótica'):'registros_marinas_exoticas2', 
    #('especiesMarinas', 'Endémica'):'especies_marinas_endemicas2', 
    #('especiesMarinas', 'Errático'):'especies_marinas_erratico',
    #('especiesMarinas', 'Exótica'):'especies_marinas_exoticas2', 
    
    ('especiesSalobres', 'threatStatus_MADS'):'especies_salobres_amenazadas_nacional', 
    ('especiesSalobres', 'CR_MADS'):'especies_salobres_amenazadas_nacional_cr',
    ('especiesSalobres', 'EN_MADS'):'especies_salobres_amenazadas_nacional_en', 
    ('especiesSalobres', 'VU_MADS'):'especies_salobres_amenazadas_nacional_vu',
    ('especiesSalobres', 'appendixCITES'):'especies_salobres_cites', 
    ('especiesSalobres', 'I'):'especies_salobres_cites_i', 
    ('especiesSalobres', 'I/II'):'especies_salobres_cites_i_ii', #Como es el campo
    ('especiesSalobres', 'II'):'especies_salobres_cites_ii', 
    ('especiesSalobres', 'III'):'especies_salobres_cites_iii', 
    ('especiesSalobres', 'endemic'):'especies_salobres_endemicas', 
    ('especiesSalobres', 'exotic'):'especies_salobres_exoticas', 
    ('especiesSalobres', 'Migratorio'):'especies_salobres_migratorias', 
    ('especiesSalobres', 'Invasora_MADS'):'especies_salobres_invasora',
    #('especiesSalobres', 'Invasora GRIIS'):'especies_salobres_invasora_griis', 
    ('especiesSalobres', 'threatStatus_UICN'):'especies_salobres_amenazadas_global', 
    ('especiesSalobres', 'EX_IUCN'):'especies_salobres_amenazadas_global_ex', 
    ('especiesSalobres', 'EW_IUCN'):'especies_salobres_amenazadas_global_ew', 
    ('especiesSalobres', 'CR_IUCN'):'especies_salobres_amenazadas_global_cr', 
    ('especiesSalobres', 'EN_IUCN'):'especies_salobres_amenazadas_global_en', 
    ('especiesSalobres', 'VU_IUCN'):'especies_salobres_amenazadas_global_vu',
    ('especiesSalobres', 'NT_IUCN'):'especies_salobres_amenazadas_global_nt', 
    ('especiesSalobres', 'LC_IUCN'):'especies_salobres_amenazadas_global_lc',
    ('especiesSalobres', 'DD_IUCN'):'especies_salobres_amenazadas_global_dd',
    #('especiesSalobres', 'LR/lc_IUCN'):'especies_salobres_amenazadas_global_lr_lc', 
    #('especiesSalobres', 'LR/nt_IUCN'):'especies_salobres_amenazadas_global_lr_nt',
    #('especiesSalobres', 'Residente'):'especies_salobres_residente',
    
    ('registrosSalobres', 'threatStatus_MADS'):'registros_salobres_amenazadas_nacional', 
    ('registrosSalobres', 'CR_MADS'):'registros_salobres_amenazadas_nacional_cr', 
    ('registrosSalobres', 'EN_MADS'):'registros_salobres_amenazadas_nacional_en',
    ('registrosSalobres', 'VU_MADS'):'registros_salobres_amenazadas_nacional_vu',  
    ('registrosSalobres', 'appendixCITES'):'registros_salobres_cites', 
    ('registrosSalobres', 'I'):'registros_salobres_cites_i', 
    ('registrosSalobres', 'I/II'):'registros_salobres_cites_i_ii', #Como es el campo
    ('registrosSalobres', 'II'):'registros_salobres_cites_ii', 
    ('registrosSalobres', 'III'):'registros_salobres_cites_iii', 
    ('registrosSalobres', 'endemic'):'registros_salobres_endemicas', 
    ('registrosSalobres', 'exotic'):'registros_salobres_exoticas', 
    ('registrosSalobres', 'Migratorio'):'registros_salobres_migratorias', 
    ('registrosSalobres', 'Invasora_MADS'):'registros_salobres_invasora', 
    #('registrosSalobres', 'Invasora GRIIS'):'registros_salobres_invasora_griis',
    ('registrosSalobres', 'threatStatus_UICN'):'registros_salobres_amenazadas_global', 
    ('registrosSalobres', 'EX_IUCN'):'registros_salobres_amenazadas_global_ex', 
    ('registrosSalobres', 'EW_IUCN'):'registros_salobres_amenazadas_global_ew', 
    ('registrosSalobres', 'CR_IUCN'):'registros_salobres_amenazadas_global_cr', 
    ('registrosSalobres', 'EN_IUCN'):'registros_salobres_amenazadas_global_en', 
    ('registrosSalobres', 'VU_IUCN'):'registros_salobres_amenazadas_global_vu', 
    ('registrosSalobres', 'NT_IUCN'):'registros_salobres_amenazadas_global_nt', 
    ('registrosSalobres', 'LC_IUCN'):'registros_salobres_amenazadas_global_lc', 
    ('registrosSalobres', 'DD_IUCN'):'registros_salobres_amenazadas_global_dd',
    #('registrosSalobres', 'LR/lc_IUCN'):'registros_salobres_amenazadas_global_lr_lc', 
    #('registrosSalobres', 'LR/nt_IUCN'):'registros_salobres_amenazadas_global_lr_nt', 
    #('registrosSalobres', 'Residente'):'registros_salobres_residente', 
    #('registros_salobres', 'isInvasive_griis'):'registros_salobres_is_invasive_griis', 
    #('registros_salobres', 'isInvasive_mads'):'registros_salobres_is_invasive_mads', 
    #('registros_salobres', 'migratory'):'registros_salobres_migratorios', 
    #('especies_salobres', 'isInvasive_griis'):'especies_salobres_is_invasive_griis', 
    #('especies_salobres', 'isInvasive_mads'):'especies_salobres_is_invasive_mads', 
    #('especies_salobres', 'migratory'):'especies_salobres_migratorias', 
    #('registrosBrackish', 'Endémica'):'registros_salobres_endemicas2', 
    #('registrosBrackish', 'Errático'):'registros_salobres_erratico',
    #('registros_salobres', 'Exótica'):'registros_salobres_exoticas2', 

    #('especies_salobres', 'Endémica'):'especies_salobres_endemicas2', 
    #('especies_salobres', 'Errático'):'especies_salobres_erratico', 
    #('especies_salobres', 'Exótica'):'especies_salobres_exoticas2', 
    }

    ##Número de columnas del conjunto de datos inicial
    columns_num = len(archivo.columns)

    ##Crear los diccionarios vacios
    names_tot,names_list,names_final=[],[],[]

    i=0

    ##Hacer un ciclo que se repita el número de columnas que tenga el conjunto de datos inicial
    while i < columns_num:

        ##Recorrer el diccionario con los nombres
        for name in names:

            ##Comparar el nombre de la columna en la posición i con los valores del listado de nombres
            if archivo.columns.values[i] == name:
                
                ##Reasignar el nombre
                archivo.columns.values[i]=names[name]

                ##Crear una lista con los nombres nuevos de las columnas que se encuentran en el archivo
                names_tot.append(names[name])   
            
            ##Crear una lista con solo los nombres definitvos de la lista de nombres
            names_list.append(names[name])  
        i+=1

    ##Recorrer el listado de nombres names_tot de la lista de nombres
    for i in names_list:

        ## Recorrer la lista con los nombres nuevos de las columnas que se encuentran en el archivo
        for j in names_tot:

            ## Si son iguales agregar el nombre a la lista
            if i in j:
                names_final.append(i)

    ##Ordenar las columnas de la tabla con la lista final
    archivo=archivo.reset_index()
    archivo=archivo.reindex(columns=pd.unique(names_final))

    ##Quitar .0 al final de las cifras
    archivo=archivo.astype(str)
    archivo=archivo.replace(to_replace='\.0+$',value="",regex=True)

    #Cifras finales por grupo biológico y temática
    ##Para grupos biologicos
    if tipo== 1:
        archivo.to_csv(nombre+'cifrasGruposBiologicos.tsv',sep='\t', index=False )
        archivo.to_excel(nombre+'cifrasGruposBiologicos.xlsx', sheet_name='cifrasGruposBiologicos', index=False )

    ##Para cifras geograficas
    if tipo==2:
        archivo.to_csv(nombre+'cifrasGeográficas.tsv',sep='\t', index=False )
        archivo.to_excel(nombre+'cifrasGeográficas.xlsx', sheet_name='cifrasGeográficas', index=False )

##-------------------------------------------------Ejecución de la función ajuste_nombres ------------------------------------------------##
##Se llama la función ingresando los dos parametros requeridos
#Para grupos biologicos

ajuste_nombres(grupos_biologicos_total, 1)

#Para cifras geográficas
ajuste_nombres(geografia_total, 2)

fin=time.time()    
print(fin-inicio)
