# -*- coding: utf-8 -*-
"""
Created on Tue Aug 23 09:11:04 2022

@author: camila.plata
@editor: Nerieth Leuro
"""

##-----------------------------------------------------Preparar el entorno---------------------------------------------##
##Importar las librerias necesarias para el uso del script
import pandas as pd 
import os 
import time
import numpy as np


##Iniciar el conteo de tiempo de ejecución del script
inicio=time.time()
print(inicio)

##Mostrar la carpeta principal donde se encuentra alojado el script
print(os.getcwd())

##Establecer la carpeta de trabajo
##Tenga en cuenta que en el momento de diligenciar la ubicación de la carpeta de trabajo se deben documentar doble barra invertida "\\" para evitar errores de sintaxis
os.chdir("D:\\Cifras_RNLP\\2022T2\\Reserva")

##Crear una variable string con el nombre del conjunto de datos. Este nombre debe incluir: la geografía, el año y el corte trimestral utilizado 
nombre='RNLP2022T2_'
fecha_corte='2022-07-13'

##----------------------------------------------------Cargar archivos--------------------------------------------------##
'''
Para conjuntos de datos para obtener cifras municipales y equipos con poca memoria RAM (8 gb o menos)
Se agrega dtype (asignar el tipo de dato a cada columna)
Con el fin de evitar el error: pandas.errors.ParserError: Error tokenizing data. C error: out of memory
En el caso de conjuntos de datos para cifras departamentales realice la ejecución en un equipo con memoria RAM de 16 gb

dtype: se especifica cada columna y el tipo de dato que corresponde, el orden de los campos no afecta la ejecución, pero debe actualizarse
cada que se agreguen y quiten campos para hacer más eficiente el proceso.
    str: Se utiliza para campos que contengan texto y símbolos
    float: Aplica en campos que contengan números decimales
Adicionalmente, se especifican las columnas a cargar con el fin de dejar solo las columnas necesarias
para la obtención de cifras y no sobrecargar la memoria RAM y hacer el proceso más eficiente.
'''

##Crear un objeto cargando la tabla de datos(formato tsv o txt)
  
registros = pd.read_table('RRBB_Reserva2022T2_final.tsv', sep='\t', encoding = "utf8",dtype=str)    


##Reemplaza los campos vacios por NaN, esto para evitar errores de conversión de datos al momento de
## hacer los calculos
registros = registros.replace(r'^\s*$', np.nan, regex=True)

## Cargar el archivo de grupos biologicos que relacionan la taxonomía con los grupos biológicos
grupos_biologicos = pd.read_table('grupos_biologicos\\gruposBiologicosCifrasSiB-v20220705.tsv', sep='\t', encoding = "utf8")
grupos_biologicos=grupos_biologicos.loc[grupos_biologicos.loc[:, 'tipo_grupo'] != '-']
##Archivo del último reporte mensual
entidades_reporte= pd.read_table('reporteMensual\\datasetCO-2022-09-01.tsv', sep='\t', encoding = "utf8",usecols=['organization','Logo','URLSocio','typeOrg'])

##Cargar archivos de referencia geográfica 
##Con la información de departamentos
staProv_divipola = pd.read_table('divipola\\departamento.tsv', encoding = "utf8")

estimadas_dept = pd.read_table('divipola\\estimadas_departamentos.csv', encoding = "utf8", sep=';')

##Con la información de municipios
staProv_divipola_m = pd.read_table('divipola\\region.tsv', encoding = "utf8",usecols=['parent','slug','label'])

##----------------------------------------------------Tipo de ejecución--------------------------------------------------##
'''
La variable 'tipo' permite condicionar los procesos teniendo en cuenta si se van a sacar cifras departamentales o municipales y si 
el conjunto de datos contiene información de registros marítimos
Departamental con datos marinos='DCDM'
Departamental sin datos marinos='DSDM'
Municipal con datos marinos='MCDM'
Municipal sin datos marinos='MSDM'
Otros='OT'
Al seleccionar alguna de las opciones sin datos marinos, las cifras se calculan en forma general, sin discriminar por los hábitat marino, 
terrestre y salobre

Para las opciones con datos marinos, se realiza el cálculo de cifras general y adicionalmente el cálculo para los hábitat marino, terrestre 
y salobre. Es importante aclarar que debido a los hábitos y distribución de las especies se pueden encontrar especies presentes en más de 
un hábitat por lo tanto, el valor general no corresponde a la suma de los valores para cada hábitat.

'''
#tipo='DCDM'
#tipo='DSDM'
#tipo='MCDM'
#tipo='MSDM'
#tipo='CCDM'
tipo='CSDM'

#departamento='Putumayo'
##--------------------------------------------Ajustes preliminares de los datos ---------------------------------------------##
'''
Cuando se realiza la ejecución de este script sin realizar un proceso de limpieza en el campo de stateProvince se debe realizar el cruce con el
archivo de referencia geográfica DIVIPOLA. Lo cual permitirá asegurar que en este campo solo se encuentren los departamentos válidos para el
país.
'''
if tipo=='DCDM' or tipo=='DSDM' or tipo=='CCDM' or tipo=='CSDM':
    
     ##Reajustar campo stateProvince (asignar al campo verbatimStateProvince el valor de stateProvince)
    registros['verbatimStateProvince']=registros['stateProvince']
    
    ##Quitar comillas
    registros['stateProvince']=registros.stateProvince.str.replace('"','')
    
    ## Unir las tablas por medio del campo stateProvince
    registros=pd.merge(registros,staProv_divipola, left_on='stateProvince', right_on='label',how='left') 
    
    #Reemplazar los valores null de stateProvinceDivipola por los valores de Departamento-ubicacionCoordenada
    registros['stateProvince']=registros['label'].mask(pd.isnull, registros['Departamento-ubicacionCoordenada'])

        ##Si se especifica el departamento se habilitan estas tres lineas de lo contrario se pueden mantener comentadas
    #registros_descartados=registros.loc[registros.loc[:, 'stateProvince'] != departamento]
    #registros_descartados.to_csv(nombre+'registros_descartados.tsv',sep='\t', index=False )
    
    #registros=registros.loc[registros.loc[:, 'stateProvince'] == departamento]    
if tipo=='DCDM' or tipo=='DSDM':    
    region='slug'

if tipo=='CCDM' or tipo=='CSDM':
    registros['slug_col']='colombia'
    region='slug_col'  
    
    
if tipo=='MCDM' or tipo=='MSDM':
    ##Reajustar campo stateProvince (asignar al campo verbatimStateProvince el valor de stateProvince)
    registros['verbatimStateProvince']=registros['stateProvince']
    
    ##Quitar comillas
    registros['stateProvince']=registros.stateProvince.str.replace('"','')
    
    ## Unir las tablas por medio del campo stateProvince
    registros=pd.merge(registros,staProv_divipola, left_on='stateProvince', right_on='label',how='left') 
    
    #Reemplazar los valores null de stateProvinceDivipola por los valores de Departamento-ubicacionCoordenada
    registros['stateProvince']=registros['label'].mask(pd.isnull, registros['Departamento-ubicacionCoordenada'])
        
    registros=pd.merge(registros,staProv_divipola_m, left_on=['slug','county'], right_on=['parent','label'],how='left') 
    registros['county']=registros['label_y'].mask(pd.isnull, registros['Municipio-ubicacionCoordenada'])
        
    region='slug_y'
    ##Si se especifica el departamento se habilitan estas tres lineas de lo contrario se pueden mantener comentadas
    #registros_descartados=registros.loc[registros.loc[:, 'stateProvince'] != departamento]
    #registros_descartados.to_csv(nombre+'registros_descartados.tsv',sep='\t', index=False )
    
    #registros=registros.loc[registros.loc[:, 'stateProvince'] == departamento]

##Condicional para registros marinos, continentales y salobres
##Este permite eliminar la categoría de marino para registros que no pertenecen a municipios con zonas marinas
if tipo =='DCDM':   
    dept_marinos=staProv_divipola[staProv_divipola['marino']=='FALSE']    
    registros.loc[(registros['stateProvince'].isin(dept_marinos['label'])), 'isMarine'] = np.nan

if tipo =='MCDM':   
    county_divipola=pd.read_table('divipola\\municipio.tsv', encoding = "utf8")
    mun_marinos=county_divipola[county_divipola['marino']=='FALSE']    
    registros.loc[(registros['county'].isin(mun_marinos['label'])), 'isMarine'] = np.nan
  


##Ajuste de las categorías threathStatus para asegurar integridad de las cifras MADS e IUCN en los conteos
registros.threatStatus_UICN=registros.threatStatus_UICN + '_IUCN'
registros.threatStatus_MADS=registros.threatStatus_MADS + '_MADS'


#registros.to_csv('registros_Putumayo2022T2.tsv',sep='\t', index=False )
##---------------------------------------------Cifras generales organizaciones publicadoras---------------------------------##
'''
Se obtienen las cifras de especies y registros para todas las organizaciones publicadoras que se encuentran representadas en 
el conjunto de datos

'''
##Quitar el texto: "País publicador:" del campo 'publishingCountry'
registros=registros.replace('País publicador: ','',regex=True)

##Crear dataframes vacíos para almacenar las cifras de entidades publicadoras
entidades_registros=pd.DataFrame()
entidades_total=pd.DataFrame()

##Crear dataframe con los nombres, país y logo de las organizaciones publicadoras
entidades =pd.DataFrame()
entidades['publishingOrgKey'],entidades['organization'],entidades['publishingCountry'],entidades['logoUrl']=registros['publishingOrgKey'],registros['organization'],registros['publishingCountry'],registros['logoUrl']
##Condicional para registros marinos, continentales y salobres
    
if tipo =='CSDM' or tipo =='MSDM' or tipo =='DSDM': 
    ## Cifras registros por organización publicadora: Crear un dataframe con los datos agrupar por organización y contar los registros generales
    entidades_registros['registros']= registros.groupby([region,'organization'])['gbifID'].count()
    
    ##Crear conjunto de datos con especies y entidad publicadora general
    ##Contar número de especies para entidad publicadora general
    entidades_sp= registros.groupby([region,'organization','species'])['species'].count().to_frame(name = 'especies').reset_index()
    entidades_sp= entidades_sp.groupby([region,'organization'])['especies'].count().to_frame(name = 'especies').reset_index() 

    ##Unir los conteos de registros y especies por entidad general
    entidades_total=pd.merge(entidades_registros,entidades_sp, on=[region,'organization'],how='left').sort_values('organization').fillna('-')
    
    ##Agregar la información del logo y seleccionar las columnas finales
    entidades_total=pd.merge(entidades_total,entidades, on=['organization'],how='left')
    aggregation_functions = {'publishingOrgKey': 'first', 'publishingCountry': 'first', 'logoUrl':'first','registros':'first','especies': 'first'}
    entidades_total = entidades_total.groupby(['organization',region]).aggregate(aggregation_functions).reset_index().fillna('-')

    ##completar con la información del reporte mensual
    entidades_total=pd.merge(entidades_total,entidades_reporte, on=['organization'],how='left')
    ##Asignar la URL del logo proveniente del reporte
    entidades_total.loc[(entidades_total.logoUrl!=entidades_total.Logo) & (entidades_total.Logo!=np.nan),'logoUrl']=entidades_total.Logo
    entidades_total=entidades_total.drop(['Logo'], axis='columns')
    aggregation_functions = {'publishingOrgKey': 'first', 'publishingCountry': 'first', 'logoUrl':'first', 'typeOrg':'first','registros':'first','especies': 'first'}
    entidades_total = entidades_total.groupby(['organization',region]).aggregate(aggregation_functions).reset_index().fillna('-')


if tipo =='MCDM' or tipo =='DCDM' or tipo =='CCDM':
    ## Cifras registros por organización publicadora: Crear un dataframe con los datos, agrupar por organización y contar los registros para cada una de las categorías (marinos, salobres y continentales)
    ##Crear conjunto de datos con registros y organización para datos marinos, salobres y continentales
    entidades_registros['registros'],entidades_registros['registrosMarinos'],entidades_registros['registrosSalobres'],entidades_registros['registrosContinentales']= registros.groupby(['organization',region])['gbifID'].count(),registros.groupby(['organization',region])['isMarine'].count(),registros.groupby(['organization',region])['isBrackish'].count(),registros.groupby(['organization',region])['isTerrestrial'].count()
    
    ## Cifras especies por organización publicadora: Crear un dataframe con los datos, agrupar por organización y especie, contar los registros por species para cada una de las categorías (marinos, salobres y continentales)
    entidades_sp= registros.groupby(['organization','species',region])['species'].count().to_frame(name = 'especies').reset_index()
    entidades_sp= entidades_sp.groupby(['organization',region])['especies'].count().to_frame(name = 'especies').reset_index() 
    
    ##Crear conjunto de datos con especies y entidad publicadora para datos marinos
    ##Contar número de especies marinos para entidad publicadora 
    ent_sp_marino= registros.groupby(['organization','species','isMarine',region])['species'].count().to_frame(name = 'especies').reset_index()
    ent_sp_marino= ent_sp_marino.groupby(['organization',region])['especies'].count().to_frame(name = 'especiesMarinas').reset_index() 

    ##Crear conjunto de datos con especies y entidad publicadora para datos salobres
    ##Contar número de especies salobres para entidad publicadora 
    ent_sp_salobre= registros.groupby(['organization','species','isBrackish',region])['species'].count().to_frame(name = 'especies').reset_index()
    ent_sp_salobre= ent_sp_salobre.groupby(['organization',region])['especies'].count().to_frame(name = 'especiesSalobres').reset_index() 

    ##Crear conjunto de datos con especies y entidad publicadora para datos continentales
    ##Contar número de especies continentales para entidad publicadora
    ent_sp_continental= registros.groupby(['organization','species','isTerrestrial',region])['species'].count().to_frame(name = 'especies').reset_index() 
    ent_sp_continental= ent_sp_continental.groupby(['organization',region])['especies'].count().to_frame(name = 'especiesContinentales').reset_index() 
 
    ##Unir todos los conjuntos de datos, Reemplazar los valores NaN y ordenar las entidades geográficas
    entidades_total=registros.groupby(['organization',region])['organization'].count().to_frame(name = 'especies').reset_index().drop('especies', axis=1)  
    entidades_total=pd.merge(entidades_total,entidades_sp, on=['organization',region],how='left').merge(ent_sp_salobre, on=['organization',region],how='left').merge(ent_sp_marino, on=['organization',region],how='left').merge(ent_sp_continental, on=['organization',region],how='left').merge(entidades_registros, on=['organization',region],how='left').sort_values('organization').fillna('-')
    
    ##Agregar la información del logo y seleccionar las columnas finales
    entidades_total=pd.merge(entidades_total,entidades, on=['organization'], how='left')      
    aggregation_functions = {'publishingOrgKey': 'first', 'publishingCountry': 'first', 'logoUrl':'first','registros':'first','registrosContinentales':'first','registrosSalobres':'first','registrosMarinos':'first','especies': 'first','especiesSalobres': 'first','especiesMarinas': 'first','especiesContinentales': 'first'}
    entidades_total = entidades_total.groupby(['organization',region]).aggregate(aggregation_functions).reset_index().fillna('-')

    ##completar con la información del reporte mensual
    entidades_total=pd.merge(entidades_total,entidades_reporte, on=['organization'],how='left')
    ##Asignar la URL del logo proveniente del reporte
    entidades_total.loc[(entidades_total.logoUrl!=entidades_total.Logo) & (entidades_total.Logo!=np.nan),'logoUrl']=entidades_total.Logo
    entidades_total=entidades_total.drop(['Logo'], axis='columns')
    aggregation_functions = {'publishingOrgKey': 'first', 'publishingCountry': 'first', 'logoUrl':'first', 'typeOrg':'first','registros':'first','registrosSalobres':'first','registrosMarinos':'first','registrosContinentales':'first','especies': 'first','especiesSalobres': 'first','especiesMarinas': 'first','especiesContinentales': 'first'}
    entidades_total = entidades_total.groupby(['organization',region]).aggregate(aggregation_functions).reset_index().fillna('-')
    

##Quitar los .0
entidades_total=entidades_total.astype(str)
entidades_total=entidades_total.replace(to_replace='\.0+$',value="",regex=True)

##Reemplazar los nombres y la url del logo para las entidades que lo requieran

entidades_total.loc[entidades_total.organization=='iNaturalist.org','organization']='Naturalista Colombia'
entidades_total.loc[entidades_total.organization=='Naturalista Colombia','publishingCountry']='CO'
entidades_total.loc[entidades_total.organization=='Naturalista Colombia','logoUrl']='https://statics.sibcolombia.net/sib-resources/images/logos-socios/portal-sib/logo-naturalistaco.png'
entidades_total.loc[entidades_total.publishingCountry=='CO','tipoPublicador']='Nacional'
entidades_total.loc[entidades_total.publishingCountry!='CO','tipoPublicador']='Internacional'
entidades_total.loc[entidades_total.organization=='Naturalista Colombia','typeOrg']='Redes/Iniciativas'
entidades_total.loc[entidades_total.organization=='Cornell Lab of Ornithology','typeOrg']='Redes/Iniciativas'
entidades_total.loc[entidades_total.organization=='Cornell Lab of Ornithology','organization']='eBird Colombia'
entidades_total.loc[entidades_total.organization=='eBird Colombia','logoUrl']='https://statics.sibcolombia.net/sib-resources/images/logos-socios/500px/ebird.jpg'
entidades_total.loc[entidades_total.tipoPublicador=='Internacional','logoUrl']='https://statics.sibcolombia.net/sib-resources/images/santander/world.png'
entidades_total.loc[entidades_total.publishingOrgKey=='827fad55-4521-496e-949c-28e3b0428765','typeOrg']='ONG'
entidades_total.loc[entidades_total.publishingOrgKey=='827fad55-4521-496e-949c-28e3b0428765','logoUrl']='https://raw.githubusercontent.com/SIB-Colombia/logos/main/socio-SiB-cunaguaro.png'
entidades_total.loc[entidades_total.publishingOrgKey=='e62a5313-e771-4c81-b6d1-cba6e4085635','logoUrl']='https://raw.githubusercontent.com/SIB-Colombia/logos/main/socio-SiB-aures.png'
entidades_total.loc[entidades_total.publishingOrgKey=='112087f6-a6c0-4cee-8441-387f900d34f9','logoUrl']='https://raw.githubusercontent.com/SIB-Colombia/logos/main/socio-SiB-udes.png'
entidades_total['URLSocio']='https://biodiversidad.co/data/?publishingOrg='+entidades_total['publishingOrgKey']

entidades_total.loc[entidades_total.typeOrg=='Centros/Institutos de Investigación','typeOrg']='Centros de investigación'
entidades_total.loc[entidades_total.typeOrg=='Autoridades Ambientales','typeOrg']='Autoridades ambientales'
entidades_total.loc[entidades_total.typeOrg=='Redes/Iniciativas','typeOrg']='Redes e iniciativas'
entidades_total.loc[entidades_total.typeOrg=='Entidades Administrativas Territoriales','typeOrg']='Entidades territoriales'
##Exportar los datos a archivos tsv y xlsx

if tipo =='CCDM' or tipo =='MCDM' or tipo =='DCDM':
    #entidades_total['slug_region']='colombia'
    publicadores=entidades_total
    publicadores.columns=['label','slug_region','slug','pais_publicacion','url_logo','tipo_organizacion','registros','registros_salobres','registros_marinos','registros_continentales','especies','especies_salobres','especies_marinas','especies_continentales','tipo_publicador','url_socio']
    publicadores=publicadores[['slug','label','pais_publicacion','tipo_organizacion','tipo_publicador','url_logo','url_socio']]    
    
    entidades_total.columns=['label','slug_region','slug_publicador','pais_publicacion','url_logo','tipo_organizacion','registros','registros_salobres','registros_marinos','registros_continentales','especies','especies_salobres','especies_marinas','especies_continentales','tipo_publicador','url_socio']
    entidades_total=entidades_total[['slug_region','slug_publicador','registros','registros_continentales','registros_marinos','registros_salobres','especies','especies_continentales','especies_marinas','especies_salobres']]  
    
    publicadores = publicadores.drop_duplicates()  
    publicadores.to_csv(nombre+'publicador.tsv',sep='\t', index=False )
    publicadores.to_excel(nombre+'publicador.xlsx', sheet_name='cifrasEntidades', index=False )

if tipo =='CSDM' or tipo =='MSDM' or tipo =='DSDM':
    #entidades_total['slug_region']='colombia'
    publicadores=entidades_total

    publicadores.columns=['label','slug_region','slug','pais_publicacion','url_logo','tipo_organizacion','registros','especies','tipo_publicador','url_socio']
    publicadores=entidades_total[['slug','label','pais_publicacion','tipo_organizacion','tipo_publicador','url_logo','url_socio']]
    
    entidades_total.columns=['label','slug_region','slug_publicador','pais_publicacion','url_logo','tipo_organizacion','registros','especies','tipo_publicador','url_socio']
    entidades_total=entidades_total[['slug_region','slug_publicador','registros','especies']]
    
    publicadores = publicadores.drop_duplicates()    
    publicadores.to_csv(nombre+'publicador.tsv',sep='\t', index=False )
    publicadores.to_excel(nombre+'publicador.xlsx', sheet_name='cifrasEntidades', index=False )


entidades_total.to_csv(nombre+'region_publicador.tsv',sep='\t', index=False )
entidades_total.to_excel(nombre+'region_publicador.xlsx', sheet_name='cifrasEntidades', index=False )


##----------------------------------------------------Obtener tabla de especie--------------------------------------------------##
##Solo se habilita para obtener la lista de especies total para el país y si se cuenta con un corte de datos actualizado
##Obtener los nombres de las especies con su taxonomía superior
especies_colombia=registros.groupby(['species'])['kingdom', 'phylum', 'class', 'order', 'family', 'genus'].first().reset_index()

##Crear la columna de slug
especies_colombia['slug']=especies_colombia['species'].str.lower().replace(to_replace=' ',value="-",regex=True)

##Reordenar las columnas
especies_colombia=especies_colombia[['slug','species','kingdom', 'phylum', 'class', 'order', 'family', 'genus']]

##crear archivo tsv 
especies_colombia.to_csv(nombre+'especie.tsv',sep='\t', index=False)     

##crear archivo excel
especies_colombia.to_excel(nombre+'especie.xlsx', sheet_name='Lista especies', index=False)    

##----------------------------------------------------Obtener tabla de especie_meta--------------------------------------------------##
##Obtener los nombres de las especies con su taxonomía superior
especies_colombia=registros.groupby(['species'])['kingdom', 'phylum', 'class', 'order', 'family', 'genus'].first().reset_index()

##Crear la columna de slug
especies_colombia['slug']=especies_colombia['species'].str.lower().replace(to_replace=' ',value="-",regex=True)

##Reordenar las columnas
especies_colombia=especies_colombia[['slug']]
especies_colombia['vernacular_name_es']=''
especies_colombia['url_especie']=''

##crear archivo tsv 
especies_colombia.to_csv(nombre+'especie_meta.tsv',sep='\t', index=False)     

##crear archivo excel
especies_colombia.to_excel(nombre+'especie_meta.xlsx', sheet_name='Lista especies meta', index=False)       

##----------------------------------------------------Cruzar las tablas especie_meta y especie--------------------------------------------------##
'''
##Crear un dataFrame para almacenar las nuevas especies a agregar
nuevas=pd.DataFrame()

##Cargar la última versión de la tabla especie_meta
meta = pd.read_table('\\especie_meta\\especie_meta.tsv', sep='\t', encoding = "utf8",dtype=str)   

##Obtener los slug de las especies obtenids en el último corte
especies=especies_colombia['slug']

##Crear una lista con los slug de las especies que ya se encuentran almacenadas en la 
## tabla especie_meta
meta_slug=list(meta['slug'])

##Comparar las especies de la tabla nueva talba especie y meta_especie y en el caso
##de que no se encuentre el valor agregarlo en el dataFrame nuevas
nuevas['slug']=especies[especies.isin(meta_slug)==False]

##Agregar todos los registros del dataFrame nuevas al dataFrame meta el cual 
##tendra la información final de la tabla especie_meta para exportar
meta=meta.append(nuevas,ignore_index=True)


##crear archivo tsv 
meta.to_csv(nombre+'_meta_nueva.tsv',sep='\t', index=False)     

##crear archivo excel
meta.to_excel(nombre+'_meta_nueva.xlsx', sheet_name='Lista especies', index=False)
'''

##-------------------------------------------------Calcular especie especie_grupo_biologico y especie_grupo_interes_conservacion---------------------------------------------------##
##Crear los dataFrames que almacenaran la información separada de grupos 
##biologicos y grupos de interes
dff_biologicos=pd.DataFrame()
dff_interes=pd.DataFrame()

##Crear los diccionarios que contienen la información de cada ciclo por 
##categoría taxonomica
dic_kingdom,dic_phylum, dic_class, dic_order,dic_family,dic_genus,dic_species={},{},{},{},{},{},{}

##Obtener el listado de espcies unicas con su taxonomía superior
especies_colombiagb=registros.groupby(['species'])['kingdom', 'phylum', 'class', 'order', 'family', 'genus'].first().reset_index()

##Obtener los grupos biologicos que aplican en cada categoría taxonomíca
reino=grupos_biologicos[grupos_biologicos['taxonRank']=='kingdom']
filo=grupos_biologicos[grupos_biologicos['taxonRank']=='phylum']
clase=grupos_biologicos[grupos_biologicos['taxonRank']=='class']
orden=grupos_biologicos[grupos_biologicos['taxonRank']=='order']
familia=grupos_biologicos[grupos_biologicos['taxonRank']=='family']
genero=grupos_biologicos[grupos_biologicos['taxonRank']=='genus']
especie=grupos_biologicos[grupos_biologicos['taxonRank']=='species']


#Obtener los id de cada grupo primero para grupos biologicos 
##y despues para grupos de interes
gb=list(grupos_biologicos['grupo_id'][grupos_biologicos['tipo_grupo']=='biologico'])
gi=list(grupos_biologicos['grupo_id'][grupos_biologicos['tipo_grupo']=='interes'])


##Se recorre el listado de especies unica y los diferentes niveles taxonomicos
##en el caso de que la especie o la taxonomia superior coincida con algún elemento
##del dataFrame de cada categoría se agrega al diccionario correspondiente
for i in especies_colombiagb.index:
    for ii in reino.index:
        if especies_colombiagb['kingdom'][i]==reino['grupoTax'][ii]:  
            dic_kingdom[especies_colombiagb['species'][i]+'|'+reino['grupo_id'][ii]]=reino['grupo_id'][ii]
            
    for ii in filo.index:
        if especies_colombiagb['phylum'][i]==filo['grupoTax'][ii]:         
            dic_phylum[especies_colombiagb['species'][i]+'|'+filo['grupo_id'][ii]]=filo['grupo_id'][ii]

    for ii in clase.index:
        if especies_colombiagb['class'][i]==clase['grupoTax'][ii]:        
            dic_class[especies_colombiagb['species'][i]+'|'+clase['grupo_id'][ii]]=clase['grupo_id'][ii]
    
    for ii in orden.index:        
        if especies_colombiagb['order'][i]==orden['grupoTax'][ii]:        
            dic_order[especies_colombiagb['species'][i]+'|'+orden['grupo_id'][ii] ]=orden['grupo_id'][ii] 
            
    for ii in familia.index:
        if especies_colombiagb['family'][i]==familia['grupoTax'][ii]:        
            dic_family[especies_colombiagb['species'][i]+'|'+familia['grupo_id'][ii] ]=familia['grupo_id'][ii] 
            
    for ii in genero.index:
        if especies_colombiagb['genus'][i]==genero['grupoTax'][ii]:        
            dic_genus[especies_colombiagb['species'][i]+'|'+genero['grupo_id'][ii]]=genero['grupo_id'][ii] 
    
    for ii in especie.index:
        if especies_colombiagb['species'][i]==especie['grupoTax'][ii]: 
            dic_species[especies_colombiagb['species'][i]+'|'+especie['grupo_id'][ii]]=especie['grupo_id'][ii] 
            #dic_species.setdefault(especies_colombiagb['species'][i], []).append(especie['grupo_id'][ii])


##Cada diccionario se convierte en un dataFrame, se ajustan filas y columnas
##y se unen en un dataFrame final
df_kingdom = pd.DataFrame(dic_kingdom, index=['slug_grupo_biologico'])
df_kingdom=(df_kingdom.T)
df_kingdom = df_kingdom.rename_axis('slug_especie').reset_index()
df_kingdom[['slug_especie','slug_grupo_biologico']]=df_kingdom.slug_especie.str.split('|',expand=True)

df_phylum = pd.DataFrame(dic_phylum, index=['slug_grupo_biologico'])
df_phylum=(df_phylum.T)
df_phylum = df_phylum.rename_axis('slug_especie').reset_index()
df_phylum[['slug_especie','slug_grupo_biologico']]=df_phylum.slug_especie.str.split('|',expand=True)

df_class = pd.DataFrame(dic_class, index=['slug_grupo_biologico'])
df_class=(df_class.T)
df_class = df_class.rename_axis('slug_especie').reset_index()
df_class[['slug_especie','slug_grupo_biologico']]=df_class.slug_especie.str.split('|',expand=True)

df_order = pd.DataFrame(dic_order, index=['slug_grupo_biologico'])
df_order=(df_order.T)
df_order = df_order.rename_axis('slug_especie').reset_index()
if df_order.slug_especie.any():
    df_order[['slug_especie','slug_grupo_biologico']]=df_order.slug_especie.str.split('|',expand=True)

df_family = pd.DataFrame(dic_family, index=['slug_grupo_biologico'])
df_family=(df_family.T)
df_family = df_family.rename_axis('slug_especie').reset_index()
df_family[['slug_especie','slug_grupo_biologico']]=df_family.slug_especie.str.split('|',expand=True)

df_genus = pd.DataFrame(dic_genus, index=['slug_grupo_biologico'])
df_genus=(df_genus.T)
df_genus = df_genus.rename_axis('slug_especie').reset_index()
df_genus[['slug_especie','slug_grupo_biologico']]=df_genus.slug_especie.str.split('|',expand=True)

df_species = pd.DataFrame(dic_species, index=['slug_grupo_biologico'])
df_species=(df_species.T)
df_species = df_species.rename_axis('slug_especie').reset_index()
if df_species.slug_especie.any():
    df_species[['slug_especie','slug_grupo_biologico']]=df_species.slug_especie.str.split('|',expand=True)

df_final=pd.concat([df_kingdom,df_phylum,df_class,df_order,df_family,df_genus,df_species]) 

##Se aplica  un consulta en el dataFrame final y en el caso de que el grupo 
#se encuentre en la lista de grupos biologicos o grupos de interes se mantiene 
##en el respectivo dataFrame
#dff_biologicos=df_final[df_final['slug_grupo_biologico'].isin(gb)==True]
#dff_interes=df_final[df_final['slug_grupo_biologico'].isin(gi)==True]

df_final['slug_especie']=df_final['slug_especie'].str.lower().replace(to_replace=' ',value="-",regex=True)
#dff_biologicos['slug_especie']=dff_biologicos['slug_especie'].str.lower().replace(to_replace=' ',value="-",regex=True)
#dff_interes['slug_especie']=dff_interes['slug_especie'].str.lower().replace(to_replace=' ',value="-",regex=True)

##crear archivo csv 
df_final.to_csv(nombre+'especie_grupo_biologico.tsv',sep='\t', index=False )     
#dff_biologicos.to_csv(nombre+'especie_grupo_biologico.tsv',sep='\t', index=False )     
#dff_interes.to_csv(nombre+'especie_grupo_interes_conservacion.tsv',sep='\t', index=False )     

##crear archivo excel y la hoja Cifras totales
df_final.to_excel(nombre+'especie_grupo_biologico.xlsx', sheet_name='Lista especies_grupoBiologico', index=False )         
#dff_biologicos.to_excel(nombre+'especie_grupo_biologico.xlsx', sheet_name='Lista especies_grupoBiologico', index=False )         
#dff_interes.to_excel(nombre+'especie_grupo_interes_conservacion.xlsx', sheet_name='Lista especies_grupoInteres', index=False )                      


##--------------------------------------------Calcular número de registros por especie en la región ---------------------------------------------##

if tipo=='DCDM' or tipo=='DSDM' or tipo=='MCDM' or tipo=='MSDM':
    especies_region=registros.groupby(['species',region])['species'].count().to_frame(name = 'registros').reset_index()
    ##Crear la columna de slug
    especies_region['slug_specie']=especies_region['species'].str.lower().replace(to_replace=' ',value="-",regex=True)
    especies_region.columns=['species','slug_region','registros','slug_species']
    especies_region=especies_region[['slug_region','slug_species','registros']]

if tipo=='CCDM' or tipo=='CSDM':
    especies_region=registros.groupby(['species'])['species'].count().to_frame(name = 'registros').reset_index()

    ##Crear la columna de slug
    especies_region['slug_specie']=especies_region['species'].str.lower().replace(to_replace=' ',value="-",regex=True)
    especies_region['slug_region']='colombia'
    especies_region.columns=['species','registros','slug_specie','slug_region']
    especies_region=especies_region[['slug_region','slug_specie','registros']]
    

##crear archivo csv 
especies_region.to_csv(nombre+'especie_region.tsv',sep='\t', index=False)     
##crear archivo excel y la hoja Cifras totales
especies_region.to_excel(nombre+'especie_region.xlsx', sheet_name='Lista especies', index=False) 

##--------------------------------------------Especies con tematica por región ---------------------------------------------##
'''
El loop permite iterar en el conjunto de datos, realizando diferentes procesos y creación de variables, teniendo en cuenta 
las categorias tematicas y los grupos taxonómicos 

Dentro de los nombres se encuentran las siguientes convenciones:
rb= Registros biologicos
sp= especies
geo= geografía
'''

sp_tematica_region=pd.DataFrame()

## Crear listas con las categorías temáticas sobre las cuales se corre el loop
tematica =['threatStatus_UICN','especies_invasoras','appendixCITES','threatStatus_MADS','especies_exoticas','especies_exotica_riesgo_invasion','endemic','migratory']

  
for t in tematica:                       
    ##Se crea una variable que almacena las especies y la temática de interés
    if tipo=='DCDM' or tipo=='DSDM' or tipo=='MCDM' or tipo=='MSDM':
        rb_numero_categoria = registros.groupby(['species',t,region])[t].count().to_frame(name = 'registros' ).reset_index()
    if tipo=='CCDM' or tipo=='CSDM':
        rb_numero_categoria = registros.groupby(['species',t])[t].count().to_frame(name = 'registros' ).reset_index()
        rb_numero_categoria['slug']='colombia'
        
    rb_numero_categoria['thematic']=t
    rb_numero_categoria = rb_numero_categoria[rb_numero_categoria['species'].notna()]  
    rb_numero_categoria=rb_numero_categoria.rename(columns={t:'category'})
    sp_tematica_region=pd.concat([sp_tematica_region,rb_numero_categoria])    

        
##Se eliminan duplicados y se terminan de ajustar los nombres de todo el conjunto de datos
sp_tematica_region = sp_tematica_region.drop_duplicates()
sp_tematica_region=sp_tematica_region.replace('Invasora GRIIS','exotica-riesgo-invasion',regex=True)
sp_tematica_region=sp_tematica_region.replace('LC_IUCN','amenazadas-global-lc',regex=True)         
sp_tematica_region=sp_tematica_region.replace('NT_IUCN','amenazadas-global-nt',regex=True)    
sp_tematica_region=sp_tematica_region.replace('VU_IUCN','amenazadas-global-vu',regex=True)
sp_tematica_region=sp_tematica_region.replace('EN_IUCN','amenazadas-global-en',regex=True)           
sp_tematica_region=sp_tematica_region.replace('CR_IUCN','amenazadas-global-cr',regex=True)    
sp_tematica_region=sp_tematica_region.replace('DD_IUCN','amenazadas-global-dd',regex=True)
sp_tematica_region=sp_tematica_region.replace('LR/lc_IUCN','amenazadas-global-lr-lc',regex=True)           
sp_tematica_region=sp_tematica_region.replace('LR/nt_IUCN','amenazadas-global-lr-nt',regex=True)    
sp_tematica_region=sp_tematica_region.replace('EW_IUCN','amenazadas-global-ew',regex=True)
sp_tematica_region=sp_tematica_region.replace('EX_IUCN','amenazadas-global-ex',regex=True)           
sp_tematica_region=sp_tematica_region.replace('NE_IUCN','amenazadas-global-ne',regex=True) 
sp_tematica_region=sp_tematica_region.replace('LR/cd_IUCN','amenazadas-global-lr-cd',regex=True)    
sp_tematica_region=sp_tematica_region.replace('Invasora_MADS','invasoras',regex=True)  
sp_tematica_region=sp_tematica_region.replace('I/II','cites-i-ii',regex=True)   
sp_tematica_region=sp_tematica_region.replace('III','cites-iii',regex=True)
sp_tematica_region=sp_tematica_region.replace('II','cites-ii',regex=True)            
sp_tematica_region=sp_tematica_region.replace('I','cites-i',regex=True)         
sp_tematica_region=sp_tematica_region.replace('VU_MADS','amenazadas-nacional-vu',regex=True)    
sp_tematica_region=sp_tematica_region.replace('EN_MADS','amenazadas-nacional-en',regex=True)
sp_tematica_region=sp_tematica_region.replace('CR_MADS','amenazadas-nacional-cr',regex=True)           
sp_tematica_region=sp_tematica_region.replace('Exótica','exoticas',regex=True)    
sp_tematica_region=sp_tematica_region.replace('Endémica','endemicas',regex=True)           
sp_tematica_region=sp_tematica_region.replace('Migratorio','migratorias',regex=True)    
sp_tematica_region=sp_tematica_region.replace('Errática','erraticas',regex=True)
sp_tematica_region=sp_tematica_region.replace('Residente','residente',regex=True)           



sp_tematica_region['slug_specie']=sp_tematica_region['species'].str.lower().replace(to_replace=' ',value="-",regex=True)

if tipo=='MCDM' or tipo=='MSDM':
    sp_tematica_region=sp_tematica_region[['slug_specie','slug_y','category']]
    sp_tematica_region.columns=['slug_specie','slug_region','slug_tematica']

if tipo=='DCDM' or tipo=='DSDM' or tipo=='CCDM' or tipo=='CSDM':
    sp_tematica_region=sp_tematica_region[['slug_specie','slug','category']]
    sp_tematica_region.columns=['slug_specie','slug_region','slug_tematica']

##crear archivo csv 
sp_tematica_region.to_csv(nombre+'especie_tematica.tsv',sep='\t', index=False)     
##crear archivo excel y la hoja Cifras totales
sp_tematica_region.to_excel(nombre +'especie_tematica.xlsx', sheet_name='Lista especies', index=False)         

##----------------------------------------------------Cifras totales -----------------------------------------------------------##
'''
En esta sección se calculan las cifras totales para el departamento o el país sin tener en cuenta temáticas de interés, publicadores
o grupos biológicos

'''
##Crear un dataframe vacío
total_cifras=pd.DataFrame(index=[0])

##Crear variable para los conteos por especie general
variable_conteos = registros[registros['species'].notna()].drop_duplicates('species').sort_values(by=['species']) 

##Crear la columna registros y especies general contando el campo gbifID
total_cifras['registros'],total_cifras['especies']=registros['gbifID'].count(),variable_conteos['gbifID'].count()

##Condicional para registros marinos, continentales y salobres
if tipo =='MCDM' or tipo =='DCDM' or tipo =='CCDM':
    ##Crear un subconjunto de datos para los registros marinos, continentales y salobres
    registros_marinos=registros[registros['isMarine']=='Marine']
    registros_continentales=registros[registros['isTerrestrial']=='Terrestrial']
    registros_salobres=registros[registros['isBrackish']=='Brackish']

    ##Crear variables para los conteos por especie de las categorias marinos, continentales y salobres
    #variable_conteos_marinos = registros_marinos[registros_marinos['species'].notna()].drop_duplicates('species').sort_values(by=['species']) #se puede dejar en una sola variable
    #variable_conteos_continentales = registros_continentales[registros_continentales['species'].notna()].drop_duplicates('species').sort_values(by=['species']) #se puede dejar en una sola variable
    #variable_conteos_salobres = registros_salobres[registros_salobres['species'].notna()].drop_duplicates('species').sort_values(by=['species']) #se puede dejar en una sola variable

    ##Crear las columnas registros marinos y especies marinas
    total_cifras['registrosMarinos'],total_cifras['especiesMarinas']=registros['isMarine'].count(),variable_conteos['isMarine'].count()
    
    ##Crear la columna registros salobres y especies salobres
    total_cifras['registrosSalobres'],total_cifras['especiesSalobres']=registros['isBrackish'].count(),variable_conteos['isBrackish'].count()

    ##Crear las columnas registros continentales y especies continentales
    total_cifras['registrosContinentales'],total_cifras['especiesContinentales']=registros['isTerrestrial'].count(),variable_conteos['isTerrestrial'].count()
    total_cifras.columns=['registros','especies','registros_marinos','especies_marinas','registros_salobres', 'especies_salobres', 'registros_continentales', 'especies_continentales']
    total_cifras=total_cifras[['registros','registros_continentales','registros_marinos','registros_salobres','especies','especies_continentales', 'especies_marinas', 'especies_salobres']]

##crear archivo tsv 
total_cifras.to_csv(nombre+'cifrasTotales.tsv',sep='\t', index=False ) 

##crear archivo excel y la hoja Cifras totales
total_cifras.to_excel(nombre +'cifrasTotales.xlsx', sheet_name='Cifras totales', index=False)



##---------------------------------------------Cifras generales geográficas --------------------------------------------------##
'''
Dentro de esta sección se define el alcance geográfico del conjunto de datos. Se selecciona si se va a trabajar a nivel nacional o
departamental. En caso de que se trabaje a nivel nacional, las cifras geográficas harán referencia a los departamentos. Para el nivel 
departamental las cifras se obtendrán para cada uno de los municipios encontrados dentro del departamento de interés
Dentro de los nombres se encuentran las siguientes convenciones:
rb= Registros biologicos
sp= especies
geo= geografía
'''

##Crear dataframes vacíos para almacenar las cifras geográficas de especies y registros
geo_rb_total=pd.DataFrame()
geo_sp_total=pd.DataFrame()

##Cifras registros por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geográficas y contar los registros generales
##Crear columna registros
if tipo =='MCDM' or tipo =='DCDM' or tipo =='MSDM' or tipo =='DSDM' or tipo =='CCDM' or tipo =='CSDM':
    geo_rb_total['registros']= registros.groupby(region)['gbifID'].count()

    ##Cifras especies por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geografica y contar los registros por species
    ##Crear un dataframe con los datos. Agrupar por entidad geografica y contar los registros por species
    geo_sp_total= registros.groupby([region,'species'])['species'].count().to_frame(name = 'registros').reset_index()
    geo_sp_total= geo_sp_total.groupby([region])['species'].count().to_frame(name = 'especies').reset_index().sort_values(region).fillna('-')

if tipo =='MCDM' or tipo =='DCDM' or tipo =='CCDM':
    
    ##Cifras registros por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geográfica y contar los registros para cada una de las categorías (marinos, salobres y continentales)
    ##Crear columna registros para datos marinos
    geo_rb_total['registrosMarinos']= registros.groupby(region)['isMarine'].count()
    ##Crear columna registros para datos salobres
    geo_rb_total['registrosSalobres']= registros.groupby(region)['isBrackish'].count()
    ##Crear columna registros para datos continentales
    geo_rb_total['registrosContinentales']= registros.groupby(region)['isTerrestrial'].count()

    ##Cifras especies por entidad geográfica: Crear un dataframe con los datos, agrupar los datos por entidad geográfica y contar los registros por species para cada una de las categorías (marinos, salobres y continentales)
    ##Crear conjunto de datos con especies y geografía para datos marinos y Contar número de especies marinos para entidad geográfica 
    geo_sp_marino=registros.groupby([region,'species','isMarine'])['isMarine'].count().to_frame(name = 'registros').reset_index()
    geo_sp_marino=geo_sp_marino.groupby(region)['registros'].count().to_frame(name = 'especiesMarinas').reset_index()
    
    ##Crear conjunto de datos con especies y geografía para datos salobres y Contar número de especies salobres para entidad geográfica 
    geo_sp_salobre=registros.groupby([region,'species','isBrackish'])['isBrackish'].count().to_frame(name = 'registros').reset_index()
    geo_sp_salobre=geo_sp_salobre.groupby(region)['registros'].count().to_frame(name = 'especiesSalobres').reset_index()

    ##Crear conjunto de datos con especies y geografía para datos continentales y Contar número de especies continentales para entidad geográfica 
    ##Contar número de especies salobres para entidad geográfica 
    geo_sp_continental=registros.groupby([region,'species','isTerrestrial'])['isTerrestrial'].count().to_frame(name = 'registros').reset_index()
    geo_sp_continental=geo_sp_continental.groupby(region)['registros'].count().to_frame(name = 'especiesContinentales').reset_index()

    ##Unir todos los conjuntos de datos, Reemplazar los valores NaN y ordenar las entidades geográficas
    geo_sp_total=pd.merge(geo_sp_total,geo_sp_salobre, on=[region],how='left').merge(geo_sp_marino, on=[region],how='left').merge(geo_sp_continental, on=[region],how='left').sort_values(region).fillna('-')


##Quitar los .0
#geo_sp_total=geo_sp_total.astype(str)
#geo_sp_total=geo_sp_total.replace(to_replace='\.0+$',value="",regex=True)
#geo_rb_total=geo_rb_total.astype(str)
#geo_rb_total=geo_rb_total.replace(to_replace='\.0+$',value="",regex=True)


##-------------------------------------------------Loop calculo cifras ---------------------------------------------------##
'''
El loop permite iterar en el conjunto de datos, realizando diferentes procesos y creación de variables, teniendo en cuenta 
las categorias tematicas y los grupos taxonómicos 

Durante el loop se realiza el conteo de los registros biologicos y el número de especies para las diferenes categorias. Dentro de 
los nombres se encuentran las siguientes convenciones:
rb= Registros biologicos
sp= especies
geo= geografía
'''

## Crear listas con las categorías tematicas sobre las cuales se corre el loop
tematica =['threatStatus_UICN','especies_invasoras','appendixCITES','threatStatus_MADS','especies_exoticas','especies_exotica_riesgo_invasion','endemic','migratory']

## Define una lista con todos las categorias taxonomicas
taxon = ['kingdom','phylum','class','order','family','genus','species'] 

## Creación de dataframes vacíos para guardar las cifras generadas en el loop
## Dataframes para registros
rb_taxon_total=pd.DataFrame()
rb_tematica_total=pd.DataFrame()
rb_taxon_tematica_total=pd.DataFrame()
rb_taxon_categoria_total=pd.DataFrame()

## Dataframes para especies
sp_taxon_total=pd.DataFrame()
sp_tematica_total=pd.DataFrame()
sp_taxon_tematica_total=pd.DataFrame()
sp_taxon_categoria_total=pd.DataFrame()

## Dataframes geográficos
geo_tematica_rb_total=pd.DataFrame() 
geo_tematica_sp_total=pd.DataFrame()
geo_categoria_rb_total=pd.DataFrame()
geo_categoria_sp_total=pd.DataFrame()


## La primera parte del loop recorre las categorias temáticas 
for i in tematica:

    if tipo =='CSDM' or tipo =='CCDM':    
        ## Cifras registros: Agrupar por categorías temáticas y contar por gbifID generales
        rb_numero = registros.groupby(i)['gbifID'].count().to_frame(name = 'registros' ).reset_index()
        ##Asignar al campo 'thematic' el valor que tiene i 
        rb_numero['thematic']=i
        rb_numero=rb_numero.rename(columns={i:'category'})

    if tipo =='CCDM':
        ## Cifras registros marinos: Agrupar por categorías temáticas y contar por gbifID con categoría marinos
        rb_numero_marino = registros_marinos.groupby([i,'isMarine'])['gbifID'].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
        rb_numero_marino['thematic']=i
        rb_numero_marino=rb_numero_marino.rename(columns={i:'category'})

        ## Cifras registros continentales: Agrupar por categorías temáticas y contar por gbifID con categoría continental
        rb_numero_continental = registros_continentales.groupby([i,'isTerrestrial'])['gbifID'].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
        rb_numero_continental['thematic']=i
        rb_numero_continental=rb_numero_continental.rename(columns={i:'category'})

        ## Cifras registros salobres: Agrupar por categorías temáticas y contar por gbifID con categoría salobre    
        rb_num_salobre = registros_salobres.groupby([i,'isBrackish'])['gbifID'].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
        rb_num_salobre['thematic']=i
        rb_num_salobre=rb_num_salobre.rename(columns={i:'category'})

        ##Agrupar las cifras marinas, continentales y salobres
        rb_tematica=pd.merge(rb_numero,rb_numero_continental, on=['category','thematic'],how='left').merge(rb_numero_marino, on=['category','thematic'],how='left').merge(rb_num_salobre, on=['category','thematic'],how='left')
        ##Almacenar la información de cada recorrido del loop
        rb_tematica_total=pd.concat([rb_tematica_total,rb_tematica])
    
    if tipo =='MCDM' or tipo =='DCDM' or tipo =='MSDM' or tipo =='DSDM':
        ## Cifras registros: Agrupar por categorías temáticas y contar por gbifID generales
        rb_numero = registros.groupby([i,region])['gbifID'].count().to_frame(name = 'registros' ).reset_index()
        ##Asignar al campo 'thematic' el valor que tiene i 
        rb_numero['thematic']=i
        rb_numero=rb_numero.rename(columns={i:'category'})
    
        ##Condicional para registros marinos, continentales y salobres
    if tipo =='MCDM' or tipo =='DCDM':
        ## Cifras registros marinos: Agrupar por categorías temáticas y contar por gbifID con categoría marinos
        rb_numero_marino = registros_marinos.groupby([i,'isMarine',region])['gbifID'].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
        rb_numero_marino['thematic']=i
        rb_numero_marino=rb_numero_marino.rename(columns={i:'category'})

        ## Cifras registros continentales: Agrupar por categorías temáticas y contar por gbifID con categoría continental
        rb_numero_continental = registros_continentales.groupby([i,'isTerrestrial',region])['gbifID'].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
        rb_numero_continental['thematic']=i
        rb_numero_continental=rb_numero_continental.rename(columns={i:'category'})

        ## Cifras registros salobres: Agrupar por categorías temáticas y contar por gbifID con categoría salobre    
        rb_num_salobre = registros_salobres.groupby([i,'isBrackish',region])['gbifID'].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
        rb_num_salobre['thematic']=i
        rb_num_salobre=rb_num_salobre.rename(columns={i:'category'})

        ##Agrupar las cifras marinas, continentales y salobres
        rb_tematica=pd.merge(rb_numero,rb_numero_continental, on=['category','thematic',region],how='left').merge(rb_numero_marino, on=['category','thematic',region],how='left').merge(rb_num_salobre, on=['category','thematic',region],how='left')
        ##Almacenar la información de cada recorrido del loop
        rb_tematica_total=pd.concat([rb_tematica_total,rb_tematica])   
    
    if tipo =='MSDM' or tipo =='DSDM' or tipo =='CSDM':
        ##Almacenar la información de cada recorrido del loop
        rb_tematica_total=pd.concat([rb_tematica_total,rb_numero])

    if tipo =='CSDM' or tipo =='CCDM': 
        ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID generales
        spp_numero=registros.drop_duplicates([i,'species'])##
        sp_numero = spp_numero.groupby([i])['species'].count().to_frame(name = 'especies' ).reset_index()
        sp_numero['thematic']=i
        sp_numero=sp_numero.rename(columns={i:'category'})

    if tipo =='MCDM' or tipo =='DCDM' or tipo =='MSDM' or tipo =='DSDM':
        ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID generales
        spp_numero=registros.drop_duplicates([region, i,'species'])##
        sp_numero = spp_numero.groupby([i,region])['species'].count().to_frame(name = 'especies' ).reset_index()
        sp_numero['thematic']=i
        sp_numero=sp_numero.rename(columns={i:'category'})
    
    ##Condicional para registros marinos, continentales y salobres
    if tipo =='MCDM' or tipo =='DCDM':
        ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID para la categoría marinos
        spp_numero=registros_marinos.drop_duplicates([region, i,'species'])##
        sp_numero_marino = spp_numero.groupby([i,region,'isMarine'])['gbifID'].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
        #sp_numero_marino = variable_conteos_marinos.groupby([i,'isMarine'])['gbifID'].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
        sp_numero_marino['thematic']=i
        sp_numero_marino=sp_numero_marino.rename(columns={i:'category'})    

        ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID para la categoría continental
        spp_numero=registros_continentales.drop_duplicates([region, i,'species'])##
        sp_numero_continental = spp_numero.groupby([i,region,'isTerrestrial'])['gbifID'].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
        #sp_numero_continental = variable_conteos_continentales.groupby([i,'isTerrestrial'])['gbifID'].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
        sp_numero_continental['thematic']=i
        sp_numero_continental=sp_numero_continental.rename(columns={i:'category'})  

        ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID para la categoría salobre
        spp_numero=registros_salobres.drop_duplicates([region, i,'species'])##
        sp_numero_salobre = spp_numero.groupby([i,region,'isBrackish'])['gbifID'].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
        #sp_numero_salobre = variable_conteos_salobres.groupby([i,'isBrackish'])['gbifID'].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
        sp_numero_salobre['thematic']=i
        sp_numero_salobre=sp_numero_salobre.rename(columns={i:'category'})  

        ##Agrupar las cifras marinas, continentales y salobres
        sp_tematica=pd.merge(sp_numero,sp_numero_continental, on=['category','thematic',region],how='left').merge(sp_numero_marino, on=['category','thematic',region],how='left').merge(sp_numero_salobre, on=['category','thematic',region],how='left')
        sp_tematica_total=pd.concat([sp_tematica_total,sp_tematica])    

    ##Condicional para registros marinos, continentales y salobres
    if tipo =='CCDM':
        ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID para la categoría marinos
        spp_numero=registros_marinos.drop_duplicates([i,'species'])##
        sp_numero_marino = spp_numero.groupby([i,'isMarine'])['gbifID'].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
        #sp_numero_marino = variable_conteos_marinos.groupby([i,'isMarine'])['gbifID'].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
        sp_numero_marino['thematic']=i
        sp_numero_marino=sp_numero_marino.rename(columns={i:'category'})    

        ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID para la categoría continental
        spp_numero=registros_continentales.drop_duplicates([i,'species'])##
        sp_numero_continental = spp_numero.groupby([i,'isTerrestrial'])['gbifID'].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
        #sp_numero_continental = variable_conteos_continentales.groupby([i,'isTerrestrial'])['gbifID'].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
        sp_numero_continental['thematic']=i
        sp_numero_continental=sp_numero_continental.rename(columns={i:'category'})  

        ## Cifras especies: Agrupar por categorías temáticas y contar por gbifID para la categoría salobre
        spp_numero=registros_salobres.drop_duplicates([i,'species'])##
        sp_numero_salobre = spp_numero.groupby([i,'isBrackish'])['gbifID'].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
        #sp_numero_salobre = variable_conteos_salobres.groupby([i,'isBrackish'])['gbifID'].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
        sp_numero_salobre['thematic']=i
        sp_numero_salobre=sp_numero_salobre.rename(columns={i:'category'})  

        ##Agrupar las cifras marinas, continentales y salobres
        sp_tematica=pd.merge(sp_numero,sp_numero_continental, on=['category','thematic'],how='left').merge(sp_numero_marino, on=['category','thematic'],how='left').merge(sp_numero_salobre, on=['category','thematic'],how='left')
        sp_tematica_total=pd.concat([sp_tematica_total,sp_tematica]) 
        
    if tipo =='MSDM' or tipo =='DSDM' or tipo =='CSDM':
        sp_tematica_total=pd.concat([sp_tematica_total,sp_numero])
   

    ## Cifras registros por entidad geográfica: Agrupar por geografía y categorías temáticas, contar por temática generales
    geo_rb= registros.groupby([region,i])[i].count().to_frame(name = 'registros').reset_index()
    geo_rb['thematic']=i
    geo_rb=geo_rb.rename(columns={i:'category'})


 ##Condicional para registros marinos, continentales y salobres
    if tipo =='MCDM' or tipo =='DCDM' or tipo =='CCDM':
        ## Cifras registros por entidad geográfica: Agrupar por geografía y categorías temáticas, contar por temática para marinos
        geo_rb_marino= registros_marinos.groupby([region,i,'isMarine'])[i].count().to_frame(name = 'registrosMarinos').reset_index().drop(['isMarine'], axis=1)
        geo_rb_marino['thematic']=i
        geo_rb_marino=geo_rb_marino.rename(columns={i:'category'})

        ## Cifras registros por entidad geográfica: Agrupar por geografía y categorías temáticas, contar por temática para continentales
        geo_rb_continental= registros_continentales.groupby([region,i,'isTerrestrial'])[i].count().to_frame(name = 'registrosContinentales').reset_index().drop(['isTerrestrial'], axis=1)
        geo_rb_continental['thematic']=i
        geo_rb_continental=geo_rb_continental.rename(columns={i:'category'})

        ## Cifras registros por entidad geográfica: Agrupar por geografía y categorías temáticas, contar por temática para salobres
        geo_rb_salobre= registros_salobres.groupby([region,i,'isBrackish'])[i].count().to_frame(name = 'registrosSalobres').reset_index().drop(['isBrackish'], axis=1)
        geo_rb_salobre['thematic']=i
        geo_rb_salobre=geo_rb_salobre.rename(columns={i:'category'})

        ##Agrupar las cifras marinas, continentales y salobres
        geo_categoria_rb=pd.merge(geo_rb,geo_rb_continental, on=[region,'category','thematic'],how='left').merge(geo_rb_marino, on=[region,'category','thematic'],how='left').merge(geo_rb_salobre, on=[region,'category','thematic'],how='left')
        geo_categoria_rb_total=pd.concat([geo_categoria_rb_total,geo_categoria_rb])

    
    else:
        geo_categoria_rb_total=pd.concat([geo_categoria_rb_total,geo_rb])
    
    
    
    ## Cifras especies por entidad geográfica: Agrupar por geografía y categorías temáticas y categorías de cada temática general categorías temáticas, contar por temática para marinos, continentales y salobres
    geo_sp= registros.groupby([region,i,'species']).size().reset_index().groupby([region,i]).size().to_frame(name = 'especies').reset_index()
    geo_sp['thematic']=i
    geo_sp= geo_sp.rename(columns={i:'category'})


    if tipo =='MCDM' or tipo =='DCDM' or tipo =='CCDM':
        
        ## Cifras especies por entidad geográfica: Agrupar por geografia y categorias temáticas y categorías de cada temática para marinos
        geo_sp_marino= registros_marinos.groupby([region,i,'species','isMarine']).size().reset_index().groupby([region,i]).size().to_frame(name = 'especiesMarinas').reset_index()
        geo_sp_marino['thematic']=i
        geo_sp_marino= geo_sp_marino.rename(columns={i:'category'})

        ## Cifras especies por entidad geográfica: Agrupar por geografia y categorias temáticas y categorías de cada temática para continentales
        geo_sp_continental= registros_continentales.groupby([region,i,'species','isTerrestrial']).size().reset_index().groupby([region,i]).size().to_frame(name = 'especiesContinentales').reset_index()
        geo_sp_continental['thematic']=i
        geo_sp_continental= geo_sp_continental.rename(columns={i:'category'})

        ## Cifras especies por entidad geográfica: Agrupar por geografia y categorias temáticas y categorías de cada temática para salobres
        geo_sp_salobre= registros_salobres.groupby([region,i,'species','isBrackish']).size().reset_index().groupby([region,i]).size().to_frame(name = 'especiesSalobres').reset_index()
        geo_sp_salobre['thematic']=i
        geo_sp_salobre= geo_sp_salobre.rename(columns={i:'category'})
    
        ##Agrupar las cifras marinas, continentales y salobres
        geo_categoria_sp=pd.merge(geo_sp,geo_sp_continental, on=[region,'category','thematic'],how='left').merge(geo_sp_marino, on=[region,'category','thematic'],how='left').merge(geo_sp_salobre, on=[region,'category','thematic'],how='left')
        geo_categoria_sp_total=pd.concat([geo_categoria_sp_total,geo_categoria_sp])
    
    else:
        geo_categoria_sp_total=pd.concat([geo_categoria_sp_total,geo_sp])


    if tipo =='MSDM' or tipo =='DSDM' or tipo =='MCDM' or tipo =='DCDM' or tipo =='CCDM' or tipo =='CSDM':        
        ## Conteo de total registros biológicos para todas las categorías de cada temática general
        geo_rb = registros.groupby([region])[i].count().to_frame(name = 'registros').reset_index()
        geo_rb['thematic']=i
        geo_rb=geo_rb.rename(columns={i:'category'})

 ##Condicional para registros marinos, continentales y salobres
    if tipo =='MCDM' or tipo =='DCDM' or tipo =='CCDM':
 
        ## Conteo de total registros biológicos para todas las categorías de cada temática para marinos
        geo_rb_marino = registros_marinos.groupby([region,'isMarine'])[i].count().to_frame(name = 'registrosMarinos').reset_index().drop(['isMarine'], axis=1)
        geo_rb_marino['thematic']=i
        geo_rb_marino=geo_rb_marino.rename(columns={i:'category'})

        ## Conteo de total registros biológicos para todas las categorías de cada temática para continentales
        geo_rb_continental = registros_continentales.groupby([region,'isTerrestrial'])[i].count().to_frame(name = 'registrosContinentales').reset_index().drop(['isTerrestrial'], axis=1)
        geo_rb_continental['thematic']=i
        geo_rb_continental=geo_rb_continental.rename(columns={i:'category'})

        ## Conteo de total registros biológicos para todas las categorías de cada temática para salobres
        geo_rb_salobre = registros_salobres.groupby([region,'isBrackish'])[i].count().to_frame(name = 'registrosSalobres').reset_index().drop(['isBrackish'], axis=1)
        geo_rb_salobre['thematic']=i
        geo_rb_salobre=geo_rb_salobre.rename(columns={i:'category'})    
    
        ##Agrupar las cifras marinas, continentales y salobres
        geo_the_rb=pd.merge(geo_rb,geo_rb_continental, on=[region,'thematic'],how='left').merge(geo_rb_marino, on=[region,'thematic'],how='left').merge(geo_rb_salobre, on=[region,'thematic'],how='left')
        geo_tematica_rb_total=pd.concat([geo_tematica_rb_total,geo_the_rb])

    if tipo =='MSDM' or tipo =='DSDM' or tipo =='CSDM':
        geo_tematica_rb_total=pd.concat([geo_tematica_rb_total,geo_rb])


    ## Conteo de total especies únicas para todas las categorías temáticas generales
    geo_sp= registros.groupby([region,i,'species']).size().reset_index().groupby([region])[i].size().to_frame(name = 'especies').reset_index()
    geo_sp['thematic']=i
    geo_sp=geo_sp.rename(columns={i:'category'})

    ##Condicional para registros marinos, continentales y salobres
    if tipo =='MCDM' or tipo =='DCDM' or tipo =='CCDM':

        ## Conteo de total especies únicas para todas las categorias para marinos
        geo_sp_marino= registros_marinos.groupby([region,i,'species','isMarine']).size().reset_index().groupby([region])[i].count().to_frame(name = 'especiesMarinas').reset_index()
        geo_sp_marino['thematic']=i
        geo_sp_marino=geo_sp_marino.rename(columns={i:'category'})

        ## Conteo de total especies únicas para todas las categorias para continentales
        geo_sp_continental= registros_continentales.groupby([region,i,'species','isTerrestrial']).size().reset_index().groupby([region])[i].count().to_frame(name = 'especiesContinentales').reset_index()
        geo_sp_continental['thematic']=i
        geo_sp_continental=geo_sp_continental.rename(columns={i:'category'})

        ## Conteo de total especies únicas para todas las categorias para salobres
        geo_sp_salobre= registros_salobres.groupby([region,i,'species','isBrackish']).size().reset_index().groupby([region])[i].count().to_frame(name = 'especiesSalobres').reset_index()
        geo_sp_salobre['thematic']=i
        geo_sp_salobre=geo_sp_salobre.rename(columns={i:'category'})

        ##Agrupar las cifras marinas, continentales y salobres
        geo_the_sp=pd.merge(geo_sp,geo_sp_continental, on=[region,'thematic'],how='left').merge(geo_sp_marino, on=[region,'thematic'],how='left').merge(geo_sp_salobre, on=[region,'thematic'],how='left')
        geo_tematica_sp_total=pd.concat([geo_tematica_sp_total,geo_the_sp])

    if tipo =='MSDM' or tipo =='DSDM' or tipo =='CSDM':
        geo_tematica_sp_total=pd.concat([geo_tematica_sp_total,geo_sp])

    ## La segunda parte del loop recorre los grupos taxonómicos
    for j in taxon:    
        
        if tipo =='CCDM' or tipo =='CSDM': 
            ##Número de registros por grupo taxonómico general
            rb_numero = registros.groupby(j)['gbifID'].count().to_frame(name = 'registros' ).reset_index()
            rb_numero['taxonRank']=j
            rb_numero=rb_numero.rename(columns={j:'grupoTax'}) 
        
        if tipo =='MCDM' or tipo =='DCDM' or tipo =='MSDM' or tipo =='DSDM':
            ##Número de registros por grupo taxonómico general
            rb_numero = registros.groupby([j,region])['gbifID'].count().to_frame(name = 'registros' ).reset_index()
            rb_numero['taxonRank']=j
            rb_numero=rb_numero.rename(columns={j:'grupoTax'}) 

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='CCDM': 

            ##Número de registros por grupo taxonómico para marinos
            rb_numero_marino = registros_marinos.groupby([j,'isMarine'])['gbifID'].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
            rb_numero_marino['taxonRank']=j
            rb_numero_marino=rb_numero_marino.rename(columns={j:'grupoTax'}) 

            ##Número de registros por grupo taxonómico para continentales
            rb_numero_continental = registros_continentales.groupby([j,'isTerrestrial'])['gbifID'].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            rb_numero_continental['taxonRank']=j
            rb_numero_continental=rb_numero_continental.rename(columns={j:'grupoTax'}) 

            ##Número de registros por grupo taxonómico para salobres
            rb_num_salobre = registros_salobres.groupby([j,'isBrackish'])['gbifID'].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            rb_num_salobre ['taxonRank']=j
            rb_num_salobre=rb_num_salobre.rename(columns={j:'grupoTax'}) 

            ##Agrupar las cifras marinas, continentales y salobres
            rb_tax=pd.merge(rb_numero,rb_numero_continental, on=['taxonRank','grupoTax'],how='left').merge(rb_numero_marino, on=['taxonRank','grupoTax'],how='left').merge(rb_num_salobre, on=['taxonRank','grupoTax'],how='left')
            rb_taxon_total=pd.concat([rb_taxon_total,rb_tax])
            
                ##Condicional para registros marinos, continentales y salobres
        if tipo =='MCDM' or tipo =='DCDM': 

            ##Número de registros por grupo taxonómico para marinos
            rb_numero_marino = registros_marinos.groupby([j,'isMarine',region])['gbifID'].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
            rb_numero_marino['taxonRank']=j
            rb_numero_marino=rb_numero_marino.rename(columns={j:'grupoTax'}) 

            ##Número de registros por grupo taxonómico para continentales
            rb_numero_continental = registros_continentales.groupby([j,'isTerrestrial',region])['gbifID'].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            rb_numero_continental['taxonRank']=j
            rb_numero_continental=rb_numero_continental.rename(columns={j:'grupoTax'}) 

            ##Número de registros por grupo taxonómico para salobres
            rb_num_salobre = registros_salobres.groupby([j,'isBrackish',region])['gbifID'].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            rb_num_salobre ['taxonRank']=j
            rb_num_salobre=rb_num_salobre.rename(columns={j:'grupoTax'}) 

            ##Agrupar las cifras marinas, continentales y salobres
            rb_tax=pd.merge(rb_numero,rb_numero_continental, on=['taxonRank','grupoTax',region],how='left').merge(rb_numero_marino, on=['taxonRank','grupoTax',region],how='left').merge(rb_num_salobre, on=['taxonRank','grupoTax',region],how='left')
            rb_taxon_total=pd.concat([rb_taxon_total,rb_tax])
            
        if tipo =='MSDM' or tipo =='DSDM' or tipo =='CSDM':
            rb_taxon_total=pd.concat([rb_taxon_total,rb_numero])

        
        if tipo =='CCDM' or tipo =='CSDM': 
            ## Número de registros por grupo taxonómico en la categoría general
            rb_numero = registros.groupby(j)[i].count().to_frame(name = 'registros' ).reset_index()
            rb_numero['thematic']=i
            rb_numero['taxonRank']=j
            rb_numero=rb_numero.rename(columns={j:'grupoTax'})
            rb_numero=rb_numero.rename(columns={i:'category'})
        
        if tipo =='MCDM' or tipo =='DCDM' or tipo =='MSDM' or tipo =='DSDM': 
            ## Número de registros por grupo taxonómico en la categoría general
            rb_numero = registros.groupby([j,region])[i].count().to_frame(name = 'registros' ).reset_index()
            rb_numero['thematic']=i
            rb_numero['taxonRank']=j
            rb_numero=rb_numero.rename(columns={j:'grupoTax'})
            rb_numero=rb_numero.rename(columns={i:'category'})

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='CCDM':   
            ## Número de registros por grupo taxonómico en la categoría para marinos
            rb_numero_marino = registros_marinos.groupby([j,'isMarine'])[i].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
            rb_numero_marino['thematic']=i
            rb_numero_marino['taxonRank']=j
            rb_numero_marino=rb_numero_marino.rename(columns={j:'grupoTax'})
            rb_numero_marino=rb_numero_marino.rename(columns={i:'category'})

            ## Número de registros por grupo taxonómico en la categoría para continentales
            rb_numero_continental = registros_continentales.groupby([j,'isTerrestrial'])[i].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            rb_numero_continental['thematic']=i
            rb_numero_continental['taxonRank']=j
            rb_numero_continental=rb_numero_continental.rename(columns={j:'grupoTax'})
            rb_numero_continental=rb_numero_continental.rename(columns={i:'category'})

            ## Número de registros por grupo taxonómico en la categoría para salobres
            rb_num_salobre = registros_salobres.groupby([j,'isBrackish'])[i].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            rb_num_salobre['thematic']=i
            rb_num_salobre['taxonRank']=j
            rb_num_salobre=rb_num_salobre.rename(columns={j:'grupoTax'})
            rb_num_salobre=rb_num_salobre.rename(columns={i:'category'})        
        
            ##Agrupar las cifras marinas, continentales y salobres
            rb_tax_the=pd.merge(rb_numero,rb_numero_continental, on=['taxonRank','grupoTax','thematic'],how='left').merge(rb_numero_marino, on=['taxonRank','grupoTax','thematic'],how='left').merge(rb_num_salobre, on=['taxonRank','grupoTax','thematic'],how='left')
            rb_taxon_tematica_total=pd.concat([rb_taxon_tematica_total,rb_tax_the])

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='MCDM' or tipo =='DCDM':   
            ## Número de registros por grupo taxonómico en la categoría para marinos
            rb_numero_marino = registros_marinos.groupby([region,j,'isMarine'])[i].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
            rb_numero_marino['thematic']=i
            rb_numero_marino['taxonRank']=j
            rb_numero_marino=rb_numero_marino.rename(columns={j:'grupoTax'})
            rb_numero_marino=rb_numero_marino.rename(columns={i:'category'})

            ## Número de registros por grupo taxonómico en la categoría para continentales
            rb_numero_continental = registros_continentales.groupby([region,j,'isTerrestrial'])[i].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            rb_numero_continental['thematic']=i
            rb_numero_continental['taxonRank']=j
            rb_numero_continental=rb_numero_continental.rename(columns={j:'grupoTax'})
            rb_numero_continental=rb_numero_continental.rename(columns={i:'category'})

            ## Número de registros por grupo taxonómico en la categoría para salobres
            rb_num_salobre = registros_salobres.groupby([region,j,'isBrackish'])[i].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            rb_num_salobre['thematic']=i
            rb_num_salobre['taxonRank']=j
            rb_num_salobre=rb_num_salobre.rename(columns={j:'grupoTax'})
            rb_num_salobre=rb_num_salobre.rename(columns={i:'category'})        
        
            ##Agrupar las cifras marinas, continentales y salobres
            rb_tax_the=pd.merge(rb_numero,rb_numero_continental, on=[region,'taxonRank','grupoTax','thematic'],how='left').merge(rb_numero_marino, on=[region,'taxonRank','grupoTax','thematic'],how='left').merge(rb_num_salobre, on=[region,'taxonRank','grupoTax','thematic'],how='left')
            rb_taxon_tematica_total=pd.concat([rb_taxon_tematica_total,rb_tax_the])

        if tipo =='MSDM' or tipo =='DSDM' or tipo =='CSDM':
            rb_taxon_tematica_total=pd.concat([rb_taxon_tematica_total,rb_numero])



        if tipo =='MCDM' or tipo =='DCDM' or tipo =='MSDM' or tipo =='DSDM':
            ## Número de registros por grupo taxonómico y categoría temática en general
            rb_numero = registros.groupby([j,i,region])[i].count().to_frame(name = 'registros' ).reset_index()
            rb_numero['thematic']=i
            rb_numero['taxonRank']=j
            rb_numero=rb_numero.rename(columns={j:'grupoTax'})
            rb_numero=rb_numero.rename(columns={i:'category'})
            
        if tipo =='CCDM' or tipo =='CSDM':
            ## Número de registros por grupo taxonómico y categoría temática en general
            rb_numero = registros.groupby([j,i])[i].count().to_frame(name = 'registros' ).reset_index()
            rb_numero['thematic']=i
            rb_numero['taxonRank']=j
            rb_numero=rb_numero.rename(columns={j:'grupoTax'})
            rb_numero=rb_numero.rename(columns={i:'category'})

        if tipo =='CCDM':
            ## Número de registros por grupo taxonómico y categoría temática para marinos
            rb_numero_marino = registros_marinos.groupby([j,i,'isMarine'])[i].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
            rb_numero_marino['thematic']=i
            rb_numero_marino['taxonRank']=j
            rb_numero_marino=rb_numero_marino.rename(columns={j:'grupoTax'})
            rb_numero_marino=rb_numero_marino.rename(columns={i:'category'})

            ## Número de registros por grupo taxonómico y categoría temática para continentales
            rb_numero_continental = registros_continentales.groupby([j,i,'isTerrestrial'])[i].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            rb_numero_continental['thematic']=i
            rb_numero_continental['taxonRank']=j
            rb_numero_continental=rb_numero_continental.rename(columns={j:'grupoTax'})
            rb_numero_continental=rb_numero_continental.rename(columns={i:'category'})    

            ## Número de registros por grupo taxonómico y categoría temática para salobres
            rb_num_salobre = registros_salobres.groupby([j,i,'isBrackish'])[i].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            rb_num_salobre['thematic']=i
            rb_num_salobre['taxonRank']=j
            rb_num_salobre=rb_num_salobre.rename(columns={j:'grupoTax'})
            rb_num_salobre=rb_num_salobre.rename(columns={i:'category'})    

            ##Agrupar las cifras marinas, continentales y salobres
            rb_taxon_categoria=pd.merge(rb_numero,rb_numero_continental, on=['taxonRank','grupoTax','category','thematic'],how='left').merge(rb_numero_marino, on=['taxonRank','grupoTax','category','thematic'],how='left').merge(rb_num_salobre, on=['taxonRank','grupoTax','category','thematic'],how='left') 
            rb_taxon_categoria_total=pd.concat([rb_taxon_categoria_total,rb_taxon_categoria])

        if tipo =='MCDM' or tipo =='DCDM':
            ## Número de registros por grupo taxonómico y categoría temática para marinos
            rb_numero_marino = registros_marinos.groupby([region,j,i,'isMarine'])[i].count().to_frame(name = 'registrosMarinos' ).reset_index().drop(['isMarine'], axis=1)
            rb_numero_marino['thematic']=i
            rb_numero_marino['taxonRank']=j
            rb_numero_marino=rb_numero_marino.rename(columns={j:'grupoTax'})
            rb_numero_marino=rb_numero_marino.rename(columns={i:'category'})

            ## Número de registros por grupo taxonómico y categoría temática para continentales
            rb_numero_continental = registros_continentales.groupby([region,j,i,'isTerrestrial'])[i].count().to_frame(name = 'registrosContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            rb_numero_continental['thematic']=i
            rb_numero_continental['taxonRank']=j
            rb_numero_continental=rb_numero_continental.rename(columns={j:'grupoTax'})
            rb_numero_continental=rb_numero_continental.rename(columns={i:'category'})    

            ## Número de registros por grupo taxonómico y categoría temática para salobres
            rb_num_salobre = registros_salobres.groupby([region,j,i,'isBrackish'])[i].count().to_frame(name = 'registrosSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            rb_num_salobre['thematic']=i
            rb_num_salobre['taxonRank']=j
            rb_num_salobre=rb_num_salobre.rename(columns={j:'grupoTax'})
            rb_num_salobre=rb_num_salobre.rename(columns={i:'category'})    

            ##Agrupar las cifras marinas, continentales y salobres
            rb_taxon_categoria=pd.merge(rb_numero,rb_numero_continental, on=[region,'taxonRank','grupoTax','category','thematic'],how='left').merge(rb_numero_marino, on=[region,'taxonRank','grupoTax','category','thematic'],how='left').merge(rb_num_salobre, on=[region,'taxonRank','grupoTax','category','thematic'],how='left') 
            rb_taxon_categoria_total=pd.concat([rb_taxon_categoria_total,rb_taxon_categoria])

        if tipo =='MSDM' or tipo =='DSDM' or tipo =='CSDM':
            rb_taxon_categoria_total=pd.concat([rb_taxon_categoria_total,rb_numero])

               
        ## Especies 
        if tipo =='MSDM' or tipo =='DSDM' or tipo =='MCDM' or tipo =='DCDM':
            ## Número de especies por grupo taxonómico general
            spp_numero=registros.drop_duplicates([region, j,'species'])##
            sp_numero = spp_numero.groupby([j,region])['species'].count().to_frame(name = 'especies' ).reset_index()
            sp_numero['taxonRank']=j
            sp_numero=sp_numero.rename(columns={j:'grupoTax'})
        
        if tipo =='CSDM' or tipo =='CCDM':
            ## Número de especies por grupo taxonómico general
            sp_numero = variable_conteos.groupby(j)['gbifID'].count().to_frame(name = 'especies' ).reset_index()
            sp_numero['taxonRank']=j
            sp_numero=sp_numero.rename(columns={j:'grupoTax'})

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='CCDM':
            ## Número de especies por grupo taxonómico para marinos
            spp_numero=registros_marinos.drop_duplicates([j,'species'])##
            sp_numero_marino = spp_numero.groupby([j,'isMarine'])['species'].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            #sp_numero_marino = variable_conteos_marinos.groupby([j,'isMarine'])['gbifID'].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            sp_numero_marino['taxonRank']=j
            sp_numero_marino=sp_numero_marino.rename(columns={j:'grupoTax'})        

            ## Número de especies por grupo taxonómico para continentales
            spp_numero=registros_continentales.drop_duplicates([j,'species'])##
            sp_numero_continental = spp_numero.groupby([j,'isTerrestrial'])['species'].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            #sp_numero_continental = variable_conteos_continentales.groupby([j,'isTerrestrial'])['gbifID'].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            sp_numero_continental['taxonRank']=j
            sp_numero_continental=sp_numero_continental.rename(columns={j:'grupoTax'})   

            ## Número de especies por grupo taxonómico para salobres
            spp_numero=registros_salobres.drop_duplicates([j,'species'])##
            sp_numero_salobre = spp_numero.groupby([j,'isBrackish'])['species'].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            #sp_numero_salobre = variable_conteos_salobres.groupby([j,'isBrackish'])['gbifID'].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            sp_numero_salobre['taxonRank']=j
            sp_numero_salobre=sp_numero_salobre.rename(columns={j:'grupoTax'})   

            ##Agrupar las cifras marinas, continentales y salobres
            sp_taxon=pd.merge(sp_numero,sp_numero_continental, on=['taxonRank','grupoTax'],how='left').merge(sp_numero_marino, on=['taxonRank','grupoTax'],how='left').merge(sp_numero_salobre, on=['taxonRank','grupoTax'],how='left')  
            sp_taxon_total=pd.concat([sp_taxon_total,sp_taxon])   

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='MCDM' or tipo =='DCDM':
            ## Número de especies por grupo taxonómico para marinos
            spp_numero=registros_marinos.drop_duplicates([region,j,'species'])##
            sp_numero_marino = spp_numero.groupby([region,j,'isMarine'])['species'].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            #sp_numero_marino = variable_conteos_marinos.groupby([region,j,'isMarine'])['gbifID'].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            sp_numero_marino['taxonRank']=j
            sp_numero_marino=sp_numero_marino.rename(columns={j:'grupoTax'})        

            ## Número de especies por grupo taxonómico para continentales
            spp_numero=registros_continentales.drop_duplicates([region,j,'species'])##
            sp_numero_continental = spp_numero.groupby([region,j,'isTerrestrial'])['species'].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            #sp_numero_continental = variable_conteos_continentales.groupby([region,j,'isTerrestrial'])['gbifID'].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            sp_numero_continental['taxonRank']=j
            sp_numero_continental=sp_numero_continental.rename(columns={j:'grupoTax'})   

            ## Número de especies por grupo taxonómico para salobres
            spp_numero=registros_salobres.drop_duplicates([region,j,'species'])##
            sp_numero_salobre = spp_numero.groupby([region,j,'isBrackish'])['gbifID'].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            #sp_numero_salobre = variable_conteos_salobres.groupby([region,j,'isBrackish'])['gbifID'].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            sp_numero_salobre['taxonRank']=j
            sp_numero_salobre=sp_numero_salobre.rename(columns={j:'grupoTax'})   

            ##Agrupar las cifras marinas, continentales y salobres
            sp_taxon=pd.merge(sp_numero,sp_numero_continental, on=[region,'taxonRank','grupoTax'],how='left').merge(sp_numero_marino, on=[region,'taxonRank','grupoTax'],how='left').merge(sp_numero_salobre, on=[region,'taxonRank','grupoTax'],how='left')  
            sp_taxon_total=pd.concat([sp_taxon_total,sp_taxon])   
        
        if tipo =='MSDM' or tipo =='DSDM' or tipo =='CSDM':
            sp_taxon_total=pd.concat([sp_taxon_total,sp_numero])

        
        if tipo =='MSDM' or tipo =='DSDM' or tipo =='MCDM' or tipo =='DCDM':
            ## Número de especies por grupo taxonómico, categoría y temática, general
            spp_numero=registros.drop_duplicates([region,j,'species'])
            sp_numero = spp_numero.groupby([j,region])[i].count().to_frame(name = 'especies' ).reset_index()
            sp_numero['thematic']=i
            sp_numero['taxonRank']=j
            sp_numero=sp_numero.rename(columns={j:'grupoTax'})
            sp_numero=sp_numero.rename(columns={i:'category'})

        if tipo =='CSDM' or tipo =='CCDM':
            ## Número de especies por grupo taxonómico, categoría y temática, general
            sp_numero = variable_conteos.groupby(j)[i].count().to_frame(name = 'especies' ).reset_index()
            sp_numero['thematic']=i
            sp_numero['taxonRank']=j
            sp_numero=sp_numero.rename(columns={j:'grupoTax'})
            sp_numero=sp_numero.rename(columns={i:'category'})

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='CCDM':
            ## Número de especies por grupo taxonómico, categoría y temática para marinos
            spp_numero=registros_marinos.drop_duplicates([j,'species'])##
            sp_numero_marino = spp_numero.groupby([j,'isMarine'])[i].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            #sp_numero_marino = variable_conteos_marinos.groupby([j,'isMarine'])[i].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            sp_numero_marino['thematic']=i
            sp_numero_marino['taxonRank']=j
            sp_numero_marino=sp_numero_marino.rename(columns={j:'grupoTax'})
            sp_numero_marino=sp_numero_marino.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría y temática para continentales
            spp_numero=registros_continentales.drop_duplicates([j,'species'])##
            sp_numero_continental = spp_numero.groupby([j,'isTerrestrial'])[i].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            #sp_numero_continental = variable_conteos_continentales.groupby([j,'isTerrestrial'])[i].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            sp_numero_continental['thematic']=i
            sp_numero_continental['taxonRank']=j
            sp_numero_continental=sp_numero_continental.rename(columns={j:'grupoTax'})
            sp_numero_continental=sp_numero_continental.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría y temática para salobres
            spp_numero=registros_salobres.drop_duplicates([j,'species'])##
            sp_numero_salobre = spp_numero.groupby([j,'isBrackish'])[i].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            #sp_numero_salobre = variable_conteos_salobres.groupby([j,'isBrackish'])[i].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            sp_numero_salobre['thematic']=i
            sp_numero_salobre['taxonRank']=j
            sp_numero_salobre=sp_numero_salobre.rename(columns={j:'grupoTax'})
            sp_numero_salobre=sp_numero_salobre.rename(columns={i:'category'})

            ##Agrupar las cifras marinas, continentales y salobres
            sp_taxon_tematica=pd.merge(sp_numero,sp_numero_continental, on=['taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_marino, on=['taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_salobre, on=['taxonRank','thematic','grupoTax'],how='left')                
            sp_taxon_tematica_total=pd.concat([sp_taxon_tematica_total,sp_taxon_tematica]) 

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='MCDM' or tipo =='DCDM':
            ## Número de especies por grupo taxonómico, categoría y temática para marinos
            spp_numero=registros_marinos.drop_duplicates([region,j,'species'])##
            sp_numero_marino = spp_numero.groupby([region,j,'isMarine'])[i].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            #sp_numero_marino = variable_conteos_marinos.groupby([region,j,'isMarine'])[i].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            sp_numero_marino['thematic']=i
            sp_numero_marino['taxonRank']=j
            sp_numero_marino=sp_numero_marino.rename(columns={j:'grupoTax'})
            sp_numero_marino=sp_numero_marino.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría y temática para continentales
            spp_numero=registros_continentales.drop_duplicates([region,j,'species'])##
            sp_numero_continental = spp_numero.groupby([region,j,'isTerrestrial'])[i].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            #sp_numero_continental = variable_conteos_continentales.groupby([region,j,'isTerrestrial'])[i].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            sp_numero_continental['thematic']=i
            sp_numero_continental['taxonRank']=j
            sp_numero_continental=sp_numero_continental.rename(columns={j:'grupoTax'})
            sp_numero_continental=sp_numero_continental.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría y temática para salobres
            spp_numero=registros_salobres.drop_duplicates([region,j,'species'])##
            sp_numero_salobre = spp_numero.groupby([region,j,'isBrackish'])[i].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            #sp_numero_salobre = variable_conteos_salobres.groupby([region,j,'isBrackish'])[i].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            sp_numero_salobre['thematic']=i
            sp_numero_salobre['taxonRank']=j
            sp_numero_salobre=sp_numero_salobre.rename(columns={j:'grupoTax'})
            sp_numero_salobre=sp_numero_salobre.rename(columns={i:'category'})

            ##Agrupar las cifras marinas, continentales y salobres
            sp_taxon_tematica=pd.merge(sp_numero,sp_numero_continental, on=[region,'taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_marino, on=[region,'taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_salobre, on=[region,'taxonRank','thematic','grupoTax'],how='left')                
            sp_taxon_tematica_total=pd.concat([sp_taxon_tematica_total,sp_taxon_tematica]) 

        if tipo =='MSDM' or tipo =='DSDM' or tipo =='CSDM':
            sp_taxon_tematica_total=pd.concat([sp_taxon_tematica_total,sp_numero]) 

        ## Número de especies por grupo taxonómico, categoría temática y temática, general
        if tipo =='MSDM' or tipo =='DSDM' or tipo =='MCDM' or tipo =='DCDM':
            spp_numero=registros.drop_duplicates([region,j,'species'])##
            sp_numero = spp_numero.groupby([j,i,region])[i].count().to_frame(name = 'especies' ).reset_index()
            sp_numero['thematic']=i
            sp_numero['taxonRank']=j
            sp_numero=sp_numero.rename(columns={j:'grupoTax'})
            sp_numero=sp_numero.rename(columns={i:'category'})
    
        if tipo =='CSDM' or tipo =='CCDM':
            ## Número de especies por grupo taxonómico, categoría temática y temática, general
            sp_numero = variable_conteos.groupby([j,i])[i].count().to_frame(name = 'especies' ).reset_index()
            sp_numero['thematic']=i
            sp_numero['taxonRank']=j
            sp_numero=sp_numero.rename(columns={j:'grupoTax'})
            sp_numero=sp_numero.rename(columns={i:'category'})

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='CCDM':
            ## Número de especies por grupo taxonómico, categoría temática y temática, general para marinos
            spp_numero=registros_marinos.drop_duplicates([j,'species'])##
            sp_numero_marino = spp_numero.groupby([j,i,'isMarine'])[i].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            #sp_numero_marino = variable_conteos_marinos.groupby([j,i,'isMarine'])[i].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            sp_numero_marino['thematic']=i
            sp_numero_marino['taxonRank']=j
            sp_numero_marino=sp_numero_marino.rename(columns={j:'grupoTax'})
            sp_numero_marino=sp_numero_marino.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría temática y temática, general para continentales
            spp_numero=registros_continentales.drop_duplicates([j,'species'])##
            sp_numero_continental = spp_numero.groupby([j,i,'isTerrestrial'])[i].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            #sp_numero_continental = variable_conteos_continentales.groupby([j,i,'isTerrestrial'])[i].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            sp_numero_continental['thematic']=i
            sp_numero_continental['taxonRank']=j
            sp_numero_continental=sp_numero_continental.rename(columns={j:'grupoTax'})
            sp_numero_continental=sp_numero_continental.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría temática y temática, general para salobres
            spp_numero=registros_salobres.drop_duplicates([j,'species'])##
            sp_numero_salobre = spp_numero.groupby([j,i,'isBrackish'])[i].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            #sp_numero_salobre = variable_conteos_salobres.groupby([j,i,'isBrackish'])[i].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            sp_numero_salobre['thematic']=i
            sp_numero_salobre['taxonRank']=j
            sp_numero_salobre=sp_numero_salobre.rename(columns={j:'grupoTax'})
            sp_numero_salobre=sp_numero_salobre.rename(columns={i:'category'})

            ##Agrupar las cifras marinas, continentales y salobres
            sp_taxon_categoria=pd.merge(sp_numero,sp_numero_continental, on=['category','taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_marino, on=['category','taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_salobre, on=['category','taxonRank','thematic','grupoTax'],how='left')  
            sp_taxon_categoria_total=pd.concat([sp_taxon_categoria_total,sp_taxon_categoria])

        ##Condicional para registros marinos, continentales y salobres
        if tipo =='MCDM' or tipo =='DCDM':
            ## Número de especies por grupo taxonómico, categoría temática y temática, general para marinos
            spp_numero=registros_marinos.drop_duplicates([region,j,'species'])##
            sp_numero_marino = spp_numero.groupby([region,j,i,'isMarine'])[i].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            #sp_numero_marino = variable_conteos_marinos.groupby([region,j,i,'isMarine'])[i].count().to_frame(name = 'especiesMarinas' ).reset_index().drop(['isMarine'], axis=1)
            sp_numero_marino['thematic']=i
            sp_numero_marino['taxonRank']=j
            sp_numero_marino=sp_numero_marino.rename(columns={j:'grupoTax'})
            sp_numero_marino=sp_numero_marino.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría temática y temática, general para continentales
            spp_numero=registros_continentales.drop_duplicates([region,j,'species'])##
            sp_numero_continental = spp_numero.groupby([region,j,i,'isTerrestrial'])[i].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            #sp_numero_continental = variable_conteos_continentales.groupby([region,j,i,'isTerrestrial'])[i].count().to_frame(name = 'especiesContinentales' ).reset_index().drop(['isTerrestrial'], axis=1)
            sp_numero_continental['thematic']=i
            sp_numero_continental['taxonRank']=j
            sp_numero_continental=sp_numero_continental.rename(columns={j:'grupoTax'})
            sp_numero_continental=sp_numero_continental.rename(columns={i:'category'})

            ## Número de especies por grupo taxonómico, categoría temática y temática, general para salobres
            spp_numero=registros_salobres.drop_duplicates([region,j,'species'])##
            sp_numero_salobre = spp_numero.groupby([region,j,i,'isBrackish'])[i].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            #sp_numero_salobre = variable_conteos_salobres.groupby([region,j,i,'isBrackish'])[i].count().to_frame(name = 'especiesSalobres' ).reset_index().drop(['isBrackish'], axis=1)
            sp_numero_salobre['thematic']=i
            sp_numero_salobre['taxonRank']=j
            sp_numero_salobre=sp_numero_salobre.rename(columns={j:'grupoTax'})
            sp_numero_salobre=sp_numero_salobre.rename(columns={i:'category'})

            ##Agrupar las cifras marinas, continentales y salobres
            sp_taxon_categoria=pd.merge(sp_numero,sp_numero_continental, on=[region,'category','taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_marino, on=[region,'category','taxonRank','thematic','grupoTax'],how='left').merge(sp_numero_salobre, on=[region,'category','taxonRank','thematic','grupoTax'],how='left')  
            sp_taxon_categoria_total=pd.concat([sp_taxon_categoria_total,sp_taxon_categoria])

        if tipo =='MSDM' or tipo =='DSDM' or tipo =='CSDM':
            sp_taxon_categoria_total=pd.concat([sp_taxon_categoria_total,sp_numero])


##Fin del loop para realizar conteos

if tipo =='MCDM' or tipo =='DCDM' or tipo =='MSDM' or tipo =='DSDM':
    ## Agrupar la información: Número de registros y especies por grupo taxonómico en la temática
    taxon_tematica=pd.merge(rb_taxon_tematica_total,sp_taxon_tematica_total,on=['taxonRank','thematic','grupoTax',region],how='left')
else:
    ## Agrupar la información: Número de registros y especies por grupo taxonómico en la temática
    taxon_tematica=pd.merge(rb_taxon_tematica_total,sp_taxon_tematica_total,on=['taxonRank','thematic','grupoTax'],how='left')

if tipo =='MCDM' or tipo =='DCDM' or tipo =='MSDM' or tipo =='DSDM':
    ## Agrupar la información: Número de registros y especies por grupo taxonómico en la temática y categoría
    taxon_categoria=pd.merge(rb_taxon_categoria_total,sp_taxon_categoria_total,on=['taxonRank','thematic','grupoTax','category',region],how='left')
else:
 ## Agrupar la información: Número de registros y especies por grupo taxonómico en la temática y categoría
    taxon_categoria=pd.merge(rb_taxon_categoria_total,sp_taxon_categoria_total,on=['taxonRank','thematic','grupoTax','category'],how='left')    
## Guardar archivos de resultado sobre los cuales se puede recalcular cifras según los grupos biológicos
#rb_taxon_total.to_csv('registrosBiologicosTaxonTotal.tsv',sep='\t', index=False )
#sp_taxon_total.to_csv('especiesTaxonTotal.tsv',sep='\t', index=False )
#rb_taxon_tematica_total.to_csv('registrosBiologicosTaxonTematicas.tsv',sep='\t', index=False )
#sp_taxon_tematica_total.to_csv('especiesTaxonTematicas.tsv',sep='\t', index=False )
#rb_taxon_categoria_total.to_csv('registrosBiologicosTaxonCategoriasTotal.tsv',sep='\t', index=False )
#sp_taxon_categoria_total.to_csv('especiesTaxonCategoriasTotal.tsv',sep='\t', index=False )             



   
#sp_tematica_total.to_excel('taxon_tematica_cifras.xlsx', sheet_name='taxon_tematica_cifras', index=False )

##-----------------------------Organización en un único dataframe de las cifras temáticas totales------------------------------##
'''
Se crea el archivo final con el número de especies y registros para todas las temáticas y sus respectivas categorías 


##Unir los conjuntos de datos teniendo en cuenta los campos de thematic y category
tematica_cifras=pd.merge(rb_tematica_total,sp_tematica_total, on=['thematic','category',region],how='left') 

##Exportar el archivo a formato tsv o xlsx

tematica_cifras.columns=['categoria','departamento','registros','tematica','especies']
tematica_cifras=tematica_cifras[['tematica','departamento','categoria','registros','especies']]
    
tematica_cifras.to_csv(nombre+'cifrasTematicasTotales.tsv',sep='\t')
tematica_cifras.to_excel(nombre+'cifrasTematicasTotales.xlsx', sheet_name='tematica_cifras', index=False)
'''
##-----------------------------Organización en un único dataframe de las cifras geográficas------------------------------------##
'''
En esta sección se crea el conjunto de datos final para la obtención de las cifras temáticas para cada una de las entidades geográficas
Dentro de los nombres se encuentran las siguientes convenciones:
rb= Registros biologicos
sp= especies
geo= geografía
'''


#geo_tematica_rb_total.to_csv('geo_tematica_rb_total.tsv',sep='\t', index=False )
## Reorganizar los conjuntos de datos


geo_tematica_rb_total=geo_tematica_rb_total.pivot(region,'thematic')

geo_tematica_sp_total=geo_tematica_sp_total.pivot(region,'thematic')

##Eliminar columna 'thematic' y reordenar
geo_categoria_rb_total=geo_categoria_rb_total.drop(['thematic'],axis=1)
geo_categoria_rb_total=geo_categoria_rb_total.pivot(region,'category')
geo_categoria_sp_total=geo_categoria_sp_total.drop(['thematic'],axis=1)
geo_categoria_sp_total=geo_categoria_sp_total.pivot(region,'category')

##Conjunto de datos final para cifras geográficas

geografia_total=pd.merge(geo_rb_total,geo_sp_total,on=region,how='left').merge(geo_tematica_rb_total,on=region,how='left').merge(geo_tematica_sp_total,on=region,how='left').merge(geo_categoria_rb_total,on=region,how='left').merge(geo_categoria_sp_total,on=region,how='left')


#geografia_total['especies_exoticas_total']=geografia_total[('especies', 'especies_exoticas')].astype("float")+geografia_total[('especies', 'especies_invasoras')].astype("float")+geografia_total[('especies', 'especies_exotica_riesgo_invasion')].astype("float")
#geografia_total['especies_exoticas_total']=geografia_total[[('especies', 'especies_exoticas'),('especies', 'especies_invasoras'),('especies', 'especies_exotica_riesgo_invasion')]].sum(numeric_only=True,skipna=True)
#geografia_total['registros_exoticas_total']=geografia_total[('registros', 'especies_exoticas')]+geografia_total[('registros', 'especies_invasoras')]+geografia_total[('registros', 'especies_exotica_riesgo_invasion')]
#geografia_total['especies_amenazadas_global_total']=geografia_total[('especies', 'EN_IUCN')]+geografia_total[('especies', 'CR_IUCN')]+geografia_total[('especies', 'VU_IUCN')]
#geografia_total['registros_amenazadas_global_total']=geografia_total[('registros', 'EN_IUCN')]+geografia_total[('registros', 'CR_IUCN')]+geografia_total[('registros', 'VU_IUCN')]
#geografia_total.to_excel('geografia_total.xlsx', sheet_name='cifrasGeográficas', index=False )
geografia_total['especies_exoticas_total']=''
geografia_total['registros_exoticas_total']=''
geografia_total['especies_amenazadas_global_total']=''
geografia_total['registros_amenazadas_global_total']=''

if tipo =='MCDM' or tipo =='DCDM' or tipo =='CCDM':
    geografia_total['especies_continentales_exoticas_total']=geografia_total[('especiesContinentales', 'especies_exoticas')]+geografia_total[('especiesContinentales', 'especies_invasoras')]+geografia_total[('especiesContinentales', 'especies_exotica_riesgo_invasion')]
    geografia_total['registros_continentales_exoticas_total']=geografia_total[('registrosContinentales', 'especies_exoticas')]+geografia_total[('registrosContinentales', 'especies_invasoras')]+geografia_total[('registrosContinentales', 'especies_exotica_riesgo_invasion')]
    geografia_total['especies_marinas_exoticas_total']=geografia_total[('especiesMarinas', 'especies_exoticas')]+geografia_total[('especiesMarinas', 'especies_invasoras')]+geografia_total[('especiesMarinas', 'especies_exotica_riesgo_invasion')]
    geografia_total['registros_marinas_exoticas_total']=geografia_total[('registrosMarinos', 'especies_exoticas')]+geografia_total[('registrosMarinos', 'especies_invasoras')]+geografia_total[('registrosMarinos', 'especies_exotica_riesgo_invasion')]
    geografia_total['especies_salobres_exoticas_total']=geografia_total[('especiesSalobres', 'especies_exoticas')]+geografia_total[('especiesSalobres', 'especies_invasoras')]+geografia_total[('especiesSalobres', 'especies_exotica_riesgo_invasion')]
    geografia_total['registros_salobres_exoticas_total']=geografia_total[('registrosSalobres', 'especies_exoticas')]+geografia_total[('registrosSalobres', 'especies_invasoras')]+geografia_total[('registrosSalobres', 'especies_exotica_riesgo_invasion')]

    geografia_total[('especies_marinas_amenazadas_global_total')]=geografia_total[('especiesMarinas', 'EN_IUCN')]+geografia_total[('especiesMarinas', 'CR_IUCN')]+geografia_total[('especiesMarinas', 'VU_IUCN')]
    geografia_total[('registros_marinas_amenazadas_global_total')]=geografia_total[('registrosMarinos', 'EN_IUCN')]+geografia_total[('registrosMarinos', 'CR_IUCN')]+geografia_total[('registrosMarinos', 'VU_IUCN')]
    geografia_total[('especies_continentales_amenazadas_global_total')]=geografia_total[('especiesContinentales', 'EN_IUCN')]+geografia_total[('especiesContinentales', 'CR_IUCN')]+geografia_total[('especiesContinentales', 'VU_IUCN')]
    geografia_total[('registros_continentales_amenazadas_global_total')]=geografia_total[('registrosContinentales', 'EN_IUCN')]+geografia_total[('registrosContinentales', 'CR_IUCN')]+geografia_total[('registrosContinentales', 'VU_IUCN')]
    geografia_total[('especies_salobres_amenazadas_global_total')]=geografia_total[('especiesSalobres', 'EN_IUCN')]+geografia_total[('especiesSalobres', 'CR_IUCN')]+geografia_total[('especiesSalobres', 'VU_IUCN')]
    geografia_total[('registros_salobres_amenazadas_global_total')]=geografia_total[('registrosSalobres', 'EN_IUCN')]+geografia_total[('registrosSalobres', 'CR_IUCN')]+geografia_total[('registrosSalobres', 'VU_IUCN')]

if tipo =='DCDM' or tipo =='DSDM':
    geografia_total=pd.merge(geografia_total,estimadas_dept,left_on='slug',right_on='departamento',how='left')

geografia_total['fecha_corte']=fecha_corte
geografia_total=geografia_total.replace(np.nan,'-',regex=True)

##---------------------Transformación cifras por categoría taxonómica a cifras por grupos biológicos----------------------------##
'''
Se carga el archivo guía de grupos biologicos y con este se calculan las cifras tematicas para cada uno de estos
Dentro de los nombres se encuentran las siguientes convenciones:
rb= Registros biologicos
sp= especies
'''

## Resumen cifras registros por grupo biológico general
rb_grupos_biologicos=pd.merge(rb_taxon_total,grupos_biologicos, on=['grupoTax','taxonRank'],how='left').drop_duplicates() 

if tipo =='CCDM':
    ##Resumen cifras registros por grupo biológico para marinos, continentales y salobres
    rb_grupos_biologicos=rb_grupos_biologicos[['registros','registrosContinentales','registrosMarinos','registrosSalobres','grupoTax','grupo_id']]

if tipo =='MCDM' or tipo =='DCDM':
    ##Resumen cifras registros por grupo biológico para marinos, continentales y salobres
    rb_grupos_biologicos=rb_grupos_biologicos[['registros',region,'registrosContinentales','registrosMarinos','registrosSalobres','grupoTax','grupo_id']]

if tipo =='CSDM':
    ## Seleccionar las columnas deseadas 
    rb_grupos_biologicos=rb_grupos_biologicos[['registros','grupoTax','grupo_id']]

if tipo =='MSDM' or tipo =='DSDM':
    ## Seleccionar las columnas deseadas 
    rb_grupos_biologicos=rb_grupos_biologicos[['registros',region,'grupoTax','grupo_id']]

if tipo =='CCDM' or tipo =='CSDM':
    ## Agrupar registros por grupo biológico
    rb_cifras=rb_grupos_biologicos.groupby('grupo_id').sum().reset_index()
    
if tipo =='MSDM' or tipo =='DSDM' or tipo =='MCDM' or tipo =='DCDM':
    ## Agrupar registros por grupo biológico
    rb_cifras=rb_grupos_biologicos.groupby(['grupo_id',region]).sum().reset_index()

## Resumen cifras especies por grupo biológico general
sp_grupos_biologicos=pd.merge(sp_taxon_total,grupos_biologicos, on=['grupoTax','taxonRank'],how='left').drop_duplicates()

## Condicional para registros marinos, continentales y salobres
if tipo =='CCDM':
    ##Resumen cifras especies por grupo biológico para marinos, continentales y salobres
    sp_grupos_biologicos=sp_grupos_biologicos[['especies','especiesMarinas','especiesContinentales','especiesSalobres','grupoTax','grupo_id']]
if tipo =='CSDM':
    ##Seleccionar las columnas deseadas y agrupar por grupo biológico
    sp_grupos_biologicos=sp_grupos_biologicos[['especies','grupoTax','grupo_id']]

if tipo =='MCDM' or tipo =='DCDM':
    ##Resumen cifras especies por grupo biológico para marinos, continentales y salobres
    sp_grupos_biologicos=sp_grupos_biologicos[['especies',region,'especiesMarinas','especiesContinentales','especiesSalobres','grupoTax','grupo_id']]
if tipo =='MSDM' or tipo =='DSDM': 
    ##Seleccionar las columnas deseadas y agrupar por grupo biológico
    sp_grupos_biologicos=sp_grupos_biologicos[['especies',region,'grupoTax','grupo_id']]

if tipo =='MSDM' or tipo =='DSDM' or tipo =='MCDM' or tipo =='DCDM':
    sp_cifras=sp_grupos_biologicos.groupby(['grupo_id',region]).sum().reset_index()
    
    ## Unión de las cifras de registros y especies para cada grupo biológico
    taxon_cifras=pd.merge(rb_cifras,sp_cifras, on=['grupo_id',region],how='left') 

if tipo =='CCDM' or tipo =='CSDM':
    sp_cifras=sp_grupos_biologicos.groupby('grupo_id').sum().reset_index()

    ## Unión de las cifras de registros y especies para cada grupo biológico
    taxon_cifras=pd.merge(rb_cifras,sp_cifras, on=['grupo_id'],how='left') 

## Resumen cifras por grupo biológico y temática general
taxon_tematica_cifras=pd.merge(taxon_tematica,grupos_biologicos, on=['grupoTax','taxonRank'],how='left').drop_duplicates()

if tipo =='MSDM' or tipo =='DSDM':
    ## Seleccionar las columnas deseadas y agrupar por grupo biológico y temática
    taxon_tematica_cifras=taxon_tematica_cifras[['registros','especies','grupo_id','thematic',region]]
## Condicional para registros marinos, continentales y salobres
if tipo =='MCDM' or tipo =='DCDM':
    ##Resumen cifras por grupo biológico y temática para marinos, continentales y salobres
    taxon_tematica_cifras=taxon_tematica_cifras[['registros','registrosContinentales','registrosMarinos','registrosSalobres','especies','especiesMarinas','especiesContinentales','especiesSalobres','grupo_id','thematic',region]]
## Condicional para registros marinos, continentales y salobres
if tipo =='CCDM':
    ##Resumen cifras por grupo biológico y temática para marinos, continentales y salobres
    taxon_tematica_cifras=taxon_tematica_cifras[['registros','registrosContinentales','registrosMarinos','registrosSalobres','especies','especiesMarinas','especiesContinentales','especiesSalobres','grupo_id','thematic']]
if tipo =='CSDM':
    ## Seleccionar las columnas deseadas y agrupar por grupo biológico y temática
    taxon_tematica_cifras=taxon_tematica_cifras[['registros','especies','grupo_id','thematic']]


if tipo =='MSDM' or tipo =='DSDM' or tipo =='MCDM' or tipo =='DCDM':
    ## Agrupar las cifras por grupo biológico y temática, y reordenar el conjunto de datos
    taxon_tematica_cifras=taxon_tematica_cifras.groupby(['grupo_id','thematic',region]).sum().reset_index()
    taxon_tematica_cifras=taxon_tematica_cifras.pivot(index=['grupo_id',region], columns=['thematic'])

if tipo =='CCDM' or tipo =='CSDM':
    ## Agrupar las cifras por grupo biológico y temática, y reordenar el conjunto de datos
    taxon_tematica_cifras=taxon_tematica_cifras.groupby(['grupo_id','thematic']).sum().reset_index()
    taxon_tematica_cifras=taxon_tematica_cifras.pivot('grupo_id','thematic').fillna('-') 

    
## Resumen cifras de especies y registros por grupo biológico y por categoría dentro de cada temática general
taxon_categoria_cifras=pd.merge(taxon_categoria,grupos_biologicos, on=['grupoTax','taxonRank'],how='left').drop_duplicates()


##Condicional para registros marinos, continentales y salobres
if tipo =='CCDM':
    #Resumen cifras de especies y registros por grupo biológico y por categoría dentro de cada temática para marinos, continentales y salobres
    taxon_categoria_cifras=taxon_categoria_cifras[['registros','registrosContinentales','registrosMarinos','registrosSalobres','especies','especiesMarinas','especiesContinentales','especiesSalobres','grupo_id','thematic','category']]
if tipo =='CSDM':
    ##Seleccionar las columnas deseadas 
    taxon_categoria_cifras=taxon_categoria_cifras[['registros','especies','grupo_id','thematic','category']]
if tipo =='MSDM' or tipo =='DSDM':
    ##Seleccionar las columnas deseadas 
    taxon_categoria_cifras=taxon_categoria_cifras[['registros','especies',region,'grupo_id','thematic','category']]
if tipo =='MCDM' or tipo =='DCDM':
    taxon_categoria_cifras=taxon_categoria_cifras[['registros','registrosContinentales','registrosMarinos','registrosSalobres','especies','especiesMarinas','especiesContinentales','especiesSalobres','grupo_id','thematic','category',region]]


if tipo =='MSDM' or tipo =='DSDM' or tipo =='MCDM' or tipo =='DCDM':
    ## Agrupar por grupo biológico y temática, eliminar la columna temática y reordenar el conjunto de datos   
    taxon_categoria_cifras=taxon_categoria_cifras.groupby(['grupo_id','thematic','category',region]).sum().reset_index()
    taxon_categoria_cifras=taxon_categoria_cifras.drop(['thematic'],axis=1).pivot(index=['grupo_id',region], columns=['category'])
    
    ##Unión de las cifras de especies y registros para los dos grupos biológicos
    grupos_biologicos_total=pd.merge(taxon_cifras,taxon_tematica_cifras,on=['grupo_id',region],how='left').merge(taxon_categoria_cifras,on=['grupo_id',region],how='left') ###FinalBiologicalGroupFile

if tipo =='CCDM' or tipo =='CSDM':
    ## Agrupar por grupo biológico y temática, eliminar la columna temática y reordenar el conjunto de datos   
    taxon_categoria_cifras=taxon_categoria_cifras.groupby(['grupo_id','thematic','category']).sum().reset_index()
    taxon_categoria_cifras=taxon_categoria_cifras.drop(['thematic'],axis=1).pivot('grupo_id','category')
    
    ##Unión de las cifras de especies y registros para los dos grupos biológicos
    grupos_biologicos_total=pd.merge(taxon_cifras,taxon_tematica_cifras,on='grupo_id',how='left').merge(taxon_categoria_cifras,on='grupo_id',how='left') ###FinalBiologicalGroupFile
    grupos_biologicos_total['slug_region']='colombia'


grupos_biologicos_total['especies_exoticas_total']=grupos_biologicos_total[('especies', 'especies_exoticas')]+grupos_biologicos_total[('especies', 'especies_invasoras')]+grupos_biologicos_total[('especies', 'especies_exotica_riesgo_invasion')]
grupos_biologicos_total['registros_exoticas_total']=grupos_biologicos_total[('registros', 'especies_exoticas')]+grupos_biologicos_total[('registros', 'especies_invasoras')]+grupos_biologicos_total[('registros', 'especies_exotica_riesgo_invasion')]
grupos_biologicos_total[('especies_amenazadas_global_total')]=grupos_biologicos_total[('especies', 'EN_IUCN')]+grupos_biologicos_total[('especies', 'CR_IUCN')]+grupos_biologicos_total[('especies', 'VU_IUCN')]
grupos_biologicos_total[('registros_amenazadas_global_total')]=grupos_biologicos_total[('registros', 'EN_IUCN')]+grupos_biologicos_total[('registros', 'CR_IUCN')]+grupos_biologicos_total[('registros', 'VU_IUCN')]


if tipo =='MCDM' or tipo =='DCDM' or tipo =='CCDM':
    grupos_biologicos_total['especies_continentales_exoticas_total']=grupos_biologicos_total[('especiesContinentales', 'especies_exoticas')]+grupos_biologicos_total[('especiesContinentales', 'especies_invasoras')]+grupos_biologicos_total[('especiesContinentales', 'especies_exotica_riesgo_invasion')]
    grupos_biologicos_total['registros_continentales_exoticas_total']=grupos_biologicos_total[('registrosContinentales', 'especies_exoticas')]+grupos_biologicos_total[('registrosContinentales', 'especies_invasoras')]+grupos_biologicos_total[('registrosContinentales', 'especies_exotica_riesgo_invasion')]
    grupos_biologicos_total['especies_marinas_exoticas_total']=grupos_biologicos_total[('especiesMarinas', 'especies_exoticas')]+grupos_biologicos_total[('especiesMarinas', 'especies_invasoras')]+grupos_biologicos_total[('especiesMarinas', 'especies_exotica_riesgo_invasion')]
    grupos_biologicos_total['registros_marinas_exoticas_total']=grupos_biologicos_total[('registrosMarinos', 'especies_exoticas')]+grupos_biologicos_total[('registrosMarinos', 'especies_invasoras')]+grupos_biologicos_total[('registrosMarinos', 'especies_exotica_riesgo_invasion')]
    grupos_biologicos_total['especies_salobres_exoticas_total']=grupos_biologicos_total[('especiesSalobres', 'especies_exoticas')]+grupos_biologicos_total[('especiesSalobres', 'especies_invasoras')]+grupos_biologicos_total[('especiesSalobres', 'especies_exotica_riesgo_invasion')]
    grupos_biologicos_total['registros_salobres_exoticas_total']=grupos_biologicos_total[('registrosSalobres', 'especies_exoticas')]+grupos_biologicos_total[('registrosSalobres', 'especies_invasoras')]+grupos_biologicos_total[('registrosSalobres', 'especies_exotica_riesgo_invasion')]

    grupos_biologicos_total[('especies_marinas_amenazadas_global_total')]=grupos_biologicos_total[('especiesMarinas', 'EN_IUCN')]+grupos_biologicos_total[('especiesMarinas', 'CR_IUCN')]+grupos_biologicos_total[('especiesMarinas', 'VU_IUCN')]
    grupos_biologicos_total[('registros_marinas_amenazadas_global_total')]=grupos_biologicos_total[('registrosMarinos', 'EN_IUCN')]+grupos_biologicos_total[('registrosMarinos', 'CR_IUCN')]+grupos_biologicos_total[('registrosMarinos', 'VU_IUCN')]
    grupos_biologicos_total[('especies_continentales_amenazadas_global_total')]=grupos_biologicos_total[('especiesContinentales', 'EN_IUCN')]+grupos_biologicos_total[('especiesContinentales', 'CR_IUCN')]+grupos_biologicos_total[('especiesContinentales', 'VU_IUCN')]
    grupos_biologicos_total[('registros_continentales_amenazadas_global_total')]=grupos_biologicos_total[('registrosContinentales', 'EN_IUCN')]+grupos_biologicos_total[('registrosContinentales', 'CR_IUCN')]+grupos_biologicos_total[('registrosContinentales', 'VU_IUCN')]
    grupos_biologicos_total[('especies_salobres_amenazadas_global_total')]=grupos_biologicos_total[('especiesSalobres', 'EN_IUCN')]+grupos_biologicos_total[('especiesSalobres', 'CR_IUCN')]+grupos_biologicos_total[('especiesSalobres', 'VU_IUCN')]
    grupos_biologicos_total[('registros_salobres_amenazadas_global_total')]=grupos_biologicos_total[('registrosSalobres', 'EN_IUCN')]+grupos_biologicos_total[('registrosSalobres', 'CR_IUCN')]+grupos_biologicos_total[('registrosSalobres', 'VU_IUCN')]

grupos_biologicos_total=grupos_biologicos_total.replace(np.nan,'-',regex=True)

#grupos_biologicos_total.to_excel('grupos_biologicos_total.xlsx', sheet_name='cifrasGruposBiologicos', index=False )

##-----------------------Renombrar y reorganizar las columnas del dataframe de cifras por grupo biológico y geograficas-----------------##
'''
En esta sección se crea una función para renombrar y reorganizar los conjuntos de datos obtenidos para grupos biológicos y cifras geográficas.
Según la información deseada y los campos que se requieran se debe ajustar el diccionario 'names'.
El primera campo corresponde al nombre que tiene la columna en el conjunto de datos y el segundo al nombre que desea que tenga.}
Los nombres que no se encuentren dentro del diccionario pero estan en el conjunto de datos inical no quedaran en el archivo final.
Si se desea modificar el orden de los campos, solo se debe ajustar el orden dentro del diccionario. 
Esta función cuenta con dos parametros de entrada: 
archivo: corresponde al conjunto de datos grupo biológico:grupos_biologicos_total cifras geográficas: geografia_total
tipo: Corresponde a un parametro númerico donde 1: grupo biológico y 2: cifras geográficas. Este parametro permite a la función deteminar el valor
de la primera columna (grupoBio o (county o stateProvince))
'''
##Creación de la función con dos parametros de entrada

def ajuste_nombres (archivo, tipo):

    ## Para grupos biologicos: Si el valor de tipo es 1 asigna el valor de 'grupoBio' a las variables llave y valor
    if tipo== 1:
        llave='grupo_id'
        valor='slug_grupo_biologico'

    ## Para cifras geograficas: Si el valor de tipo es 2 asigna el valor de 'county o stateProvince' a las variables llave y valor    
    if tipo==2:
        llave=region
        valor=region


    ##Diccionario de datos con los nombres provenientes del conjunto de datos y su correspondiente valor ajustado para exportar 
    names= {llave:valor, 
    region:'slug_region',
    'fecha_corte':'fecha_corte',
    'estimada':'especies_region_estimadas',
    'registros':'registros_region_total', 
    'registrosContinentales':'registros_continentales', 
    'registrosMarinos':'registros_marinos', 
    'registrosSalobres':'registros_salobres', 
    'especies':'especies_region_total', 
    'especiesContinentales':'especies_continentales', 
    'especiesMarinas':'especies_marinas', 
    'especiesSalobres':'especies_salobres',    
    ('especies', 'threatStatus_MADS'):'especies_amenazadas_nacional_total', 
    ('especies', 'CR_MADS'):'especies_amenazadas_nacional_cr',
    ('especies', 'EN_MADS'):'especies_amenazadas_nacional_en', 
    ('especies', 'VU_MADS'):'especies_amenazadas_nacional_vu',
    ('registros', 'threatStatus_MADS'):'registros_amenazadas_nacional_total', 
    ('registros', 'CR_MADS'):'registros_amenazadas_nacional_cr', 
    ('registros', 'EN_MADS'):'registros_amenazadas_nacional_en',
    ('registros', 'VU_MADS'):'registros_amenazadas_nacional_vu',  
    ('especies', 'appendixCITES'):'especies_cites_total', 
    ('especies', 'I'):'especies_cites_i', 
    ('especies', 'I/II'):'especies_cites_i_ii',
    ('especies', 'II'):'especies_cites_ii', 
    ('especies', 'III'):'especies_cites_iii', 
    ('registros', 'appendixCITES'):'registros_cites_total', 
    ('registros', 'I'):'registros_cites_i', 
    ('registros', 'I/II'):'registros_cites_i_ii',
    ('registros', 'II'):'registros_cites_ii', 
    ('registros', 'III'):'registros_cites_iii', 
    'especies_exoticas_total':'especies_exoticas_total', 
    ('especies', 'especies_exoticas'):'especies_exoticas', 
    ('especies', 'especies_invasoras'):'especies_invasoras', 
    ('especies', 'especies_exotica_riesgo_invasion'):'especies_exoticas_riesgo_invasion',
    'registros_exoticas_total':'registros_exoticas_total',
    ('registros', 'especies_exoticas'):'registros_exoticas', 
    ('registros', 'especies_invasoras'):'registros_invasoras',
    ('registros', 'especies_exotica_riesgo_invasion'):'registros_exoticas_riesgo_invasion',
    ('especies', 'endemic'):'especies_endemicas', 
    ('especies', 'Migratorio'):'especies_migratorias', 
    ('registros', 'endemic'):'registros_endemicas',     
    ('registros', 'Migratorio'):'registros_migratorias', 
    'especies_amenazadas_global_total':'especies_amenazadas_global_total',
    #('especies', 'threatStatus_UICN'):'especies_amenazadas_global_total',  
    ('especies', 'EX_IUCN'):'especies_amenazadas_global_ex', 
    ('especies', 'EW_IUCN'):'especies_amenazadas_global_ew', 
    ('especies', 'CR_IUCN'):'especies_amenazadas_global_cr', 
    ('especies', 'EN_IUCN'):'especies_amenazadas_global_en', 
    ('especies', 'VU_IUCN'):'especies_amenazadas_global_vu',
    ('especies', 'NT_IUCN'):'especies_amenazadas_global_nt', 
    ('especies', 'LC_IUCN'):'especies_amenazadas_global_lc', 
    ('especies', 'DD_IUCN'):'especies_amenazadas_global_dd',
    ('especies', 'LR/lc_IUCN'):'especies_amenazadas_global_lr_lc', 
    ('especies', 'LR/nt_IUCN'):'especies_amenazadas_global_lr_nt',
    'registros_amenazadas_global_total':'registros_amenazadas_global_total',
    ('registros', 'EX_IUCN'):'registros_amenazadas_global_ex', 
    ('registros', 'EW_IUCN'):'registros_amenazadas_global_ew', 
    ('registros', 'CR_IUCN'):'registros_amenazadas_global_cr', 
    ('registros', 'EN_IUCN'):'registros_amenazadas_global_en', 
    ('registros', 'VU_IUCN'):'registros_amenazadas_global_vu', 
    ('registros', 'NT_IUCN'):'registros_amenazadas_global_nt', 
    ('registros', 'LC_IUCN'):'registros_amenazadas_global_lc',
    ('registros', 'DD_IUCN'):'registros_amenazadas_global_dd',    
    ('registros', 'LR/lc_IUCN'):'registros_amenazadas_global_lr_lc', 
    ('registros', 'LR/nt_IUCN'):'registros_amenazadas_global_lr_nt', 

    
    ('especiesContinentales', 'threatStatus_MADS'):'especies_continentales_amenazadas_nacional_total', 
    ('especiesContinentales', 'CR_MADS'):'especies_continentales_amenazadas_nacional_cr',
    ('especiesContinentales', 'EN_MADS'):'especies_continentales_amenazadas_nacional_en', 
    ('especiesContinentales', 'VU_MADS'):'especies_continentales_amenazadas_nacional_vu',
    ('registrosContinentales', 'threatStatus_MADS'):'registros_continentales_amenazadas_nacional_total', 
    ('registrosContinentales', 'CR_MADS'):'registros_continentales_amenazadas_nacional_cr', 
    ('registrosContinentales', 'EN_MADS'):'registros_continentales_amenazadas_nacional_en',
    ('registrosContinentales', 'VU_MADS'):'registros_continentales_amenazadas_nacional_vu',  
    ('especiesContinentales', 'appendixCITES'):'especies_continentales_cites_total', 
    ('especiesContinentales', 'I'):'especies_continentales_cites_i', 
    ('especiesContinentales', 'I/II'):'especies_continentales_cites_i_ii', #Como es el campo
    ('especiesContinentales', 'II'):'especies_continentales_cites_ii', 
    ('especiesContinentales', 'III'):'especies_continentales_cites_iii', 
    ('registrosContinentales', 'appendixCITES'):'registros_continentales_cites_total', 
    ('registrosContinentales', 'I'):'registros_continentales_cites_i', 
    ('registrosContinentales', 'I/II'):'registros_continentales_cites_i_ii', #Como es el campo
    ('registrosContinentales', 'II'):'registros_continentales_cites_ii', 
    ('registrosContinentales', 'III'):'registros_continentales_cites_iii',    
    'especies_continentales_exoticas_total':'especies_continentales_exoticas_total', 
    ('especiesContinentales', 'especies_exoticas'):'especies_continentales_exoticas', 
    ('especiesContinentales', 'especies_invasoras'):'especies_continentales_invasoras',
    ('especiesContinentales', 'especies_exotica_riesgo_invasion'):'especies_continentales_exoticas_riesgo_invasion', 
    'registros_continentales_exoticas_total':'registros_continentales_exoticas_total', 
    ('registrosContinentales', 'especies_exoticas'):'registros_continentales_exoticas', 
    ('registrosContinentales', 'especies_invasoras'):'registros_continentales_invasoras', 
    ('registrosContinentales', 'especies_exotica_riesgo_invasion'):'registros_continentales_exoticas_riesgo_invasion',    
    ('especiesContinentales', 'endemic'):'especies_continentales_endemicas', 
    ('especiesContinentales', 'Migratorio'):'especies_continentales_migratorias', 
    ('registrosContinentales', 'endemic'):'registros_continentales_endemicas',   
    ('registrosContinentales', 'Migratorio'):'registros_continentales_migratorias',  
    'especies_continentales_amenazadas_global_total':'especies_continentales_amenazadas_global_total',
    #('especiesContinentales', 'threatStatus_UICN'):'especies_continentales_amenazadas_global_total', 
    ('especiesContinentales', 'EX_IUCN'):'especies_continentales_amenazadas_global_ex', 
    ('especiesContinentales', 'EW_IUCN'):'especies_continentales_amenazadas_global_ew', 
    ('especiesContinentales', 'CR_IUCN'):'especies_continentales_amenazadas_global_cr', 
    ('especiesContinentales', 'EN_IUCN'):'especies_continentales_amenazadas_global_en', 
    ('especiesContinentales', 'VU_IUCN'):'especies_continentales_amenazadas_global_vu',
    ('especiesContinentales', 'NT_IUCN'):'especies_continentales_amenazadas_global_nt', 
    ('especiesContinentales', 'LC_IUCN'):'especies_continentales_amenazadas_global_lc', 
    ('especiesContinentales', 'DD_IUCN'):'especies_continentales_amenazadas_global_dd',
    ('especiesContinentales', 'LR/lc_IUCN'):'especies_continentales_amenazadas_global_lr_lc', 
    ('especiesContinentales', 'LR/nt_IUCN'):'especies_continentales_amenazadas_global_lr_nt',   
    'registros_continentales_amenazadas_global_total':'registros_continentales_amenazadas_global_total', 
    ('registrosContinentales', 'EX_IUCN'):'registros_continentales_amenazadas_global_ex', 
    ('registrosContinentales', 'EW_IUCN'):'registros_continentales_amenazadas_global_ew', 
    ('registrosContinentales', 'CR_IUCN'):'registros_continentales_amenazadas_global_cr', 
    ('registrosContinentales', 'EN_IUCN'):'registros_continentales_amenazadas_global_en', 
    ('registrosContinentales', 'VU_IUCN'):'registros_continentales_amenazadas_global_vu', 
    ('registrosContinentales', 'NT_IUCN'):'registros_continentales_amenazadas_global_nt', 
    ('registrosContinentales', 'LC_IUCN'):'registros_continentales_amenazadas_global_lc', 
    ('registrosContinentales', 'DD_IUCN'):'registros_continentales_amenazadas_global_dd',
    ('registrosContinentales', 'LR/lc_IUCN'):'registros_continentales_amenazadas_global_lr_lc', 
    ('registrosContinentales', 'LR/nt_IUCN'):'registros_continentales_amenazadas_global_lr_nt' ,

    ('especiesMarinas', 'threatStatus_MADS'):'especies_marinas_amenazadas_nacional_total', 
    ('especiesMarinas', 'EN_MADS'):'especies_marinas_amenazadas_nacional_en', 
    ('especiesMarinas', 'CR_MADS'):'especies_marinas_amenazadas_nacional_cr',
    ('especiesMarinas', 'VU_MADS'):'especies_marinas_amenazadas_nacional_vu',
    ('registrosMarinos', 'threatStatus_MADS'):'registros_marinas_amenazadas_nacional_total', 
    ('registrosMarinos', 'CR_MADS'):'registros_marinas_amenazadas_nacional_cr', 
    ('registrosMarinos', 'EN_MADS'):'registros_marinas_amenazadas_nacional_en',
    ('registrosMarinos', 'VU_MADS'):'registros_marinas_amenazadas_nacional_vu',  
    ('especiesMarinas', 'appendixCITES'):'especies_marinas_cites_total', 
    ('especiesMarinas', 'I'):'especies_marinas_cites_i', 
    ('especiesMarinas', 'I/II'):'especies_marinas_cites_i_ii', #Como es el campo
    ('especiesMarinas', 'II'):'especies_marinas_cites_ii', 
    ('especiesMarinas', 'III'):'especies_marinas_cites_iii', 
    ('registrosMarinos', 'appendixCITES'):'registros_marinas_cites_total', 
    ('registrosMarinos', 'I'):'registros_marinas_cites_i', 
    ('registrosMarinos', 'I/II'):'registros_marinas_cites_i_ii', #Como es el campo
    ('registrosMarinos', 'II'):'registros_marinas_cites_ii', 
    ('registrosMarinos', 'III'):'registros_marinas_cites_iii',    
    'especies_marinas_exoticas_total':'especies_marinas_exoticas_total', 
    ('especiesMarinas', 'especies_exoticas'):'especies_marinas_exoticas',
    ('especiesMarinas', 'especies_invasoras'):'especies_marinas_invasoras',
    ('especiesMarinas', 'especies_exotica_riesgo_invasion'):'especies_marinas_exoticas_riesgo_invasion', 
    'registros_marinas_exoticas_total':'registros_marinas_exoticas_total', 
    ('registrosMarinos', 'especies_exoticas'):'registros_marinas_exoticas',     
    ('registrosMarinos', 'especies_invasoras'):'registros_marinas_invasoras', 
    ('registrosMarinos', 'especies_exotica_riesgo_invasion'):'registros_marinas_exoticas_riesgo_invasion',    
    ('especiesMarinas', 'endemic'):'especies_marinas_endemicas', 
    ('especiesMarinas', 'Migratorio'):'especies_marinas_migratorias', 
    ('registrosMarinos', 'endemic'):'registros_marinas_endemicas', 
    ('registrosMarinos', 'Migratorio'):'registros_marinas_migratorias', 
    'especies_marinas_amenazadas_global_total':'especies_marinas_amenazadas_global_total',
    #('especiesMarinas', 'threatStatus_UICN'):'especies_marinas_amenazadas_global_total', 
    ('especiesMarinas', 'EX_IUCN'):'especies_marinas_amenazadas_global_ex', 
    ('especiesMarinas', 'EW_IUCN'):'especies_marinas_amenazadas_global_ew', 
    ('especiesMarinas', 'CR_IUCN'):'especies_marinas_amenazadas_global_cr', 
    ('especiesMarinas', 'EN_IUCN'):'especies_marinas_amenazadas_global_en', 
    ('especiesMarinas', 'VU_IUCN'):'especies_marinas_amenazadas_global_vu',
    ('especiesMarinas', 'NT_IUCN'):'especies_marinas_amenazadas_global_nt', 
    ('especiesMarinas', 'LC_IUCN'):'especies_marinas_amenazadas_global_lc', 
    ('especiesMarinas', 'DD_IUCN'):'especies_marinas_amenazadas_global_dd',
    ('especiesMarinas', 'LR/lc_IUCN'):'especies_marinas_amenazadas_global_lr_lc', 
    ('especiesMarinas', 'LR/nt_IUCN'):'especies_marinas_amenazadas_global_lr_nt', 
    'registros_marinas_amenazadas_global_total':'registros_marinas_amenazadas_global_total',
    #('registrosMarinos', 'threatStatus_UICN'):'registros_marinas_amenazadas_global_total', 
    ('registrosMarinos', 'EX_IUCN'):'registros_marinas_amenazadas_global_ex', 
    ('registrosMarinos', 'EW_IUCN'):'registros_marinas_amenazadas_global_ew', 
    ('registrosMarinos', 'CR_IUCN'):'registros_marinas_amenazadas_global_cr', 
    ('registrosMarinos', 'EN_IUCN'):'registros_marinas_amenazadas_global_en', 
    ('registrosMarinos', 'VU_IUCN'):'registros_marinas_amenazadas_global_vu', 
    ('registrosMarinos', 'NT_IUCN'):'registros_marinas_amenazadas_global_nt', 
    ('registrosMarinos', 'LC_IUCN'):'registros_marinas_amenazadas_global_lc', 
    ('registrosMarinos', 'DD_IUCN'):'registros_marinas_amenazadas_global_dd',
    ('registrosMarinos', 'LR/lc_IUCN'):'registros_marinas_amenazadas_global_lr_lc', 
    ('registrosMarinos', 'LR/nt_IUCN'):'registros_marinas_amenazadas_global_lr_nt', 


  
    ('especiesSalobres', 'threatStatus_MADS'):'especies_salobres_amenazadas_nacional_total', 
    ('especiesSalobres', 'CR_MADS'):'especies_salobres_amenazadas_nacional_cr',
    ('especiesSalobres', 'EN_MADS'):'especies_salobres_amenazadas_nacional_en', 
    ('especiesSalobres', 'VU_MADS'):'especies_salobres_amenazadas_nacional_vu',
    ('registrosSalobres', 'threatStatus_MADS'):'registros_salobres_amenazadas_nacional_total', 
    ('registrosSalobres', 'CR_MADS'):'registros_salobres_amenazadas_nacional_cr', 
    ('registrosSalobres', 'EN_MADS'):'registros_salobres_amenazadas_nacional_en',
    ('registrosSalobres', 'VU_MADS'):'registros_salobres_amenazadas_nacional_vu',  
    ('especiesSalobres', 'appendixCITES'):'especies_salobres_cites_total', 
    ('especiesSalobres', 'I'):'especies_salobres_cites_i', 
    ('especiesSalobres', 'I/II'):'especies_salobres_cites_i_ii', #Como es el campo
    ('especiesSalobres', 'II'):'especies_salobres_cites_ii', 
    ('especiesSalobres', 'III'):'especies_salobres_cites_iii', 
    ('registrosSalobres', 'appendixCITES'):'registros_salobres_cites_total', 
    ('registrosSalobres', 'I'):'registros_salobres_cites_i', 
    ('registrosSalobres', 'I/II'):'registros_salobres_cites_i_ii', #Como es el campo
    ('registrosSalobres', 'II'):'registros_salobres_cites_ii', 
    ('registrosSalobres', 'III'):'registros_salobres_cites_iii', 
    'especies_salobres_exoticas_total':'especies_salobres_exoticas_total', 
    ('especiesSalobres', 'especies_exoticas'):'especies_salobres_exoticas', 
    ('especiesSalobres', 'especies_invasoras'):'especies_salobres_invasoras', 
    ('especiesSalobres', 'especies_exotica_riesgo_invasion'):'especies_salobres_exoticas_riesgo_invasion',     
    'registros_salobres_exoticas_total':'registros_salobres_exoticas_total', 
    ('registrosSalobres', 'especies_exoticas'):'registros_salobres_exoticas',
    ('registrosSalobres', 'especies_invasoras'):'registros_salobres_invasoras', 
    ('registrosSalobres', 'especies_exotica_riesgo_invasion'):'registros_salobres_exoticas_riesgo_invasion',   
    ('especiesSalobres', 'endemic'):'especies_salobres_endemicas',     
    ('especiesSalobres', 'Migratorio'):'especies_salobres_migratorias', 
    ('registrosSalobres', 'endemic'):'registros_salobres_endemicas', 
    ('registrosSalobres', 'Migratorio'):'registros_salobres_migratorias', 
    'especies_salobres_amenazadas_global_total':'especies_salobres_amenazadas_global_total',
    #('especiesSalobres', 'threatStatus_UICN'):'especies_salobres_amenazadas_global_total', 
    ('especiesSalobres', 'EX_IUCN'):'especies_salobres_amenazadas_global_ex', 
    ('especiesSalobres', 'EW_IUCN'):'especies_salobres_amenazadas_global_ew', 
    ('especiesSalobres', 'CR_IUCN'):'especies_salobres_amenazadas_global_cr', 
    ('especiesSalobres', 'EN_IUCN'):'especies_salobres_amenazadas_global_en', 
    ('especiesSalobres', 'VU_IUCN'):'especies_salobres_amenazadas_global_vu',
    ('especiesSalobres', 'NT_IUCN'):'especies_salobres_amenazadas_global_nt', 
    ('especiesSalobres', 'LC_IUCN'):'especies_salobres_amenazadas_global_lc',
    ('especiesSalobres', 'DD_IUCN'):'especies_salobres_amenazadas_global_dd',
    ('especiesSalobres', 'LR/lc_IUCN'):'especies_salobres_amenazadas_global_lr_lc', 
    ('especiesSalobres', 'LR/nt_IUCN'):'especies_salobres_amenazadas_global_lr_nt',
    'registros_salobres_amenazadas_global_total':'registros_salobres_amenazadas_global_total',
    #('registrosSalobres', 'threatStatus_UICN'):'registros_salobres_amenazadas_global_total', 
    ('registrosSalobres', 'EX_IUCN'):'registros_salobres_amenazadas_global_ex', 
    ('registrosSalobres', 'EW_IUCN'):'registros_salobres_amenazadas_global_ew', 
    ('registrosSalobres', 'CR_IUCN'):'registros_salobres_amenazadas_global_cr', 
    ('registrosSalobres', 'EN_IUCN'):'registros_salobres_amenazadas_global_en', 
    ('registrosSalobres', 'VU_IUCN'):'registros_salobres_amenazadas_global_vu', 
    ('registrosSalobres', 'NT_IUCN'):'registros_salobres_amenazadas_global_nt', 
    ('registrosSalobres', 'LC_IUCN'):'registros_salobres_amenazadas_global_lc', 
    ('registrosSalobres', 'DD_IUCN'):'registros_salobres_amenazadas_global_dd',
    ('registrosSalobres', 'LR/lc_IUCN'):'registros_salobres_amenazadas_global_lr_lc', 
    ('registrosSalobres', 'LR/nt_IUCN'):'registros_salobres_amenazadas_global_lr_nt', 
    ('registrosSalobres', 'Residente'):'registros_salobres_residente', 

    }

    ##Número de columnas del conjunto de datos inicial
    columns_num = len(archivo.columns)

    ##Crear los diccionarios vacios
    names_tot,names_list,names_final=[],[],[]

    i=0

    ##Hacer un ciclo que se repita el número de columnas que tenga el conjunto de datos inicial
    while i < columns_num:

        ##Recorrer el diccionario con los nombres
        for name in names:

            ##Comparar el nombre de la columna en la posición i con los valores del listado de nombres
            if archivo.columns.values[i] == name:
                
                ##Reasignar el nombre
                archivo.columns.values[i]=names[name]

                ##Crear una lista con los nombres nuevos de las columnas que se encuentran en el archivo
                names_tot.append(names[name])   
            
            ##Crear una lista con solo los nombres definitvos de la lista de nombres
            names_list.append(names[name])  
        i+=1

    ##Recorrer el listado de nombres names_tot de la lista de nombres
    for i in names_list:

        ## Recorrer la lista con los nombres nuevos de las columnas que se encuentran en el archivo
        for j in names_tot:

            ## Si son iguales agregar el nombre a la lista
            if i in j:
                names_final.append(i)

    ##Ordenar las columnas de la tabla con la lista final
    archivo=archivo.reset_index()
    archivo=archivo.reindex(columns=pd.unique(names_final))

    ##Quitar .0 al final de las cifras
    archivo=archivo.astype(str)
    archivo=archivo.replace(to_replace='\.0+$',value="",regex=True)

    #Cifras finales por grupo biológico y temática
    ##Para grupos biologicos
    if tipo== 1:
        archivo.to_csv(nombre+'region_grupo_biologico.tsv',sep='\t', index=False )
        archivo.to_excel(nombre+'region_grupo_biologico.xlsx', sheet_name='cifrasGruposBiologicos', index=False )

    ##Para cifras geograficas
    if tipo==2:
        archivo.to_csv(nombre+'region_tematica.tsv',sep='\t', index=False )
        archivo.to_excel(nombre+'region_tematica.xlsx', sheet_name='cifrasGeográficas', index=False )


##-------------------------------------------------Ejecución de la función ajuste_nombres ------------------------------------------------##
##Se llama la función ingresando los dos parametros requeridos
#Para grupos biologicos

ajuste_nombres(grupos_biologicos_total, 1)

#Para cifras geográficas
ajuste_nombres(geografia_total, 2)
fin=time.time()    
print((fin-inicio)/60)