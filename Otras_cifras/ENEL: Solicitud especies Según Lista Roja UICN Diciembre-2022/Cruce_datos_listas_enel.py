# -*- coding: utf-8 -*-
"""
Created on Thu Nov 10 11:28:08 2022

@author: nerieth.leuro
"""
import pandas as pd 
import numpy as np


#dwme_g= pd.read_table('D:\\ENEL Solicitud especies Según Lista Roja UICN\\Diciembre\\occurrence.txt', sep='\t', encoding = "utf8")
#print(list(dwme_g.columns))

dwme_g= pd.read_table('D:\\ENEL Solicitud especies Según Lista Roja UICN\\Diciembre\\occurrence.txt', sep='\t', encoding = "utf8",usecols=['gbifID', 'date', 'publisher', 'source', 'title', 'type', 'datasetID', 
'institutionCode','datasetName', 'ownerInstitutionCode', 'basisOfRecord','dataGeneralizations', 'dynamicProperties', 'occurrenceID', 'recordedBy','individualCount', 'organismQuantity', 'organismQuantityType', 'sex', 'lifeStage', 
'establishmentMeans', 'occurrenceStatus','associatedOccurrences', 'occurrenceRemarks','organismRemarks'
, 'eventID', 'parentEventID', 'eventDate', 'eventTime', 'startDayOfYear', 'endDayOfYear', 'year', 'month', 'day', 
'verbatimEventDate', 'habitat', 'samplingProtocol', 'sampleSizeValue', 'sampleSizeUnit', 'samplingEffort','eventRemarks','higherGeography', 'continent', 
'waterBody','countryCode', 'stateProvince', 'county', 'municipality', 'locality', 
'verbatimLocality', 'verbatimElevation','locationRemarks', 'decimalLatitude', 'decimalLongitude'
,'georeferencedBy', 'georeferencedDate', 'georeferenceProtocol', 'georeferenceSources', 
'georeferenceRemarks','verbatimIdentification', 'identificationQualifier', 'typeStatus', 'identifiedBy','dateIdentified'
, 'identificationReferences', 'identificationRemarks', 'taxonID','scientificName', 'acceptedNameUsage', 'parentNameUsage', 
'originalNameUsage', 'nameAccordingTo', 'higherClassification', 'kingdom', 'phylum', 'class'
, 'order', 'family', 'subfamily', 'genus', 'genericName', 'subgenus', 
'infragenericEpithet', 'specificEpithet', 'infraspecificEpithet', 
 'taxonRank', 'verbatimTaxonRank', 'vernacularName', 'nomenclaturalCode', 
 'taxonomicStatus', 'nomenclaturalStatus', 'taxonRemarks', 'datasetKey', 
 'elevation','species'],dtype=str)     

#Load thematic list
#CITES
cites = pd.read_table('D:\\cifras_Santander\\Nuevas 2021T4\\ListasTaxo\\ListaCITES_2022.tsv', usecols =['species','appendixCITES','taxonRank'])
cites = cites[(cites['taxonRank'] == 'Especie') ].drop(['taxonRank'],axis = 1)
#UICN
uicn = pd.read_table('D:\\cifras_Santander\\Nuevas 2021T4\\ListasTaxo\\Lista_UICNredList_2022.tsv', usecols = ['threatStatus_UICN', 'species'])
#Amenazadas MADS
MADS_am = pd.read_table('D:\\cifras_Santander\\Nuevas 2021T4\\ListasTaxo\\Lista_amenazadas_resolucion1912_2017_2022.tsv', usecols = ['threatStatus', 'species','taxonRank'])
MADS_am = MADS_am[(MADS_am['taxonRank'] == 'Especie')].drop(['taxonRank'],axis = 1)
MADS_am.rename(columns={'threatStatus': 'threatStatus_MADS'}, inplace=True)
#Invasoras MADS
All_inv = pd.read_table('D:\\cifras_Santander\\Nuevas 2021T4\\ListasTaxo\\Exóticas\\lista_invasoras_exoticas_2022.tsv', usecols = ['especies_invasoras', 'species','taxonRank'])
All_inv = All_inv[(All_inv['especies_invasoras'] == 'Invasora') & (All_inv['taxonRank'] == 'Especie')].drop(['taxonRank'],axis = 1)

All_exotic = pd.read_table('D:\\cifras_Santander\\Nuevas 2021T4\\ListasTaxo\\Exóticas\\lista_invasoras_exoticas_2022.tsv', usecols = ['especies_exoticas', 'species','taxonRank'])
All_exotic = All_exotic[(All_exotic['especies_exoticas'] == 'Exótica') & (All_exotic['taxonRank'] == 'Especie')].drop(['taxonRank'],axis = 1)

All_pot_inv = pd.read_table('D:\\cifras_Santander\\Nuevas 2021T4\\ListasTaxo\\Exóticas\\lista_invasoras_exoticas_2022.tsv', usecols = ['especies_exotica_riesgo_invasion', 'species','taxonRank'])
All_pot_inv = All_pot_inv[(All_pot_inv['especies_exotica_riesgo_invasion'] == 'Exótica con potencial de invasión') & (All_pot_inv['taxonRank'] == 'Especie') ].drop(['taxonRank'],axis = 1)

#Listas de referencia taxonómica
referencia = pd.read_table('D:\\cifras_Santander\\Nuevas 2021T4\\ListasTaxo\\Listas_taxonomicasCol_2022.tsv', usecols =['species','endemic','migratory','taxonRank', 'especies_unicas', 'datasetID'])
referencia = referencia[(referencia['taxonRank'] == 'Especie') & (referencia['especies_unicas'] == 'especie unica')].drop(['taxonRank' ],axis = 1)
referencia = referencia[referencia['species'].notnull()]
#referencia = referencia.replace({'File':{'mamiferos_col_2020.csv' : 'doi.org/10.15472/kl1whs', 'taxon_catalogo_plantas_liquenes.csv' : 'doi.org/10.15472/7avdhn', 'taxon_listaAves_2020.csv':'doi.org/10.15472/qhsz0p', 'taxon_listaPecesDulce_2020.csv': 'doi.org/10.15472/numrso' }})
referencia.rename(columns={'datasetID': 'listaReferenciaCO'}, inplace=True)

#Cruces con archivo de datos.
dwme_g=pd.merge(left=dwme_g,right=cites, how= 'left', left_on='species', right_on='species')
dwme_g=pd.merge(left=dwme_g,right=uicn, how= 'left', left_on='species', right_on='species')
dwme_g=pd.merge(left=dwme_g,right=MADS_am, how= 'left', left_on='species', right_on='species')
dwme_g=pd.merge(left=dwme_g,right=All_inv, how= 'left', left_on='species', right_on='species')
dwme_g=pd.merge(left=dwme_g,right=All_exotic, how= 'left', left_on='species', right_on='species')
dwme_g=pd.merge(left=dwme_g,right=All_pot_inv, how= 'left', left_on='species', right_on='species')
#Eliminar el valor criptogénica en la lista temática de griis
#dwm['exotic'] = np.where((dwm['exotic'] =='Criptogénica') ,'', dwm['exotic'])
dwme_g=pd.merge(left=dwme_g,right=referencia, how= 'left', left_on='species', right_on='species')


#Flag Taxonómico (Solo para aves, plantas y mamíferos, para peces se descartó debido a la complejidad de diferenciar peces dulceacuícolas y marinos)
dwme_g['flagTAXO'] = np.where((dwme_g['listaReferenciaCO'].isnull()) & (dwme_g['species'].notnull()) & (dwme_g['kingdom'] =='Plantae') , 'Especie no coincide con listas taxonómicas de referencia', '')
dwme_g['flagTAXO'] = np.where((dwme_g['listaReferenciaCO'].isnull()) & (dwme_g['species'].notnull()) & (dwme_g['class'] =='Aves') , 'Especie no coincide con listas taxonómicas de referencia', dwme_g['flagTAXO'])
dwme_g['flagTAXO'] = np.where((dwme_g['listaReferenciaCO'].isnull()) & (dwme_g['species'].notnull()) & (dwme_g['class'] =='Mammalia') , 'Especie no coincide con listas taxonómicas de referencia', dwme_g['flagTAXO'])

#Verificación Flag
dwme_g.groupby(['flagTAXO']).gbifID.nunique().reset_index()
dwme_g.groupby(['flagTAXO']).species.nunique().reset_index()

print(dwme_g.columns)
dwme_g.to_csv('Enel20221209.txt',sep='\t', index=False )


#Mammalia especies no válidadas lista taxo = 134
#Aves especies no válidadas lista taxo = 267
#plantae especies no válidadas lista taxo= 6683


del MADS_am
del All_inv
del All_exotic
del All_pot_inv
del cites
del uicn
del referencia
